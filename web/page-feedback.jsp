<%@page import="entity.Doctor"%>
<%@page import="java.util.List"%>
<%@page import="entity.FeedbackUser"%>
<%
    List<FeedbackUser> allFeedback = (List<FeedbackUser>) request.getSession().getAttribute("allFeedback");
    Doctor doctor = (Doctor) request.getSession().getAttribute("getDoctor");
    Integer patientID = (Integer) request.getSession().getAttribute("idPatient");
%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body>

        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <!-- row -->
                <div class="container-fluid">
                    <div class="page-titles">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.jsp">App</a></li>
                            <li class="breadcrumb-item active"><a href="">List Feedback Send</a></li>
                        </ol>
                    </div>
                    <div class="form-head d-flex mb-3 mb-md-4 align-items-start">
                        <div class="input-group search-area ml-auto d-inline-flex">
                            <input type="text" class="form-control" placeholder="Search here">
                            <div class="input-group-append">
                                <button type="button" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
                            </div>
                        </div>
                        <div class="dropdown ml-2 d-inline-block">
                            <div class="btn btn-primary light btn-rounded dropdown-toggle" data-toggle="dropdown">
                                Newest
                            </div>
                            <div class="dropdown-menu dropdown-menu-left">
                                <a class="dropdown-item" href="javascript:void(0);">Newest</a>
                                <a class="dropdown-item" href="javascript:void(0);">Old</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <ul class="nav nav-pills review-tab">
                                <li class="nav-item">
                                    <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">Feedback Send</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="navpills-1" class="tab-pane active">
                                    <%for (FeedbackUser feedbackUser : allFeedback) {%>
                                    <%if (feedbackUser.getPatientId() == patientID) {%>
                                    <div class="card review-table">
                                        <div class="media align-items-center">
                                            <div class="checkbox mr-lg-4 mr-0 align-self-center">
                                                <div class="custom-control custom-checkbox checkbox-info">
                                                    <input type="checkbox" class="custom-control-input" checked="" id="customCheckBox1" required="">
                                                    <label class="custom-control-label" for="customCheckBox1"></label>
                                                </div>
                                            </div>
                                            <%if (doctor.getAccount().getGender() == 1) {%>
                                            <img alt="" src="images/doctors/male.jpg" height="43" width="43" class="rounded-circle ml-4">
                                            <%} else {%>
                                            <img alt="" src="images/doctors/female.png" height="43" width="43" class="rounded-circle ml-4">
                                            <%}%>
                                            <div class="media-body d-lg-flex d-block row align-items-center">
                                                <div class="col-xl-4 col-xxl-5 col-lg-6 review-bx">
                                                    <h3 class="fs-20 font-w600 mb-1"><a class="text-black" href="javascript:void(0);"><%=doctor.getAccount().getFullname()%></a></h3>
                                                    <span class="fs-15 d-block"><%=doctor.getRole()%></span>
                                                    <span class="text-primary d-block font-w600 mt-sm-2 mt-3"><i class="las la-stethoscope scale5 mr-3"></i><%=doctor.getBranch()%></span>
                                                </div>
                                                <div class="col-xl-7 col-xxl-7 col-lg-6 text-dark mb-lg-0 mb-2">
                                                    <%=feedbackUser.getContent()%>
                                                </div>
                                            </div>
                                            <%if (feedbackUser.getVote() == Constant.ONE_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackUser.getVote() == Constant.TWO_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackUser.getVote() == Constant.THREE_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackUser.getVote() == Constant.FOUR_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackUser.getVote() == Constant.FIVE_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <div class="dropdown ml-auto text-right">
                                                <div class="btn-link" data-toggle="dropdown" >
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M12 11C11.4477 11 11 11.4477 11 12C11 12.5523 11.4477 13 12 13C12.5523 13 13 12.5523 13 12C13 11.4477 12.5523 11 12 11Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M12 18C11.4477 18 11 18.4477 11 19C11 19.5523 11.4477 20 12 20C12.5523 20 13 19.5523 13 19C13 18.4477 12.5523 18 12 18Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                    <path d="M12 4C11.4477 4 11 4.44772 11 5C11 5.55228 11.4477 6 12 6C12.5523 6 13 5.55228 13 5C13 4.44772 12.5523 4 12 4Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="edit-feedback-controller?id_feedback=<%=feedbackUser.getFeedbackId()%>">Edit</a>
                                                    <a class="dropdown-item" href="delete-feedback-controller?id_feedback=<%=feedbackUser.getFeedbackId()%>" onclick="return confirm('Are you sure you want to Remove?');">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%}%> 
                                    <%}%>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--**********************************
                Content body end
            ***********************************-->

            <!--**********************************
                Footer start
            ***********************************-->
            <%@include file="common/footer.jsp" %>
            <!--**********************************
                Footer end
            ***********************************-->


        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <%@include file="common/included-vendors.jsp" %>

        <!--**********************************
            Scripts
        ***********************************-->

    </body>

</html>