<%@page import="entity.Doctor"%>
<%
    Doctor doctor = (Doctor) request.getSession().getAttribute("getDoctor");
%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body>

        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <div class="container-fluid">
                    <div class="row page-titles">
                        <div class="col-sm-6 p-md-0">
                            <div class="welcome-text">
                                <h4>Hi, Welcome <%=account.getFullname()%></h4>
                            </div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Feedback About Doctors: <%=doctor.getAccount().getFullname()%></h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-validation">
                                        <form class="form-valide" action="#" method="POST">
                                            <div class="row">
                                            <div class="col-xl-6">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label" for="val-suggestions">Content <span
                                                            class="text-danger">*</span>
                                                    </label>
                                                    <div class="col-lg-6">
                                                        <textarea class="form-control" id="val-suggestions" name="contentFeedback" rows="5" placeholder="Please write your feedback about the doctor here !"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label" for="val-skill">Vote Star
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-group mb-0">
                                                        <label class="radio-inline mr-3"><input type="radio" name="voteStar" value="<%=Constant.ONE_STAR%>">
                                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                                <div class="disease mr-5">
                                                                    <span class="star-review  d-inline-block">
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-gray"></i>
                                                                        <i class="fa fa-star text-gray"></i>
                                                                        <i class="fa fa-star text-gray"></i>
                                                                        <i class="fa fa-star text-gray"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </label>
                                                        <label class="radio-inline mr-3"><input type="radio" name="voteStar" value="<%=Constant.TWO_STAR%>"> 
                                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                                <div class="disease mr-5">
                                                                    <span class="star-review  d-inline-block">
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-gray"></i>
                                                                        <i class="fa fa-star text-gray"></i>
                                                                        <i class="fa fa-star text-gray"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </label>
                                                        </br>
                                                        <label class="radio-inline mr-3"><input type="radio" name="voteStar" value="<%=Constant.THREE_STAR%>">
                                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                                <div class="disease mr-5">
                                                                    <span class="star-review  d-inline-block">
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-gray"></i>
                                                                        <i class="fa fa-star text-gray"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </label>
                                                        <label class="radio-inline mr-3"><input type="radio" name="voteStar" value="<%=Constant.FOUR_STAR%>">
                                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                                <div class="disease mr-5">
                                                                    <span class="star-review  d-inline-block">
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-gray"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </label>
                                                        </br>
                                                        <label class="radio-inline mr-3"><input type="radio" name="voteStar" value="<%=Constant.FIVE_STAR%>">
                                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                                <div class="disease mr-5">
                                                                    <span class="star-review  d-inline-block">
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-orange"></i>
                                                                        <i class="fa fa-star text-orange"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-8 ml-auto">
                                                        <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure you want send Feedback?');">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        <%@include file="common/footer.jsp" %>
        <!--**********************************
            Footer end
        ***********************************-->


    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!-- Required vendors -->
    <%@include file="common/included-vendors.jsp" %>

    <!--**********************************
        Scripts
    ***********************************-->

    <!-- Datatable -->
    <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

    <script>
                                                            (function ($) {

                                                                var table = $('#example5').DataTable({
                                                                    searching: false,
                                                                    paging: true,
                                                                    select: false,
                                                                    //info: false,         
                                                                    lengthChange: false

                                                                });
                                                                $('#example tbody').on('click', 'tr', function () {
                                                                    var data = table.row(this).data();

                                                                });

                                                            })(jQuery);
    </script>

</body>
</html>