<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body>

        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <!-- row -->
                <div class="container-fluid">
                    <form action="view-patients" method="POST">
                        <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="filtered">
                            <option value="0" <c:if test="${selectBox==0}">selected</c:if>>All</option>
                            <option value="1"<c:if test="${selectBox==1}">selected</c:if>>Body temperature greater than 38 degrees Celsius</option>
                            <option value="2"<c:if test="${selectBox==2}">selected</c:if>>Symptoms of headache</option>
                            <option value="3"<c:if test="${selectBox==3}">selected</c:if>>Symptoms of shortness of breath</option>
                            <option value="4"<c:if test="${selectBox==4}">selected</c:if>>Serious situation</option>
                        </select>
                        <button type="submit" class="btn btn-success">Filter</button>
                            </form>
                                <form action="view-patients" method="POST">
                                    <div class="form-head d-flex mb-3 mb-md-4 align-items-start">
                                        <div class="input-group search-area ml-auto d-inline-flex mr-3">

                                            <input type="number" class="form-control" name="filtered" value="${selectBox}" hidden="true">
                                        <input type="text" class="form-control" name="namePatient" placeholder="Search name patient">


                                    </div>
                                    <div class="input-group-append">
                                        <button type="submit" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
                                    </div>
                                </div>
                            </form>
                    
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="table-responsive">
                                <table id="example5" class="table table-striped patient-list mb-4 dataTablesCard fs-14">
                                    <thead>
                                        <tr>                                       
                                            <th>Patient ID</th>
                                            <th>Patient Name</th>
                                            <th>Email</th>
                                            <th>Phone Number</th>  
                                            <th>Body Temperature</th>
                                            <th>Fever</th>
                                            <th>Headache</th>
                                            <th>Shortness Breath</th>
                                            <th>Vaccine Status</th>
                                            <th>Patient Status</th>
                                            <th>Serious Situation</th>    
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:if test="${listPatients != null}">
                                            <c:forEach items="${listPatients}" var="viewPatient">
                                                <tr>                                        
                                                    <td>${viewPatient.patientId}</td>
                                                    <td>${viewPatient.fullname}</td>
                                                    <td>${viewPatient.email}</td>
                                                    <td>${viewPatient.phonenumber}</td>
                                                   
                                                    <c:if test="${viewPatient.bodyTemperature >= 38}">
                                                        <td style="color: red">
                                                    </c:if> 
                                                     <c:if test="${viewPatient.bodyTemperature < 38}">
                                                        <td>
                                                    </c:if>${viewPatient.bodyTemperature}</td>
                                                                
                                                    <c:if test="${viewPatient.fever == 0 || viewPatient.fever == null}">
                                                        <td>No</td>
                                                    </c:if>
                                                    <c:if test="${viewPatient.fever == 1 && viewPatient.fever != null}">
                                                         <td style="color: red">Yes</td>
                                                    </c:if>

                                                    <c:if test="${viewPatient.headache == 0 || viewPatient.headache == null}">
                                                        <td>No</td>
                                                    </c:if>
                                                    <c:if test="${viewPatient.headache == 1 && viewPatient.headache != null}">
                                                        <td style="color: red">Yes</td>
                                                    </c:if> 

                                                    <c:if test="${viewPatient.shortnessBreath == 0 || viewPatient.shortnessBreath == null}">
                                                        <td>No</td>
                                                    </c:if>
                                                    <c:if test="${viewPatient.shortnessBreath == 1 && viewPatient.shortnessBreath != null}">
                                                        <td style="color: red">Yes</td>
                                                    </c:if> 
                                                    <td>${viewPatient.vaccineStatus}</td>
                                                    <td>${viewPatient.patientStatus}</td>

                                                    <c:if test="${viewPatient.seriousSituation == 0 || viewPatient.seriousSituation == null}">
                                                        <td>No</td>
                                                    </c:if>
                                                    <c:if test="${viewPatient.seriousSituation == 1 && viewPatient.seriousSituation != null}">
                                                        <td style="color: red">Yes</td>
                                                    </c:if> 
                                                        
                                                    <td>
                                                        <a href="patient-detail?patientId=${viewPatient.patientId}">
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M17 3C17.2626 2.73735 17.5744 2.52901 17.9176 2.38687C18.2608 2.24473 18.6286 2.17157 19 2.17157C19.3714 2.17157 19.7392 2.24473 20.0824 2.38687C20.4256 2.52901 20.7374 2.73735 21 3C21.2626 3.26264 21.471 3.57444 21.6131 3.9176C21.7553 4.26077 21.8284 4.62856 21.8284 5C21.8284 5.37143 21.7553 5.73923 21.6131 6.08239C21.471 6.42555 21.2626 6.73735 21 7L7.5 20.5L2 22L3.5 16.5L17 3Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                            </svg>
                                                        </a>
                                                    </td>

                                                </tr>
                                            </c:forEach>                                            
                                        </c:if>
                                    </tbody>
                                </table>
                                <nav aria-label="...">
                                    <ul class="pagination pagination-lg" style="    display: flex;
                                                                                    justify-content: center;">
                                        <c:if test="${listPatients != null}">
                                            <c:forEach begin="1" end="${totalPage}" var="i">
                                                <c:if test="${i == currentPage}">
                                                    <li class="page-item active">
                                                </c:if>
                                                <c:if test="${i != currentPage}">
                                                    <li class="page-item">
                                                </c:if>        
                                                <a class="page-link" href="view-patients?page=${i}">${i}</a>
                                                </li>         
                                            </c:forEach>                                      
                                        </c:if>
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--**********************************
                Content body end
            ***********************************-->

            <!--**********************************
                Footer start
            ***********************************-->
            <%@include file="common/footer.jsp" %>
            <!--**********************************
                Footer end
            ***********************************-->

            <!--**********************************
            Support ticket button start
            ***********************************-->

            <!--**********************************
               Support ticket button end
            ***********************************-->


        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <%@include file="common/included-vendors.jsp" %>

        <!--**********************************
            Scripts
        ***********************************-->

        <!-- Datatable -->
        <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

        <script>
            (function ($) {

                var table = $('#example5').DataTable({
                    searching: false,
                    paging: true,
                    select: false,
                    //info: false,         
                    lengthChange: false

                });
                $('#example tbody').on('click', 'tr', function () {
                    var data = table.row(this).data();

                });

            })(jQuery);
        </script>

    </body>
</html>