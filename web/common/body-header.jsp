
<!--**********************************
    Body header start
***********************************-->

<%@page import="common.CommonUtil"%>
<%@page import="model.DBConnect"%>
<%@page import="entity.Account"%>
<%@page import="common.Constant"%>
<%
    DBConnect db = new DBConnect();
    if (db.getConnection() == null) {
        CommonUtil.dispatch(request, response, "page-error-504.jsp");
    };

    Account account = (Account) request.getSession().getAttribute("account");
    if (account == null) {
        CommonUtil.dispatch(request, response, "page-login.jsp");
    }
%>

<!--**********************************
    Nav header start
***********************************-->
<div class="nav-header">
    <a href="index.jsp" class="brand-logo">
        <img class="logo-abbr" src="./images/logo.png" alt=""></img>
        <img class="logo-compact" src="./images/logo-text.png" alt=""></img>
        <img class="brand-title" src="./images/logo-text.png" alt=""></img>
    </a>
    <div class="nav-control">
        <div class="hamburger">
            <span class="line"></span><span class="line"></span><span class="line"></span>
        </div>
    </div>
</div>
<!--**********************************
    Nav header end
***********************************-->

<!--**********************************
    Header start
***********************************-->
<div class="header">
    <div class="header-content">
        <nav class="navbar navbar-expand">
            <div class="collapse navbar-collapse justify-content-between">
                <div class="header-left">
                    <div class="dashboard_bar">
                        <%= account.getRoleName() + " Dashboard"%>
                    </div>
                </div>

                <ul class="navbar-nav header-right">
                    <li class="nav-item dropdown header-profile">
                        <a class="nav-link" href="javascript:;" role="button" data-toggle="dropdown">
                            <img src="images/profile/12.png" width="20" alt=""/>
                            <div class="header-info">
                                <span>Hello,<strong><%= " " + account.getFullname()%></strong></span>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <form action="show-profile-controller" method="POST">
                                <a href="page-user-profile.jsp" class="dropdown-item ai-icon">
                                    <svg id="icon-user1" xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                    <span class="ml-2">Profile</span>
                                </a>
                            </form>
                            <a href="page-login.jsp" class="dropdown-item ai-icon">
                                <svg id="icon-logout" xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
                                <span class="ml-2">Logout</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<!--**********************************
    Header end ti-comment-alt
***********************************-->
<!--**********************************
    Side-bar start
***********************************-->
<div class="deznav">
    <div class="deznav-scroll">
        <ul class="metismenu" id="menu">
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="flaticon-381-networking"></i>
                    <span class="nav-text">
                        <%= account.getRoleName() + " Dashboard"%>
                    </span>
                </a>
                <ul aria-expanded="false">

                    <%if (account.getRoleId() == Constant.ROLE_ADMIN) {%>
                    <li><a href="ShowPatient">Patient</a></li>
                    <li><a href="show-doctors-controller">Doctor</a></li>
                    <li><a href="show-feedback-controller">Review</a></li>
                    <li><a href="show-user-controller">User</a></li>
                    <li><a href="location-view-list-controller">Location</a></li>
                    <li><a href="show-travel-schedule-controller">Travel Schedule</a></li>
                        <%}%>

                    <%if (account.getRoleId() == Constant.ROLE_DOCTOR) {%>
                    <li><a href="view-patients">Patient</a></li>
                    <li><a href="show-feedback-controller">Review</a></li>
                        <%}%>

                    <%if (account.getRoleId() == Constant.ROLE_NURSE) {%>
                    <li><a href="ShowPatient">Patient</a></li>
                    <li><a href="show-doctors-controller">Doctor</a></li>
                    <li><a href="show-feedback-controller">Review</a></li>
                        <%}%>

                    <%if (account.getRoleId() == Constant.ROLE_PATIENT) {%>
                    <li><a href="show-doctors-controller">Doctor</a></li>
                    <li><a href="show-feedback-controller">Review</a></li>
                    <li><a href="http://localhost:8080/swp-391/page-insert-daily-form.jsp"> Daily report</a></li>
                    <li><a href="http://localhost:8080/swp-391/show-daily-form-controller?action=listAll">View daily report</a></li>
                    <li><a href="view-disease-detail-controller">Check Health Status</a></li>
                    <li><a href="interactive-controller">Add interactive list</a></li>
                    <li><a href="http://localhost:8080/swp-391/interactive-controller?action=listAll">View interactive list</a></li>
                    <li><a href="http://localhost:8080/swp-391/page-insert-travel-schedule.jsp">Add travel schedule</a></li>
                        <%}%>
                </ul>
            </li>
        </ul>

        <div class="copyright">
            <p class="fs-14 font-w200"><strong class="font-w400">
                    <%= account.getRoleName() + " Dashboard"%>
                </strong> � 2022 All Rights Reserved</p>
            <p>Made with <i class="fa fa-heart"></i> by Green Zone Project</p>
        </div>
    </div>
</div>
<!--**********************************
    Side-bar end
***********************************-->
<!--**********************************
    Body header end
***********************************-->
