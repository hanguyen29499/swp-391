<%@page import="entity.Doctor"%>
<%@page import="entity.Patient"%>
<%@page import="entity.FeedbackUser"%>
<%@page import="java.util.List"%>
<%
    List<FeedbackUser> allFeedback = (List<FeedbackUser>) request.getSession().getAttribute("allFeedback");
    List<Patient> allPatient = (List<Patient>) request.getSession().getAttribute("allPatient");
    List<Doctor> allDoctors = (List<Doctor>) request.getSession().getAttribute("allDoctors");
%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body>

        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <!-- row -->
                <div class="container-fluid">
                    <div class="page-titles">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active"><a href="index.jsp">App</a></li>
                            <li class="breadcrumb-item"><a href="show-feedback-controller">Reviews</a></li>
                        </ol>
                    </div>
                    <div class="form-head d-flex mb-3 mb-md-4 align-items-start">
                        <div class="input-group search-area ml-auto d-inline-flex">
                            <input type="text" class="form-control" placeholder="Search here">
                            <div class="input-group-append">
                                <button type="button" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
                            </div>
                        </div>
                        <div class="dropdown ml-2 d-inline-block">
                            <div class="btn btn-primary light btn-rounded dropdown-toggle" data-toggle="dropdown">
                                Newest
                            </div>
                            <div class="dropdown-menu dropdown-menu-left">
                                <a class="dropdown-item" href="javascript:void(0);">Newest</a>
                                <a class="dropdown-item" href="javascript:void(0);">Old</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <ul class="nav nav-pills review-tab">
                                <%if (account.getRoleId() == Constant.ROLE_ADMIN) {%>
                                <li class="nav-item">
                                    <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">All Review</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">Published</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#navpills-3" class="nav-link" data-toggle="tab" aria-expanded="true">Deleted</a>
                                </li>
                                <%} else {%>
                                <li class="nav-item">
                                    <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">Review Of Patient</a>
                                </li>
                                <%}%>
                            </ul>
                            <div class="tab-content">
                                <%if (account.getRoleId() == Constant.ROLE_ADMIN) {%>
                                <div id="navpills-1" class="tab-pane active">
                                    <%for (FeedbackUser feedbackUser : allFeedback) {%>
                                    <div class="card review-table">
                                        <div class="media align-items-center">
                                            <%
                                                Doctor doctor = new Doctor();
                                                for (Patient patient : allPatient) {
                                                    if (feedbackUser.getPatientId() == patient.getPatientId()) {
                                                        for (Doctor getdoctor : allDoctors) {
                                                            if (patient.getDoctorId() == getdoctor.getDoctorId()) {
                                                                doctor.setAccount(getdoctor.getAccount());
                                                                doctor.setBranch(getdoctor.getBranch());
                                                                doctor.setRole(getdoctor.getRole());
                                                                doctor.setDoctorId(getdoctor.getDoctorId());
                                                            }
                                                        }
                                                    }
                                                }
                                            %>
                                            <%if (doctor.getAccount().getGender() == 1) {%>
                                            <img alt="" src="images/doctors/male.jpg" height="43" width="43" class="rounded-circle ml-4">
                                            <%} else {%>
                                            <img alt="" src="images/doctors/female.png" height="43" width="43" class="rounded-circle ml-4">
                                            <%}%>
                                            <div class="media-body d-lg-flex d-block row align-items-center">
                                                <div class="col-xl-4 col-xxl-5 col-lg-6 review-bx">
                                                    <h3 class="fs-20 font-w600 mb-1"><a class="text-black" href="show-doctor-details-controller?doctor_id=<%=doctor.getDoctorId()%>"><%=doctor.getAccount().getFullname()%></a></h3>
                                                    <span class="fs-15 d-block"><%=doctor.getRole()%></span>
                                                    <span class="text-primary d-block font-w600 mt-sm-2 mt-3"><i class="las la-stethoscope scale5 mr-3"></i><%=doctor.getBranch()%></span>
                                                </div>
                                                <div class="col-xl-7 col-xxl-7 col-lg-6 text-dark mb-lg-0 mb-2">
                                                    <%=feedbackUser.getContent()%>
                                                </div>
                                            </div>
                                            <%if (feedbackUser.getVote() == Constant.ONE_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackUser.getVote() == Constant.TWO_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackUser.getVote() == Constant.THREE_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackUser.getVote() == Constant.FOUR_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackUser.getVote() == Constant.FIVE_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <div class="edit ml-auto">
                                                <%if (feedbackUser.getPublish() == 1) {%>
                                                <a href="publish-feedback-controller?id_feedback=<%=feedbackUser.getFeedbackId()%>&publish=0" class="text-primary font-w600 mr-4">UNPUBLISH</a>
                                                <a href="delete-feedback-controller?id_feedback=<%=feedbackUser.getFeedbackId()%>&delete=1" class="text-danger font-w600">DELETE</a>
                                                <%}%>

                                                <%if (feedbackUser.getDelete() == 1) {%>
                                                <h3>Removed !</h3>
                                                <%}%>

                                                <%if (feedbackUser.getPublish() != 1 & feedbackUser.getDelete() != 1) {%>
                                                <a href="publish-feedback-controller?id_feedback=<%=feedbackUser.getFeedbackId()%>&publish=1" class="text-primary font-w600 mr-4">PUBLISH</a>
                                                <a href="delete-feedback-controller?id_feedback=<%=feedbackUser.getFeedbackId()%>&delete=1" class="text-danger font-w600">DELETE</a>
                                                <%}%>
                                            </div>
                                        </div>
                                    </div>
                                    <%}%>
                                </div>
                                <%}%>

                                <%if (account.getRoleId() != Constant.ROLE_ADMIN || account.getRoleId() == Constant.ROLE_ADMIN){%>
                                <div id="navpills-2" class="tab-pane">
                                    <%for (FeedbackUser feedbackPublished : allFeedback) {%>
                                    <%if (feedbackPublished.getPublish() == 1) {%>
                                    <div class="card review-table">
                                        <div class="media align-items-center">
                                            <%
                                                Doctor doctor = new Doctor();
                                                for (Patient patient : allPatient) {
                                                    if (feedbackPublished.getPatientId() == patient.getPatientId()) {
                                                        for (Doctor getdoctor : allDoctors) {
                                                            if (patient.getDoctorId() == getdoctor.getDoctorId()) {
                                                                doctor.setAccount(getdoctor.getAccount());
                                                                doctor.setBranch(getdoctor.getBranch());
                                                                doctor.setRole(getdoctor.getRole());
                                                                doctor.setDoctorId(getdoctor.getDoctorId());
                                                            }
                                                        }
                                                    }
                                                }
                                            %>
                                            <%if (doctor.getAccount().getGender() == 1) {%>
                                            <img alt="" src="images/doctors/male.jpg" height="43" width="43" class="rounded-circle ml-4">
                                            <%} else {%>
                                            <img alt="" src="images/doctors/female.png" height="43" width="43" class="rounded-circle ml-4">
                                            <%}%>
                                            <div class="media-body d-lg-flex d-block row align-items-center">
                                                <div class="col-xl-4 col-xxl-5 col-lg-6 review-bx">
                                                    <h3 class="fs-20 font-w600 mb-1"><a class="text-black" href="show-doctor-details-controller?doctor_id=<%=doctor.getDoctorId()%>"><%=doctor.getAccount().getFullname()%></a></h3>
                                                    <span class="fs-15 d-block"><%=doctor.getRole()%></span>
                                                    <span class="text-primary d-block font-w600 mt-sm-2 mt-3"><i class="las la-stethoscope scale5 mr-3"></i><%=doctor.getBranch()%></span>
                                                </div>
                                                <div class="col-xl-7 col-xxl-7 col-lg-6 text-dark mb-lg-0 mb-2">
                                                    <%=feedbackPublished.getContent()%>
                                                </div>
                                            </div>
                                            <%if (feedbackPublished.getVote() == Constant.ONE_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackPublished.getVote() == Constant.TWO_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackPublished.getVote() == Constant.THREE_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackPublished.getVote() == Constant.FOUR_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackPublished.getVote() == Constant.FIVE_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <div class="edit ml-auto">
                                                <a href="publish-feedback-controller?id_feedback=<%=feedbackPublished.getFeedbackId()%>&publish=0" class="text-primary font-w600 mr-4">UNPUBLISH</a>
                                                <a href="delete-feedback-controller?id_feedback=<%=feedbackPublished.getFeedbackId()%>&delete=1" class="text-danger font-w600">DELETE</a>
                                            </div>
                                        </div>
                                    </div>
                                    <%}%>
                                    <%}%>
                                </div>
                                <%}%>

                                <%if (account.getRoleId() == Constant.ROLE_ADMIN) {%>
                                <div id="navpills-3" class="tab-pane">
                                    <%for (FeedbackUser feedbackDeleted : allFeedback) {%>
                                    <%if (feedbackDeleted.getDelete() == 1) {%>
                                    <div class="card review-table">
                                        <div class="media align-items-center">
                                            <%
                                                Doctor doctor = new Doctor();
                                                for (Patient patient : allPatient) {
                                                    if (feedbackDeleted.getPatientId() == patient.getPatientId()) {
                                                        for (Doctor getdoctor : allDoctors) {
                                                            if (patient.getDoctorId() == getdoctor.getDoctorId()) {
                                                                doctor.setAccount(getdoctor.getAccount());
                                                                doctor.setBranch(getdoctor.getBranch());
                                                                doctor.setRole(getdoctor.getRole());
                                                                doctor.setDoctorId(getdoctor.getDoctorId());
                                                            }
                                                        }
                                                    }
                                                }
                                            %>
                                            <%if (doctor.getAccount().getGender() == 1) {%>
                                            <img alt="" src="images/doctors/male.jpg" height="43" width="43" class="rounded-circle ml-4">
                                            <%} else {%>
                                            <img alt="" src="images/doctors/female.png" height="43" width="43" class="rounded-circle ml-4">
                                            <%}%>
                                            <div class="media-body d-lg-flex d-block row align-items-center">
                                                <div class="col-xl-4 col-xxl-5 col-lg-6 review-bx">
                                                    <h3 class="fs-20 font-w600 mb-1"><a class="text-black" href="show-doctor-details-controller?doctor_id=<%=doctor.getDoctorId()%>"><%=doctor.getAccount().getFullname()%></a></h3>
                                                    <span class="fs-15 d-block"><%=doctor.getRole()%></span>
                                                    <span class="text-primary d-block font-w600 mt-sm-2 mt-3"><i class="las la-stethoscope scale5 mr-3"></i><%=doctor.getBranch()%></span>
                                                </div>
                                                <div class="col-xl-7 col-xxl-7 col-lg-6 text-dark mb-lg-0 mb-2">
                                                    <%=feedbackDeleted.getContent()%>
                                                </div>
                                            </div>
                                            <%if (feedbackDeleted.getVote() == Constant.ONE_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackDeleted.getVote() == Constant.TWO_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackDeleted.getVote() == Constant.THREE_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackDeleted.getVote() == Constant.FOUR_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-gray"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%if (feedbackDeleted.getVote() == Constant.FIVE_STAR) {%>
                                            <div class="media-footer d-sm-flex d-block align-items-center">
                                                <div class="disease mr-5">
                                                    <span class="star-review ml-lg-3 mb-sm-0 mb-2 ml-0 d-inline-block">
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                        <i class="fa fa-star text-orange"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <%}%>
                                            <div class="edit ml-auto">
                                                <a href="publish-feedback-controller?id_feedback=<%=feedbackDeleted.getFeedbackId()%>&publish=0" class="text-primary font-w600 mr-4">Restore</a>
                                                <a href="delete-feedback-controller?id_feedback=<%=feedbackDeleted.getFeedbackId()%>&confirm=accept&delete=0" class="text-danger font-w600" onclick="return confirm('This operation will not be able to recover deleted data, do you still want to continue ?');">DELETE</a>
                                            </div>
                                        </div>
                                    </div>
                                    <%}%>
                                    <%}%>
                                </div>
                                <%}%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--**********************************
                Content body end
            ***********************************-->

            <!--**********************************
                Footer start
            ***********************************-->
            <%@include file="common/footer.jsp" %>
            <!--**********************************
                Footer end
            ***********************************-->


        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <%@include file="common/included-vendors.jsp" %>

        <!--**********************************
            Scripts
        ***********************************-->

    </body>

</html>