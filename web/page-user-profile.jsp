<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body>

        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->


        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
                Content body start
            ***********************************-->
            <div class="content-body">
                <div class="container-fluid">
                    <div class="page-titles">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.jsp">App</a></li>
                            <li class="breadcrumb-item active">
                                <a href="page-user-profile.jsp">Profile</a>
                            </li>

                        </ol>
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12"></div>
                        <div class="container rounded bg-white mt-5 mb-5">
                            <div class="row">
                                <div class="col-md-3 border-right">
                                    <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                        <img class="rounded-circle mt-5" width="150px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                                        <span class="font-weight-bold"><%=account.getFullname()%></span>
                                    </div>
                                </div>
                                <div class="col-md-5 border-right">
                                    <div class="p-3 py-5">
                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                            <h4 class="text-right">Profile</h4>
                                            <form action="edit-profile-controller" method="POST">    
                                                <span class="border px-3 p-1 add-experience"></i><a href="page-user-profile-edit.jsp">Edit</a></span>
                                            </form>
                                        </div>             

                                        <div class="row mt-3">
                                            <div class="col-md-12"><label class="labels">Full Name:&nbsp;&nbsp;</label><%=account.getFullname()%></div><br>
                                            <div class="col-md-12"><label class="labels">Mobile Number:&nbsp;&nbsp;</label><%=account.getPhonenumber()%></div><br>
                                            <div class="col-md-12"><label class="labels">Email:&nbsp;&nbsp;</label><%=account.getEmail()%></div><br>
                                            <div class="col-md-12"><label class="labels">Gender:&nbsp;&nbsp;</label><%= account.getGender() == 1 ? "Male" : "Female"%></div><br>
                                            <div class="col-md-12"><label class="labels">Date of birth:&nbsp;&nbsp;</label><%=account.getDob()%></div><br>
                                            <div class="col-md-12"><label class="labels">Age:&nbsp;&nbsp;</label><%=account.getAge()%></div><br>
                                            <div class="col-md-12"><label class="labels">Address:&nbsp;&nbsp;</label><%=account.getAddress()%></div><br>
                                        </div>
                                    </div>
                                    <div class="mb-3" style="text-align: right">
                                        <a href="page-change-password.jsp">
                                            <button type="button" class="btn btn-success">Change Password</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
            </div>
            <!--**********************************
                Content body end
            ***********************************-->

            <!--**********************************
                Footer start
            ***********************************-->
            <%@include file="common/footer.jsp" %>
            <!--**********************************
                Footer end
            ***********************************-->

        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <script src="./vendor/global/global.min.js"></script>
        <script src="./vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="./vendor/chart.js/Chart.bundle.min.js"></script>
        <script src="./js/custom.min.js"></script>
        <script src="./js/deznav-init.js"></script>
        <script src="./vendor/owl-carousel/owl.carousel.js"></script>

        <!--**********************************
            Scripts
        ***********************************-->
    </body>
</html>
