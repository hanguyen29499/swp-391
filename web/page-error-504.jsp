<!DOCTYPE html>
<html lang="en" class="h-100">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body class="h-100">      
        <div class="authincation h-100">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-md-5">
                        <div class="form-input-content text-center error-page">
                            <h1 class="error-text font-weight-bold">504</h1>
                            <h4><i class="fa fa-times-circle text-danger"></i>Can't not connect to server</h4>
                            <p>You seem to be having trouble finding the server!</p>
                            <div>
                                <a class="btn btn-primary" href="./index.jsp">Back to Home</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <!--**********************************
        Footer start
    ***********************************-->
    <%@include file="common/footer.jsp" %>
    <!--**********************************
        Footer end
    ***********************************-->

    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!-- Required vendors -->
    <%@include file="common/included-vendors.jsp" %>

</html>