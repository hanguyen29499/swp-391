<%@page import="java.sql.ResultSet"%>
<%@page import="entity.Patient"%>
<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body>
        <%
            List<ResultSet> listRs = (List<ResultSet>) request.getAttribute("listRs");
        %>
        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <!--Travel Schedule Table-->
                <div class="container-fluid">
                    <div class="form-head d-flex mb-3 mb-md-4 align-items-start">

                        <form action="show-travel-schedule-controller?action=filter" method="POST">
                            <select class="form-select" aria-label="Default select example" name="filter">
                                <option value="0">All</option>
                                <option value="1">Destination & Time Go</option>
                                <option value="2">Doctor Name</option>
                                <option value="3">Nurse Name</option>
                                <option value="4">View List F1, F2</option>
                            </select>
                            <button type="submit" class="btn btn-success">Filter</button>
                        </form>
                        <div class="input-group search-area ml-auto d-inline-flex mr-3">
                            <input type="text" class="form-control" placeholder="Search here">
                            <div class="input-group-append">
                                <button type="button" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
                            </div>
                        </div>
                        <a href="javascript:void(0);" class="settings-icon"><i class="flaticon-381-settings-2 mr-0"></i></a>
                    </div>


                    <div class="row">
                        <div class="col-xl-12">
                            <div class="table-responsive">
                                <table id="districtTable" class="table table-striped patient-list mb-4 dataTablesCard fs-14">
                                    <thead>
                                        <tr>
                                            <th>
                                                <div class="checkbox text-right align-self-center">
                                                    <div class="custom-control custom-checkbox ">
                                                        <input type="checkbox" class="custom-control-input" id="checkAll" required="">
                                                        <label class="custom-control-label" for="checkAll"></label>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>Patient Name</th>
                                            <th>Email</th>
                                            <th>Phone Number</th>
                                            <th>Destination</th>
                                            <th>Time go</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            for (ResultSet listR : listRs) {
                                                while (listR.next()) {
                                        %>
                                        <tr>
                                            <td>
                                                <div class="checkbox text-right align-self-center">
                                                    <div class="custom-control custom-checkbox ">
                                                        <input type="checkbox" class="custom-control-input" id="customCheckBox1" required="">
                                                        <label class="custom-control-label" for="customCheckBox1"></label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><%=listR.getString("full_name")%></td>
                                            <td><%=listR.getString("email")%></td>
                                            <td><%=listR.getString("phone_number")%></td>
                                            <td><%=listR.getString("destination")%></td>
                                            <td><%=listR.getDate("time_go")%></td>
                                        </tr>
                                        <%}%>
                                        <%}%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--**********************************
                Content body end
            ***********************************-->

            <!--**********************************
                Footer start
            ***********************************-->
            <%@include file="common/footer.jsp" %>
            <!--**********************************
                Footer end
            ***********************************-->


        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <%@include file="common/included-vendors.jsp" %>

        <!--**********************************
            Scripts
        ***********************************-->

        <!-- Datatable -->
        <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

        <script>
            (function ($) {
                var table = $('#cityTable').DataTable({
                    searching: false,
                    paging: true,
                    select: false,
                    info: false,
                    lengthChange: false
                });
                $('#cityTable tbody').on('click', 'tr', function () {
                    table.row(this).data();
                });

            })(jQuery);

            (function ($) {
                var table = $('#districtTable').DataTable({
                    searching: false,
                    paging: true,
                    select: false,
                    info: false,
                    lengthChange: false
                });
                $('#districtTable tbody').on('click', 'tr', function () {
                    table.row(this).data();
                });

            })(jQuery);
        </script>

    </body>
</html>