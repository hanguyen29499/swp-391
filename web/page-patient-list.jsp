<%@page import="entity.Patient"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body>

        <%
            List<Patient> allpatient = (List<Patient>) request.getAttribute("patient");
        %>
        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <!-- row -->
                <div class="container-fluid">
<!--                    <form action="view-patients" method="POST">
                        <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="filtered">
                            <option value="0" <c:if test="${selectBox==0}">selected</c:if>>All</option>
                            <option value="1"<c:if test="${selectBox==1}">selected</c:if>>Body temperature greater than 38 degrees Celsius</option>
                            <option value="2"<c:if test="${selectBox==2}">selected</c:if>>Symptoms of headache</option>
                            <option value="3"<c:if test="${selectBox==3}">selected</c:if>>Symptoms of shortness of breath</option>
                            <option value="4"<c:if test="${selectBox==4}">selected</c:if>>Serious situation</option>
                        </select>
                        <button type="submit" class="btn btn-success">Filter</button>
                    </form>-->
                    <form action="SearchPatientController" method="POST">
                        <div class="form-head d-flex mb-3 mb-md-4 align-items-start">
                            <div class="input-group search-area ml-auto d-inline-flex mr-3">

<!--                                <input type="number" class="form-control" name="filtered" value="${selectBox}" hidden="true">-->
                                <input type="text" class="form-control" name="namePatient" placeholder="Search name patient">


                            </div>
                            <div class="input-group-append">
                                <button type="submit" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-xl-12">
                            <div class="table-responsive">
                                <table id="example5" class="table table-striped patient-list mb-4 dataTablesCard fs-14">
                                    <thead>
                                        <tr>                                       
                                            <th></th>
                                            <th>Patient ID</th>
                                            <th>Patient Name</th>
                                            <th>Doctor Name</th>
                                            <th>Nurse Name</th>  
<!--                                            <th>Status</th>
                                            <th>Status Name</th>-->
                                            <th>Travel Detail</th>
                                            <th>Edit</th>
                                            <th></th>                                                                                      
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%for (Patient p : allpatient) {%>
                                        <tr>
                                            <td>
                                                <div class="checkbox text-right align-self-center">
                                                    <div class="custom-control custom-checkbox ">
                                                        <input type="checkbox" class="custom-control-input" id="customCheckBox1" required="">
                                                        <label class="custom-control-label" for="customCheckBox1"></label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><%=p.getPatientId()%></td>
                                            <td><%=p.getAccount().getFullname()%></td>
                                            <td><%=p.getDoctorName()%></td>
                                            <td><%=p.getNurseName()%></td>
<!--                                            <td>
                                                <span class="text-nowrap">
                                                    <svg class="mr-2" width="9" height="9" viewBox="0 0 9 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <circle cx="4.5" cy="4.5" r="4.5" fill="#FFB800"/>
                                                    </svg>
                                                    <span class="text-warning">Pending</span>
                                                </span>
                                            </td>
                                            <td>AB-002</td>-->
                                            <td>
                                                <a href="detail-travel-schedule-patient?id=<%=p.getPatientId()%>">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M10,0.186c-3.427,0-6.204,2.778-6.204,6.204c0,5.471,6.204,6.806,6.204,13.424c0-6.618,6.204-7.953,6.204-13.424C16.204,2.964,13.427,0.186,10,0.186z M10,14.453c-0.66-1.125-1.462-2.076-2.219-2.974C6.36,9.797,5.239,8.469,5.239,6.39C5.239,3.764,7.374,1.63,10,1.63c2.625,0,4.761,2.135,4.761,4.761c0,2.078-1.121,3.407-2.541,5.089C11.462,12.377,10.66,13.328,10,14.453z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="EditNurseController?patient_id=<%=p.getPatientId()%>">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M17 3C17.2626 2.73735 17.5744 2.52901 17.9176 2.38687C18.2608 2.24473 18.6286 2.17157 19 2.17157C19.3714 2.17157 19.7392 2.24473 20.0824 2.38687C20.4256 2.52901 20.7374 2.73735 21 3C21.2626 3.26264 21.471 3.57444 21.6131 3.9176C21.7553 4.26077 21.8284 4.62856 21.8284 5C21.8284 5.37143 21.7553 5.73923 21.6131 6.08239C21.471 6.42555 21.2626 6.73735 21 7L7.5 20.5L2 22L3.5 16.5L17 3Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </a>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <%}%>
                                    </tbody>
                                </table>
                                <nav aria-label="...">
                                    <ul class="pagination pagination-lg" style="    display: flex;
                                        justify-content: center;">
                                        <c:if test="${listPatients != null}">
                                            <c:forEach begin="1" end="${totalPage}" var="i">
                                                <c:if test="${i == currentPage}">
                                                    <li class="page-item active">
                                                </c:if>
                                                <c:if test="${i != currentPage}">
                                                    <li class="page-item">
                                                </c:if>        
                                                <a class="page-link" href="view-patients?page=${i}">${i}</a>
                                                </li>         
                                            </c:forEach>                                      
                                        </c:if>

                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--**********************************
                Content body end
            ***********************************-->

            <!--**********************************
                Footer start
            ***********************************-->
            <%@include file="common/footer.jsp" %>
            <!--**********************************
                Footer end
            ***********************************-->

            <!--**********************************
            Support ticket button start
            ***********************************-->

            <!--**********************************
               Support ticket button end
            ***********************************-->


        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <%@include file="common/included-vendors.jsp" %>

        <!--**********************************
            Scripts
        ***********************************-->

        <!-- Datatable -->
        <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

        <script>
            (function ($) {

                var table = $('#example5').DataTable({
                    searching: false,
                    paging: true,
                    select: false,
                    //info: false,         
                    lengthChange: false

                });
                $('#example tbody').on('click', 'tr', function () {
                    var data = table.row(this).data();

                });

            })(jQuery);
        </script>

    </body>
</html>