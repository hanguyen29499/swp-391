<%@page import="entity.Patient"%>
<%@page import="java.util.List"%>
<%@page import="entity.Doctor"%>
<%
    List<Doctor> allDoctors = (List<Doctor>) request.getSession().getAttribute("allDoctors");
    List<Patient> allpatient = (List<Patient>)request.getAttribute("patient");
%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body>

        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <!-- row -->
                <div class="container-fluid">
                    <div class="form-head d-flex mb-3 mb-md-4 align-items-start">
                        <%if (account.getRoleId() == Constant.ROLE_ADMIN) {%>
                        <div class="mr-auto d-lg-block">
                            <a href="add-doctor-controller" class="btn btn-primary btn-rounded">+ Add New Doctor</a>
                        </div>
                        <%}%>
                        <%if (account.getRoleId() == Constant.ROLE_PATIENT) {%>
                        <div class="mr-auto d-lg-block">
                            <a href="feedback-of-patient-controller" class="btn btn-primary btn-rounded">+ Create New Feedback</a>
                        </div>
                        <div class="mr-auto d-lg-block">
                            <a href="show-feedback-controller" class="btn btn-primary btn-rounded">View Feedback Send</a>
                        </div>
                        <%}%>
                        <div class="input-group search-area ml-auto d-inline-flex mr-2">
                            <form action="search-doctor-controller" method="POST">
                                <input type="text" class="form-control" placeholder="Search here" name="findName">
                        </div>
                        <div class="input-group-append">
                            <button type="submit" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
                        </div>
                        <select class="form-control" id="val-skill" name="branch" style="width: 150px;">
                            <option value="">Select Branch</option>
                            <option value="virus">Virus</option>
                            <option value="intensive care">Intensive care</option>
                            <option value="recuperate">Recuperate</option>
                            <option value="lung">Lung</option>
                            <option value="sos">SOS</option>
                        </select>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="table-responsive">
                                <table id="example5" class="table shadow-hover  table-bordered mb-4 dataTablesCard fs-14">
                                    <thead>
                                        <tr>
                                            <th>
                                                <div class="checkbox align-self-center">
                                                    <div class="custom-control custom-checkbox ">
                                                        <input type="checkbox" class="custom-control-input" id="checkAll" required="">
                                                        <label class="custom-control-label" for="checkAll"></label>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>ID</th>
                                            <th>Doctor Name</th>
                                            <th>Branch</th>
                                            <th>Specialist</th>
                                            <th>Patient Manager</th>
                                            <th>Status Manager</th>
                                            <th>Contact</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%for (Doctor doctor : allDoctors) {%>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div class="checkbox text-right align-self-center">
                                                        <div class="custom-control custom-checkbox ">
                                                            <input type="checkbox" class="custom-control-input" id="customCheckBox28" required="">
                                                            <label class="custom-control-label" for="customCheckBox28"></label>
                                                        </div>
                                                    </div>
                                                    <%if (doctor.getAccount().getGender() == 1) {%>
                                                    <img alt="" src="images/doctors/male.jpg" height="43" width="43" class="rounded-circle ml-4">
                                                    <%} else {%>
                                                    <img alt="" src="images/doctors/female.png" height="43" width="43" class="rounded-circle ml-4">
                                                    <%}%>
                                                </div>
                                            </td>
                                            <td><%=doctor.getDoctorId()%></td>
                                            <td><a href="show-doctor-details-controller?doctor_id=<%=doctor.getDoctorId()%>"><%=doctor.getAccount().getFullname()%></a></td>
                                            <td><%=doctor.getBranch()%></td>
                                            <td><%=doctor.getRole()%></td>
                                            <%
                                                int totalPatient = 0;
                                                for (Patient p : allpatient) {
                                                    if (p.getDoctorId() == doctor.getDoctorId()) {
                                                        totalPatient++;
                                                    }
                                                }
                                            %>
                                            <td style="text-align: center;"><%=totalPatient%></td>
                                            <td>
                                                <%if (totalPatient > 50) {%>
                                                <span class="text-nowrap">
                                                    <svg class="mr-2" width="9" height="9" viewBox="0 0 9 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <circle cx="4.5" cy="4.5" r="4.5" fill="#F46B68"></circle>
                                                    </svg>
                                                    <span class="text-danger">High</span>
                                                </span>
                                                <%}%>

                                                <%if (totalPatient < 10) {%>
                                                <span class="text-nowrap">
                                                    <svg class="mr-2" width="9" height="9" viewBox="0 0 9 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <circle cx="4.5" cy="4.5" r="4.5" fill="#2BC155"></circle>
                                                    </svg>
                                                    <span class="text-primary">Low</span>
                                                </span>
                                                <%}%>

                                                <%if (10 < totalPatient && totalPatient < 50) {%>
                                                <span class="text-nowrap">
                                                    <svg class="mr-2" width="9" height="9" viewBox="0 0 9 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <circle cx="4.5" cy="4.5" r="4.5" fill="#FFB800"></circle>
                                                    </svg>
                                                    <span class="text-warning">Medium</span>
                                                </span>
                                                <%}%>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span class="font-w500"><%=doctor.getAccount().getPhonenumber()%></span>
                                                    <!--If Logger is Nurse only view can not edit doctor
                                                        Only Admin can edit Doctor-->
                                                    <%if (account.getRoleId() == Constant.ROLE_ADMIN) {%>
                                                    <div class="dropdown ml-auto text-right">
                                                        <div class="btn-link" data-toggle="dropdown" >
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M12 11C11.4477 11 11 11.4477 11 12C11 12.5523 11.4477 13 12 13C12.5523 13 13 12.5523 13 12C13 11.4477 12.5523 11 12 11Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                            <path d="M12 18C11.4477 18 11 18.4477 11 19C11 19.5523 11.4477 20 12 20C12.5523 20 13 19.5523 13 19C13 18.4477 12.5523 18 12 18Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                            <path d="M12 4C11.4477 4 11 4.44772 11 5C11 5.55228 11.4477 6 12 6C12.5523 6 13 5.55228 13 5C13 4.44772 12.5523 4 12 4Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                            </svg>
                                                        </div>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="show-doctor-details-controller?doctor_id=<%=doctor.getDoctorId()%>">View Detail</a>
                                                            <a class="dropdown-item" href="edit-doctor-controller?doctor_id=<%=doctor.getDoctorId()%>">Edit</a>
                                                            <a class="dropdown-item" href="delete-doctor-controller?doctor_id=<%=doctor.getDoctorId()%>" onclick="return confirm('Are you sure you want to Remove?');">Delete</a>
                                                        </div>
                                                    </div>
                                                    <%}%>
                                                </div>
                                            </td>
                                        </tr>
                                        <%}%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--**********************************
                Content body end
            ***********************************-->

            <!--**********************************
                Footer start
            ***********************************-->
            <%@include file="common/footer.jsp" %>
            <!--**********************************
                Footer end
            ***********************************-->


        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <%@include file="common/included-vendors.jsp" %>

        <!--**********************************
            Scripts
        ***********************************-->

        <!-- Datatable -->
        <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

        <script>
                                                                (function ($) {

                                                                    var table = $('#example5').DataTable({
                                                                        searching: false,
                                                                        paging: true,
                                                                        select: false,
                                                                        //info: false,         
                                                                        lengthChange: false

                                                                    });
                                                                    $('#example tbody').on('click', 'tr', function () {
                                                                        var data = table.row(this).data();

                                                                    });

                                                                })(jQuery);
        </script>

    </body>
</html>