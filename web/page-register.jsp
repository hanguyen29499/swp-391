<!DOCTYPE html>
<html lang="en" class="h-100">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body class="h-100">
        <div class="authincation h-100">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-md-6">
                        <div class="authincation-content">
                            <div class="row no-gutters">
                                <div class="col-xl-12">
                                    <div class="auth-form">
                                        <c:if test="${message != null}" >
                                            <p class="box-title m-b-20" style="color: red">${message}</p>     
                                        </c:if>
                                        <h4 class="text-center mb-4">Register</h4>
                                        <form action="register" method="POST">
                                            <div class="form-group">
                                                <label class="mb-1"><strong>Username</strong></label>
                                                <input type="text" class="form-control" required="" placeholder="Enter your username" id="usernameRegister" name="username" onchange="checkUsername()">
                                                <c:if test="${messageUserName != null}" >
                                                    <div class="labels" style="color: red">${messageUserName}</div>     
                                                </c:if>
                                                <p id="errorUsername" class="box-title m-b-20" hidden="true" style="color: red">Username must contain letters or numbers and be more than 5 characters</p> 
                                            </div>
                                            <div class="form-group">
                                                <label class="mb-1"><strong>Password</strong></label>
                                                <input type="password" class="form-control" required="" placeholder="Enter your password" id="passwordRegister" name="password" onchange="checkPassword()">
                                                <p id="errorPassword" class="box-title m-b-20" hidden="true" style="color: red">Password must contain letters or numbers and be more than 8 characters</p> 
                                            </div>
                                            <div class="form-group">
                                                <label class="mb-1"><strong>Confirm Password</strong></label>
                                                <input type="password" class="form-control" required="" placeholder="Enter your confirm password" id="confirmPassword" onchange="checkConfirmPassword()">
                                                <p id="errorConfirmPassword" class="box-title m-b-20" hidden="true" style="color: red">Confirm password must match password</p> 
                                            </div>
                                            <div class="form-group">
                                                <label class="mb-1"><strong>Email</strong></label>
                                                <input type="email" class="form-control" required="" placeholder="Enter your email" name="email">     
                                                <c:if test="${messageEmail != null}" >
                                                    <div class="labels" style="color: red">${messageEmail}</div>     
                                                </c:if>
                                            </div>
                                            <div class="form-group">
                                                <label class="mb-1"><strong>Full Name</strong></label>
                                                <input type="text" class="form-control" required="" placeholder="Enter your full name" name="fullname">          
                                            </div>
                                            <div class="form-group ">
                                                <label class="mb-1"><strong>Phone Number</strong></label>
                                                <input class="form-control" type="text" required="" placeholder="Phone number" id="phoneNumber" onchange="checkPhone()" name="phoneNumber"> 
                                                <c:if test="${messagePhone != null}" >
                                                    <div class="labels" style="color: red">${messagePhone}</div>     
                                                </c:if>
                                                <p id="errorPhone" class="box-title m-b-20" hidden="true" style="color: red">Phone must be number and length 10-11</p> 
                                            </div>
                                            <div class="form-group">
                                                <label class="mb-1"><strong>Birth Day</strong></label>
                                                <input type="date" class="form-control" required="" placeholder="Enter your birth dat" name="birthDay">          
                                            </div>
                                            <div class="form-group">
                                                <label class="mb-1"><strong>Gender</strong></label><br>                                                
                                                <select class="form-select" aria-label="Default select example" name="gender">
                                                    <option value="1" selected>Male</option>
                                                    <option value="0">Female</option>
                                                </select>
                                            </div>
                                            <div class="form-group ">
                                                <label class="mb-1"><strong>Address</strong></label>
                                                <input class="form-control" type="text" required="" placeholder="Address" id="address" name="address"> 
                                            </div>
                                            <div class="text-center">
                                                <button id="registerBtn" type="submit" class="btn btn-primary btn-block">Register</button>
                                            </div>
                                        </form>
                                        <div class="new-account mt-3">
                                            <p>I already have an account <a class="text-primary" href="page-login.jsp">Login</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--**********************************
            Footer start
        ***********************************-->

        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <script>
            function checkUsername() {
                var regexUsername = "^[a-z0-9]{5,50}$";
                var usernameRegister = document.getElementById("usernameRegister").value;
                if (!usernameRegister.match(regexUsername)) {
                    document.getElementById("errorUsername").hidden = false;
                    document.getElementById("registerBtn").disabled = true;
                } else {
                    document.getElementById("errorUsername").hidden = true;
                    document.getElementById("registerBtn").disabled = false;
                }
            }
            function checkPassword() {
                var regexPassword = "^[a-zA-Z0-9]{8,50}$";
                var passwordRegister = document.getElementById("passwordRegister").value;
                if (!passwordRegister.match(regexPassword)) {
                    document.getElementById("errorPassword").hidden = false;
                    document.getElementById("registerBtn").disabled = true;
                } else {
                    document.getElementById("errorPassword").hidden = true;
                    document.getElementById("registerBtn").disabled = false;
                }
            }
            function checkConfirmPassword() {
                var passwordRegister = document.getElementById("passwordRegister").value;
                var confirmPassword = document.getElementById("confirmPassword").value;
                if (confirmPassword !== passwordRegister) {
                    document.getElementById("errorConfirmPassword").hidden = false;
                    document.getElementById("registerBtn").disabled = true;
                } else {
                    document.getElementById("errorConfirmPassword").hidden = true;
                    document.getElementById("registerBtn").disabled = false;
                }
            }
            function checkPhone() {
                var regexPhone = "^[0-9]{10,11}$";
                var phoneNumber = document.getElementById("phoneNumber").value;
                if (!phoneNumber.match(regexPhone)) {
                    document.getElementById("errorPhone").hidden = false;
                    document.getElementById("registerBtn").disabled = true;
                } else {
                    document.getElementById("errorPhone").hidden = true;
                    document.getElementById("registerBtn").disabled = false;
                }
            }
        </script>  
        <%@include file="common/included-vendors.jsp" %>

    </body>

</html>