<%-- 
    Document   : Edit Interactive List
    Created on : Feb 19, 2022, 12:14:24 AM
    Author     : pc
--%>



<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <title>ERES - Bootstrap Admin Dashboard</title>
        <%@include file="common/header.jsp" %>
    </head>
    <body>

        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <% ResultSet rs2 = (ResultSet) request.getAttribute("rs2");
            %>
            <div class="content-body">
                <div class="container-fluid">
                    <div class="page-titles">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.jsp">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Form</a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Update interactive person information</a></li>
                        </ol>
                    </div>
                    <!-- row -->
                    <form action="interactive-controller?action=update" method="Post" >
                         <%if (rs2.next()) {%>
                        <div class="row">
                            <div class="col-xl-12 col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Basic Information </h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="basic-form">     
                                            <div class="form-group">
                                                Interative Id (*): <input type="text" class="form-control input-rounded" name="interactiveId" value="<%=rs2.getInt(1)%>" readonly="readonly" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="basic-form">     
                                            <div class="form-group">
                                                Interative name (*): <input type="text" class="form-control input-rounded" name="interactiveName" value="<%=rs2.getString(2)%>" required="" placeholder="<%=rs2.getString(2)%>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12">
                            <div class="card">                          
                                <div class="card-body">
                                    <div class="basic-form">     
                                        <div class="form-group">
                                            Relationship (*): <input type="text" class="form-control input-rounded" name="relationship" value="<%=rs2.getString(3)%>" required="" placeholder="<%=rs2.getString(3)%>">
                                        </div>                                 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-12 col-lg-12">
                            <div class="card">                          
                                <div class="card-body">
                                    <div class="basic-form">     
                                        <div class="form-group">
                                            Body Temperature (*): <input type="text" class="form-control input-rounded" name="bodyTemperature" required="" placeholder="Please input Body Temperature">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Does the person contact with you have any of the following symptoms? (*)</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3">
                                                <input type="checkbox" class="custom-control-input" name="headache" value="headache"  id="customCheckBox1" >
                                                <label class="custom-control-label" for="customCheckBox1">Headache</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-info">
                                                <input type="checkbox" class="custom-control-input" name="fever" value="fever" id="customCheckBox2" >
                                                <label class="custom-control-label" for="customCheckBox2">Fever </label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                <input type="checkbox" class="custom-control-input" name="shortness" value="shortness" id="customCheckBox3" >
                                                <label class="custom-control-label" for="customCheckBox3">Shortness_breath</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-warning">
                                                <input type="checkbox" class="custom-control-input" name="taste" value="taste" id="customCheckBox4">
                                                <label class="custom-control-label" for="customCheckBox4">Loss of Taste</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                <input type="checkbox" class="custom-control-input" name="diarrhea" value="diarrhea" id="customCheckBox5">
                                                <label class="custom-control-label" for="customCheckBox5">Diarrhea</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-warning">
                                                <input type="checkbox" class="custom-control-input" name="skinRash" value="skinRash" id="customCheckBox6">
                                                <label class="custom-control-label" for="customCheckBox6">Skin Rash</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                <input type="checkbox" class="custom-control-input" name="confusion" value="confusion" id="customCheckBox7">
                                                <label class="custom-control-label" for="customCheckBox7">Confusion</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-info">
                                                <input type="checkbox" class="custom-control-input" name="cough" value="cough" id="customCheckBox8">
                                                <label class="custom-control-label" for="customCheckBox8">Cough</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-warning">
                                                <input type="checkbox" class="custom-control-input" name="tired" value="tired" id="customCheckBox9">
                                                <label class="custom-control-label" for="customCheckBox9">Tired</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                <input type="checkbox" class="custom-control-input" name="soreThroat" value="soreThroat" id="customCheckBox10">
                                                <label class="custom-control-label" for="customCheckBox10">Sore throat</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                <input type="checkbox" class="custom-control-input" name="vomiting" value="vomiting" id="customCheckBox11">
                                                <label class="custom-control-label" for="customCheckBox11">Vomiting</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                <input type="checkbox" class="custom-control-input" name="anorexia" value="anorexia" id="customCheckBox12">
                                                <label class="custom-control-label" for="customCheckBox12">Anorexia</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                <input type="checkbox" class="custom-control-input" name="seriousSituation" value="seriousSituation" id="customCheckBox13">
                                                <label class="custom-control-label" for="customCheckBox13">Serious Situation</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>(*): Obligatory</p>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input type="submit" class="btn btn-primary"  value="Send" name="submit" onclick="return confirm('Are you sure you want to send')">
                            </div>
                        </div>
                        <%}%>
                    </form>
                </div>                   
                <!--**********************************
                    Content body end
                ***********************************-->


                <!--**********************************
                    Footer start
                ***********************************-->
                <%@include file="common/footer.jsp" %>
                <!--**********************************
                    Footer end
                ***********************************-->

                <!--**********************************
                   Support ticket button start
                ***********************************-->

                <!--**********************************
                   Support ticket button end
                ***********************************-->


            </div>
            <!--**********************************
                Main wrapper end
            ***********************************-->

            <!-- Required vendors -->
            <%@include file="common/included-vendors.jsp" %>
            <!--**********************************
           Scripts
       ***********************************-->

            <!-- Datatable -->
            <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

            <script>
                                    (function ($) {

                                        var table = $('#example5').DataTable({
                                            searching: false,
                                            paging: true,
                                            select: false,
                                            //info: false,         
                                            lengthChange: false

                                        });
                                        $('#example tbody').on('click', 'tr', function () {
                                            var data = table.row(this).data();

                                        });

                                    })(jQuery);
            </script>


    </body>
</html>
