<%-- 
    Document   : InsertTravelSchedule
    Created on : Feb 19, 2022, 10:44:28 PM
    Author     : pc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>ERES - Bootstrap Admin Dashboard</title>
        <%@include file="common/header.jsp" %>
    </head>
   <body>

        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <div class="container-fluid">
                    <div class="page-titles">
                        <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="index.jsp">Home</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Form</a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Travel Schedule Date <%=java.time.LocalDate.now()%></a></li>
                        </ol>
                    </div>
                    <!-- row -->
                    <form action="TravelScheduleController?action=insert" method="Post">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Basic Information </h4>
                                    </div>
                                </div>
                            </div>
                             <div class="col-xl-12 col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="basic-form">                                  

                                            <div class="form-group">
                                                Departure : <input type="text" class="form-control input-default " name="departure" required="" placeholder="Please input departure">
                                            </div>
                                              <div class="form-group">
                                                  Destination : <input type="text" class="form-control input-default " name="destination" required="" placeholder="Please input destination">
                                              </div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Travel Time</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-xl-4 mb-3">
                                                <div class="example">
                                                    <input type="date" class="form-control" required="" placeholder="Enter time go" name="timego">      
                                                </div>
                                            </div>                   
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Travel information*(Optional)</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="custom-control custom-checkbox mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="customCheckBox1" >
                                                    <label class="custom-control-label" for="customCheckBox1">Airplane</label>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="custom-control custom-checkbox mb-3 checkbox-info">
                                                    <input type="checkbox" class="custom-control-input" id="customCheckBox2" >
                                                    <label class="custom-control-label" for="customCheckBox2">Ships </label>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                    <input type="checkbox" class="custom-control-input" id="customCheckBox3" >
                                                    <label class="custom-control-label" for="customCheckBox3">Car</label>
                                                </div>
                                            </div>

                                            <div class="col-4">
                                                <div class="custom-control custom-checkbox mb-3 input-rounded ">
                                                    <input type="text" class="form-control" name="other" placeholder="Other ">
                                                </div>
                                            </div>
                                        </div>                              
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <input type="submit" class="btn btn-primary"  value="Send" name="submit" onclick="return confirm('Are you sure you want Add Travel Schedule?');">
                                </div>
                            </div>
                        </div>
                    </form>   
                    <form action="TravelScheduleController?action=listAll" method="Post">
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <input type="submit" class="btn btn-primary"  value="View Travel Schedule Report" name="listAll">
                            </div>
                        </div>
                    </form>
                </div>                         
 
            <!--**********************************
                Content body end
            ***********************************-->


            <!--**********************************
                Footer start
            ***********************************-->
            <%@include file="common/footer.jsp"%>
            <!--**********************************
                Footer end
            ***********************************-->


        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!--**********************************
            Scripts
        ***********************************-->
        <!-- Required vendors -->
        <%@include file="common/included-vendors.jsp" %>

          <!--**********************************
        Scripts
    ***********************************-->

    <!-- Datatable -->
    <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

    <script>
                                                            (function ($) {

                                                                var table = $('#example5').DataTable({
                                                                    searching: false,
                                                                    paging: true,
                                                                    select: false,
                                                                    //info: false,         
                                                                    lengthChange: false

                                                                });
                                                                $('#example tbody').on('click', 'tr', function () {
                                                                    var data = table.row(this).data();

                                                                });

                                                            })(jQuery);
    </script>


        <!-- Daterangepicker -->
        <!-- momment js is must -->
        <script src="./vendor/moment/moment.min.js"></script>
        <script src="./vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- clockpicker -->
        <script src="./vendor/clockpicker/js/bootstrap-clockpicker.min.js"></script>
        <!-- asColorPicker -->
        <script src="./vendor/jquery-asColor/jquery-asColor.min.js"></script>
        <script src="./vendor/jquery-asGradient/jquery-asGradient.min.js"></script>
        <script src="./vendor/jquery-asColorPicker/js/jquery-asColorPicker.min.js"></script>
        <!-- Material color picker -->
        <script src="./vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
        <!-- pickdate -->
        <script src="./vendor/pickadate/picker.js"></script>
        <script src="./vendor/pickadate/picker.time.js"></script>
        <script src="./vendor/pickadate/picker.date.js"></script>



        <!-- Daterangepicker -->
        <script src="./js/plugins-init/bs-daterange-picker-init.js"></script>
        <!-- Clockpicker init -->
        <script src="./js/plugins-init/clock-picker-init.js"></script>
        <!-- asColorPicker init -->
        <script src="./js/plugins-init/jquery-asColorPicker.init.js"></script>
        <!-- Material color picker init -->
        <script src="./js/plugins-init/material-date-picker-init.js"></script>
        <!-- Pickdate -->
        <script src="./js/plugins-init/pickadate-init.js"></script>



    </body>
</html>
