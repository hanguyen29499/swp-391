<!DOCTYPE html>
<html lang="en" class="h-100">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body class="h-100">
        <div class="authincation h-100">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-md-6">
                        <div class="authincation-content">
                            <div class="row no-gutters">
                                <div class="col-xl-12">
                                    <div class="auth-form">

                                        <c:if test="${errorLogin != null}" >
                                            <h3 class="box-title m-b-20" style="color: red">${errorLogin}</h3>     
                                        </c:if>
                                        <c:if test="${message != null}" >
                                            <h3 class="box-title m-b-20" style="color: green">${message}</h3>     
                                        </c:if>

                                        <h4 class="text-center mb-4">Login</h4>

                                        <form action="login-controller" method="POST">
                                            <div class="form-group">
                                                <label class="mb-1"><strong>Username</strong></label>
                                                <input type="text" class="form-control" required="" placeholder="Enter your username" id="usernameLogin" name="username" onchange="checkUsername()">
                                                <p id="errorUsername" class="box-title m-b-20" hidden="true" style="color: red">Username must contain letters or numbers and be more than 5 characters</p> 
                                            </div>
                                            <div class="form-group">
                                                <label class="mb-1"><strong>Password</strong></label>
                                                <input type="password" class="form-control" required="" placeholder="Enter your password" id="passwordLogin" name="password" onchange="checkPassword()">
                                                <p id="errorPassword" class="box-title m-b-20" hidden="true" style="color: red">Password must contain letters or numbers and be more than 8 characters</p> 
                                            </div>
                                            <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                                <div class="form-group">
                                                    <a href="page-forgot-password.jsp">Forgot Password?</a>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary btn-block">Login</button>
                                            </div>
                                        </form>

                                        <div class="new-account mt-3">
                                            <p>Don't have an account? <a class="text-primary" href="page-register.jsp">Register</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--**********************************
            Footer start
        ***********************************-->
        <%@include file="common/footer.jsp" %>
        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <script>
            function checkUsername() {
                var regexUsername = "^[a-z0-9]{5,50}$";
                var usernameLogin = document.getElementById("usernameLogin").value;
                if (!usernameLogin.match(regexUsername)) {
                    document.getElementById("errorUsername").hidden = false;
                } else {
                    document.getElementById("errorUsername").hidden = true;
                }
            }
            function checkPassword() {
                var regexPassword = "^[a-zA-Z0-9]{8,50}$";
                var passwordLogin = document.getElementById("passwordLogin").value;
                if (!passwordLogin.match(regexPassword)) {
                    document.getElementById("errorPassword").hidden = false;
                } else {
                    document.getElementById("errorPassword").hidden = true;
                }
            }
        </script>    
        <%@include file="common/included-vendors.jsp" %>

    </body>

</html>