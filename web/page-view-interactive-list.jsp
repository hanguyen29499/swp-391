<%-- 
    Document   : View Interactive List
    Created on : Feb 19, 2022, 11:02:41 PM
    Author     : pc
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>ERES - Bootstrap Admin Dashboard </title>
        <%@include file="common/header.jsp" %>
    </head>
    <body>
        <%
            ResultSet rs = (ResultSet) request.getAttribute("rs");
        %>
        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->

            <div class="content-body">
                <!-- row -->

                <div class="container-fluid">
                    <div class="form-head d-flex mb-3 mb-md-4 align-items-start">
                        <form action="interactive-controller?action=insert">
                            <div class="mr-auto d-none d-lg-block">
                                <input type="submit" class="btn btn-primary btn-rounded" value="Add New">
                            </div>
                        </form>
                    </div>
                                        <form action="interactive-controller?action=listAll" method="POST">
                                            <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="filtered">
                                                <option value="all" >All</option>
                                                <option value="safe" >F1 has no common symptoms</option>
                                                <option value="suspend" >F1 has common symptoms</option>                          
                                            </select>
                                            <button type="submit" class="btn btn-success">Filter</button>
                                        </form>
                                        <form action="interactive-controller?action=listAll" method="POST">
                                            <div class="form-head d-flex mb-3 mb-md-4 align-items-start">
                                                <div class="input-group search-area ml-auto d-inline-flex mr-3">
<!--                                                    <input type="hidden" class="form-control" name="filtered" value="${selectBox}" hidden="true">-->
                                                    <input type="text" class="form-control" name="nameSearch" placeholder="Search name ">
                                                </div>
                                                <div class="input-group-append">
                                                    <button type="submit" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
                                                </div>
                                            </div>
                                        </form>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="table-responsive">
                                <table id="example5" class="table table-striped patient-list mb-4 dataTablesCard fs-14">
                                    <thead>
                                    <h1 align="center">Interactive List</h1>
                                    <tr>
                                        <th>Interactive ID</th>
                                        <th>Interactive Name</th>
                                        <th>Relationship</th>
                                        <th>Status</th> 
                                        <th>Note</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <%while(rs.next()){ %>
                                        <tr>
                                            <td><%=rs.getInt(1)%></td>
                                            <td><%=rs.getString(2)%></td>
                                            <td><%=rs.getString(3)%></td>
                                            <%if(rs.getString(4).equals("Safe")){%>
                                            <td><p style="color: rgb(0,255,0)"> <%=rs.getString(4)%></p></td>
                                            <%}%>
                                            <%if(rs.getString(4).equals("Suspected of having covid 19 !")){%>
                                            <td><p style="color: rgb(255,0,0)"> <%=rs.getString(4)%></p></td>
                                            <%}%> 
                                            <td>F1</td>
                                            <td>                    
                                                <a href="interactive-controller?action=update&accountId=<%=account.getAccountId()%>&interactiveId=<%=rs.getInt(1)%>">
                                                    <svg  width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M17 3C17.2626 2.73735 17.5744 2.52901 17.9176 2.38687C18.2608 2.24473 18.6286 2.17157 19 2.17157C19.3714 2.17157 19.7392 2.24473 20.0824 2.38687C20.4256 2.52901 20.7374 2.73735 21 3C21.2626 3.26264 21.471 3.57444 21.6131 3.9176C21.7553 4.26077 21.8284 4.62856 21.8284 5C21.8284 5.37143 21.7553 5.73923 21.6131 6.08239C21.471 6.42555 21.2626 6.73735 21 7L7.5 20.5L2 22L3.5 16.5L17 3Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </a>                                                 
                                            </td>     
                                        </tr>
                                        <%}%>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--**********************************
                Content body end
            ***********************************-->

            <!--**********************************
                Footer start
            ***********************************-->
            <%@include file="common/footer.jsp" %>
            <!--**********************************
                Footer end
            ***********************************-->

        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!--**********************************
            Scripts
        ***********************************-->
        <!-- Required vendors -->
        <script src="./vendor/global/global.min.js"></script>
        <script src="./vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="./vendor/chart.js/Chart.bundle.min.js"></script>
        <script src="./js/custom.min.js"></script>
        <script src="./js/deznav-init.js"></script>
        <script src="./vendor/owl-carousel/owl.carousel.js"></script>

        <!-- Datatable -->
        <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

        <script>
            (function ($) {

                var table = $('#example5').DataTable({
                    searching: false,
                    paging: true,
                    select: false,
                    //info: false,         
                    lengthChange: false

                });
                $('#example tbody').on('click', 'tr', function () {
                    var data = table.row(this).data();

                });

            })(jQuery);
        </script>
    </body>
</html>
