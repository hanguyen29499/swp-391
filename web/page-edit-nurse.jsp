<%@page import="entity.Patient"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>

<%
    List<Account> nurseAccounts = (List<Account>) request.getSession().getAttribute("nurseAccounts");
    List<Patient> patient = (List<Patient>) request.getAttribute("patient");
%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body>
        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <div class="container-fluid">
                    <div class="row page-titles">
                        <div class="col-sm-6 p-md-0">
                            <div class="welcome-text">
                                <h4>Hi, Welcome!</h4>
                            </div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Nurse</h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-validation">
                                        <form class="form-valide" action="EditNurseController" method="POST">
                                            <%for (Patient p : patient) {%>
                                            <input type="hidden" name="patient" value="<%=p.getPatientId()%>"
                                                   <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group row">
                                                        <label class="col-lg-4 col-form-label" for="val-branch">Patient</label>
                                                        <div class="col-lg-6">
                                                            <input type="text" class="form-control" id="val-branch" value="<%=p.getAccount().getFullname()%>" disabled>

                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-lg-4 col-form-label" for="val-role">Current Nurse</label>
                                                        <div class="col-lg-6">
                                                            <input type="text" class="form-control" id="val-role" value="<%=p.getNurseName()%>" disabled>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xl-6">
                                                    <div class="form-group row">
                                                        <label class="col-lg-4 col-form-label" for="val-branch">Doctor</label>
                                                        <div class="col-lg-6">
                                                            <input type="text" class="form-control" id="val-branch" value="<%=p.getDoctorName()%>" disabled>
                                                        </div>
                                                    </div>
                                                    <%}%>
                                                    <div class="form-group row">
                                                        <label class="col-lg-4 col-form-label" for="val-gender">List Of nurse</label>
                                                        <div class="col-lg-6">
                                                            <select class="form-control" id="val-skill" name="nurse" >
                                                                <% for (Account a : nurseAccounts) {%>
                                                                <option  value="<%=a.getAccountId()%> <%=a.getFullname()%>"><%=a.getFullname()%></option>
                                                                <%}%>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-8 ml-auto">
                                                        <button type="submit" name="submit" class="btn btn-primary" onclick="return confirm('Are you sure you want Add?');">Change Nurse</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        <%@include file="common/footer.jsp" %>
        <!--**********************************
            Footer end
        ***********************************-->


    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!-- Required vendors -->
    <%@include file="common/included-vendors.jsp" %>

    <!--**********************************
        Scripts
    ***********************************-->

    <!-- Datatable -->
    <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

    <script>
                                                            (function ($) {

                                                                var table = $('#example5').DataTable({
                                                                    searching: false,
                                                                    paging: true,
                                                                    select: false,
                                                                    //info: false,         
                                                                    lengthChange: false

                                                                });
                                                                $('#example tbody').on('click', 'tr', function () {
                                                                    var data = table.row(this).data();

                                                                });

                                                            })(jQuery);
    </script>

</body>
</html>
