<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body>

        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->


        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
                Content body start
            ***********************************-->
            <div class="content-body">
                <div class="container-fluid">
                    <div class="page-titles">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.jsp">App</a></li>
                            <li class="breadcrumb-item"><a href="page-user-profile.jsp">Profile</a></li>
                            <li class="breadcrumb-item active"><a href="page-user-profile-edit.jsp">Edit</a></li>
                        </ol>
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12"></div>                 
                        <div class="container rounded bg-white mt-5 mb-5">
                            <div class="row">
                                <div class="col-md-3 border-right">
                                    <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                        <img class="rounded-circle mt-5" width="150px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                                        <span class="font-weight-bold"><%=account.getFullname()%></span>
                                    </div>
                                </div>
                                <div class="col-md-5 border-right">
                                    <div class="p-3 py-5">
                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                            <h4 class="text-right">Profile Settings</h4>
                                        </div>
                                        <form action="edit-profile-controller" method="POST">     
                                            <div class="row mt-3">
                                                <div class="col-md-12"><label class="labels">Full Name</label><input type="text" class="form-control" placeholder="Enter full name" value="<%= account.getFullname()%>" name="fullname"></div>
                                                <div class="col-md-12"><label class="labels">Mobile Number</label><input type="text" class="form-control" placeholder="Enter phone number" value="<%= account.getPhonenumber()%>" name="phone" disabled>
                                                    <c:if test="${messagePhone != null}" >
                                                        <div class="labels" style="color: red">${messagePhone}</div>     
                                                    </c:if>
                                                </div>
                                                <div class="col-md-12"><label class="labels">Email</label><input type="text" class="form-control" placeholder="Enter email" value="<%= account.getEmail()%>" name="email" disabled>
                                                    <c:if test="${messageEmail != null}" >
                                                        <div class="labels" style="color: red">${messageEmail}</div>     
                                                    </c:if>
                                                </div>
                                                <div class="form-group ">
                                                    <div class="col-md-12" style="margin-top: 10px"><label class="labels">Gender</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input type="radio" name="gender" value="1" <%= account.getGender() == 1 ? "checked" : ""%>>Male &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input type="radio" name="gender" value="0" <%= account.getGender() == 0 ? "checked" : ""%>>Female
                                                    </div>
                                                </div>
                                                <div class="col-md-12"><label class="labels">Date of birth</label><input type="text" class="form-control" placeholder="Enter date of birth" value="<%= account.getDob()%>" name="dob"></div>
                                                <div class="col-md-12"><label class="labels">Age</label><input type="text" class="form-control" placeholder="Enter age" value="<%= account.getAge()%>" name="age" disabled></div>
                                                <div class="col-md-12"><label class="labels">Address</label><input type="text" class="form-control" placeholder="Enter address" value="<%=account.getAddress()%>" name="address"></div>
                                            </div>                                   
                                            <div class="mt-5 text-center"><input class="btn btn-primary profile-button" type="submit" value="Save Profile"/></div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        <%@include file="common/footer.jsp" %>
        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <%@include file="common/included-vendors.jsp" %>

        <!--**********************************
            Scripts
        ***********************************-->

    </body>

</html>