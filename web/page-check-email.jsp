<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" class="h-100">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body class="h-100">
        <div class="authincation h-100">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-md-6">
                        <div class="authincation-content">
                            <div class="row no-gutters">
                                <div class="col-xl-12">
                                    <div class="auth-form">
                                        <h4 class="text-center mb-2">Enter your email</h4>
                                        <c:if test="${error!=null}">
                                            <p class="text-center alert-danger">${error}</p>
                                        </c:if>
                                        <form action="reset-password" method="POST">
                                            <div class="form-group">
                                                <label>
                                                    <strong>
                                                        Please click this link to verify your account: 
                                                    </strong>
                                                </label><br/>
                                                <a href="https://gmail.com">Verify your account</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--**********************************
            Footer start
        ***********************************-->
        <%@include file="common/footer.jsp" %>
        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <%@include file="common/included-vendors.jsp" %>

    </body>

</html>