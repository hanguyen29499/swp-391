<%-- 
    Document   : EditDailyForm
    Created on : Feb 23, 2022, 3:10:59 PM
    Author     : pc
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.LocalDate"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <title>ERES - Bootstrap Admin Dashboard</title>
        <%@include file="common/header.jsp" %>
    </head>
    <body>

        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <% ResultSet rs2 = (ResultSet) request.getAttribute("rs2");
            %>
            <div class="content-body">
                <div class="container-fluid">
                    <div class="page-titles">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Form</a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Update Health Report Date <%=java.time.LocalDate.now()%></a></li>
                        </ol>
                    </div>
                    <!-- row -->
                    <form action="edit-daily-form-controller?action=update" method="Post"> 
                        <%if (rs2.next()) {%>
                        <div class="row">
                            <div class="col-xl-12 col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Update Information </h4>
                                    </div>

                                    <div class="card-body">
                                        <div class="basic-form"> 
                                            <div class="form-group">
                                                Declaration time (*): <input type="date" class="form-control input-rounded" name="declarationTime" value="<%=rs2.getDate(1)%>" required="" >
                                            </div>
                                            <div class="form-group">
                                                Account: <input type="text" class="form-control input-default " name="accountId"  value="<%=account.getAccountId()%>" readonly="readonly">
                                            </div>

                                            <div class="form-group">
                                                Full Name: <input type="text" class="form-control input-default " name="fullname"  value="<%=account.getFullname()%>" placeholder="<%=account.getFullname()%>" readonly="readonly">
                                            </div>
                                            <div class="form-group">
                                                Phone Number: <input type="text" class="form-control input-rounded" name="phonenumber"  value="<%=account.getPhonenumber()%>" placeholder="<%=account.getPhonenumber()%>" readonly="readonly">
                                            </div>
                                            <div class="form-group">
                                                Gender: <input type="text" class="form-control input-rounded" name="gender"  value="<%=account.getGender()%>" placeholder="<%=account.getGender()%>" readonly="readonly">
                                            </div>
                                            <div class="form-group">
                                                Age: <input type="text" class="form-control input-rounded" name="age"  value="<%=account.getAge()%>" placeholder="<%=account.getAge()%>" readonly="readonly">
                                            </div>
                                            <div class="form-group">
                                                Role : <input type="text" class="form-control input-rounded" name="roleid" value="<%=account.getRoleName()%>" placeholder="<%=account.getRoleName()%>" readonly="readonly" >
                                            </div>
                                            <p> Note : Gender (1: Male ; 0: Female)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12">
                            <div class="card">                          
                                <div class="card-body">
                                    <div class="basic-form">     
                                        <div class="form-group">
                                            Disease Detail Id: <input type="text" class="form-control input-rounded" name="diseasedetailid" value="<%=rs2.getInt(2)%>" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-12 col-lg-12">
                            <div class="card">                          
                                <div class="card-body">
                                    <div class="basic-form">     
                                        <div class="form-group">
                                            Body Temperature: <input type="text" class="form-control input-rounded" name="bodyTemperature" value="<%=rs2.getInt(3)%>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Have you experienced any of the following symptoms</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3">
                                                <input type="checkbox" class="custom-control-input" name="headache" value="<%=rs2.getInt(4)%>"  id="customCheckBox1" >
                                                <label class="custom-control-label" for="customCheckBox1">Headache</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-info">
                                                <input type="checkbox" class="custom-control-input" name="fever" value="<%=rs2.getInt(5)%>" id="customCheckBox2" >
                                                <label class="custom-control-label" for="customCheckBox2">Fever </label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                <input type="checkbox" class="custom-control-input" name="shortness" value="<%=rs2.getInt(6)%>" id="customCheckBox3" >
                                                <label class="custom-control-label" for="customCheckBox3">Shortness_breath</label>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="custom-control custom-checkbox mb-3 checkbox-warning">
                                                <input type="checkbox" class="custom-control-input" name="taste" value="<%=rs2.getInt(7)%>" id="customCheckBox4">
                                                <label class="custom-control-label" for="customCheckBox4">Loss of Taste</label>
                                            </div>
                                        </div>
                                                <div class="col-4">
                                                    <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                        <input type="checkbox" class="custom-control-input" name="diarrhea" value="<%=rs2.getInt(8)%>" id="customCheckBox5">
                                                        <label class="custom-control-label" for="customCheckBox5">Diarrhea</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="custom-control custom-checkbox mb-3 checkbox-warning">
                                                        <input type="checkbox" class="custom-control-input" name="skinRash" value="<%=rs2.getInt(9)%>" id="customCheckBox6">
                                                        <label class="custom-control-label" for="customCheckBox6">Skin Rash</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                        <input type="checkbox" class="custom-control-input" name="confusion" value="<%=rs2.getInt(10)%>" id="customCheckBox7">
                                                        <label class="custom-control-label" for="customCheckBox7">Confusion</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="custom-control custom-checkbox mb-3 checkbox-info">
                                                        <input type="checkbox" class="custom-control-input" name="cough" value="<%=rs2.getInt(11)%>" id="customCheckBox8">
                                                        <label class="custom-control-label" for="customCheckBox8">Cough</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="custom-control custom-checkbox mb-3 checkbox-warning">
                                                        <input type="checkbox" class="custom-control-input" name="tired" value="<%=rs2.getInt(12)%>" id="customCheckBox9">
                                                        <label class="custom-control-label" for="customCheckBox9">Tired</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                        <input type="checkbox" class="custom-control-input" name="soreThroat" value="<%=rs2.getInt(13)%>" id="customCheckBox10">
                                                        <label class="custom-control-label" for="customCheckBox10">Sore throat</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                        <input type="checkbox" class="custom-control-input" name="vomiting" value="<%=rs2.getInt(14)%>" id="customCheckBox11">
                                                        <label class="custom-control-label" for="customCheckBox11">Vomiting</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                        <input type="checkbox" class="custom-control-input" name="anorexia" value="<%=rs2.getInt(15)%>" id="customCheckBox12">
                                                        <label class="custom-control-label" for="customCheckBox12">Anorexia</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="custom-control custom-checkbox mb-3 checkbox-success">
                                                        <input type="checkbox" class="custom-control-input" name="seriousSituation" value="<%=rs2.getInt(16)%>" id="customCheckBox13">
                                                        <label class="custom-control-label" for="customCheckBox13">Serious Situation</label>
                                                    </div>
                                                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Vacxin Status(*)</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <table>
                                            <tr>
                                                <th> <input type="radio" name="injection" value="<%=rs2.getInt(17)%>" >Had 1 injection  </th>
                                            </tr>
                                            <tr>
                                                <th><input type="radio" name="injection" value="<%=rs2.getInt(17)%>" >Had 2 injection </th>
                                            </tr>
                                            <tr>
                                                <th><input type="radio" name="injection" value="<%=rs2.getInt(17)%>"  >  Had 3 injection </th>
                                            </tr>
                                            <tr>
                                                <th> <input type="radio" name="injection" value="<%=rs2.getInt(17)%>"> Had no injection</th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12">
                            <div class="card">                          
                                <div class="card-body">
                                    <div class="basic-form">     
                                        <div class="form-group">
                                            Patient Status (*): <input type="text" class="form-control input-rounded" name="patientStatus" value="<%=rs2.getString(18)%>" placeholder="<%=rs2.getString(18)%>" >
                                        </div>
                                        <p>Please enter your current status as OK (if you are not sick), F0 (with Covid), F1,2,3,.... (If you have been in contact with someone infected with Covid)
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>                

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <input type="submit" class="btn btn-primary"  value="update" name="submit">
                            </div>
                        </div>
                        <%}%>
                    </form>
                </div>
                <!--**********************************
                    Content body end
                ***********************************-->


                <!--**********************************
                    Footer start
                ***********************************-->
                <%@include file="common/footer.jsp" %>
                <!--**********************************
                    Footer end
                ***********************************-->

                <!--**********************************
                   Support ticket button start
                ***********************************-->

                <!--**********************************
                   Support ticket button end
                ***********************************-->


            </div>
            <!--**********************************
                Main wrapper end
            ***********************************-->

            <!--**********************************
                Scripts
            ***********************************-->
            <!-- Required vendors -->
            <%@include file="common/included-vendors.jsp" %>
    </body>
</html>
