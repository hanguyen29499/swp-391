<%-- 
    Document   : ViewDailyForm
    Created on : Feb 19, 2022, 11:02:41 PM
    Author     : pc
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>ERES - Bootstrap Admin Dashboard </title>
        <%@include file="common/header.jsp" %>
    </head>
    <body>
        <%
            ResultSet rs = (ResultSet) request.getAttribute("rs");
           String date = (String)request.getAttribute("date");          
        %>
        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->

            <div class="content-body">
                <!-- row -->

                <div class="container-fluid">
                    <div class="form-head d-flex mb-3 mb-md-4 align-items-start">
                        <form action="DailyFormController" method="Post">
                            <div class="mr-auto d-none d-lg-block">
                                <input type="submit" class="btn btn-primary btn-rounded" value="Add New Report">
                            </div>
                        </form>
                        <form action="view-disease-detail-controller" method="Post">
                            <div class="mr-auto d-none d-lg-block">
                                <input type="submit" class="btn btn-primary btn-rounded" value="View patient health statistics">
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="table-responsive">
                                <table id="example5" class="table table-striped patient-list mb-4 dataTablesCard fs-14">
                                    <thead>
                                        <tr>
                                            <th>Date create</th>
                                            <th>Account ID</th>
                                            <th>Patient Name</th>
                                            <th>Role</th>
                                            <th>Disease Detail ID</th>
                                            <th>Body Temperature</th>
                                            <th>Headache</th>
                                            <th>Fever</th>
                                            <th>Shortness Breath</th>
                                            <th>Loss Of Taste</th>
                                            <th>Diarrhea</th>
                                            <th>Skin Rash</th>
                                            <th>Confusion</th>
                                            <th>Cough</th>
                                            <th>Tired</th>
                                            <th>Sore Throat</th>
                                            <th>Vomiting</th>
                                            <th>Anorexia</th>
                                            <th>Serious Situation</th>
                                            <th>Vaccine Status</th>                                                    
                                            <th>Patient Status</th>                                                 
                                            <th></th>
                                        </tr>
                                    </thead>
                                   <tbody>
                                        <% while(rs.next()) {%>
                                        <tr>
                                            <td><%=rs.getDate(1)%></td>                                                                    
                                            <td><%=account.getAccountId()%></td>
                                            <td><%=account.getFullname()%></td>
                                            <td><%=account.getRoleName()%></td>
                                            <td><%=rs.getInt(2)%></td>
                                            <td><%=rs.getInt(3)%></td>
                                            <td><%= (rs.getInt(4) == 1 ? "Yes" : "No")%></td>
                                            <td><%=(rs.getInt(5) == 1 ? "Yes" : "No")%></td>
                                            <td><%=(rs.getInt(6) == 1 ? "Yes" : "No")%></td>
                                            <td><%=(rs.getInt(7) == 1 ? "Yes" : "No")%></td>
                                            <td><%= (rs.getInt(8) == 1 ? "Yes" : "No")%></td>
                                            <td><%=(rs.getInt(9) == 1 ? "Yes" : "No")%></td>
                                            <td><%=(rs.getInt(10) == 1 ? "Yes" : "No")%></td>
                                            <td><%=(rs.getInt(11) == 1 ? "Yes" : "No")%></td>
                                            <td><%= (rs.getInt(12) == 1 ? "Yes" : "No")%></td>
                                            <td><%=(rs.getInt(13) == 1 ? "Yes" : "No")%></td>
                                            <td><%=(rs.getInt(14) == 1 ? "Yes" : "No")%></td>
                                            <td><%=(rs.getInt(15) == 1 ? "Yes" : "No")%></td>
                                            <td><%=(rs.getInt(16) == 1 ? "Yes" : "No")%></td>
                                            <td><%=rs.getInt(17)%></td>
                                            <td><%=rs.getString(18)%></td>                                        
                                            <td>                    
                                                <a href="edit-daily-form-controller?action=update&accountId=<%=account.getAccountId()%>&diseasedetailid=<%=rs.getInt(2)%>">
                                                    <svg  width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M17 3C17.2626 2.73735 17.5744 2.52901 17.9176 2.38687C18.2608 2.24473 18.6286 2.17157 19 2.17157C19.3714 2.17157 19.7392 2.24473 20.0824 2.38687C20.4256 2.52901 20.7374 2.73735 21 3C21.2626 3.26264 21.471 3.57444 21.6131 3.9176C21.7553 4.26077 21.8284 4.62856 21.8284 5C21.8284 5.37143 21.7553 5.73923 21.6131 6.08239C21.471 6.42555 21.2626 6.73735 21 7L7.5 20.5L2 22L3.5 16.5L17 3Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </a>                                                 
                                            </td>                                          
                                        </tr>
                                        <%}%>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--**********************************
                Content body end
            ***********************************-->

            <!--**********************************
                Footer start
            ***********************************-->
            <%@include file="common/footer.jsp" %>
            <!--**********************************
                Footer end
            ***********************************-->

        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!--**********************************
            Scripts
        ***********************************-->
        <!-- Required vendors -->
        <script src="./vendor/global/global.min.js"></script>
        <script src="./vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="./vendor/chart.js/Chart.bundle.min.js"></script>
        <script src="./js/custom.min.js"></script>
        <script src="./js/deznav-init.js"></script>
        <script src="./vendor/owl-carousel/owl.carousel.js"></script>

        <!-- Datatable -->
        <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

        <script>
            (function ($) {

                var table = $('#example5').DataTable({
                    searching: false,
                    paging: true,
                    select: false,
                    //info: false,         
                    lengthChange: false

                });
                $('#example tbody').on('click', 'tr', function () {
                    var data = table.row(this).data();

                });

            })(jQuery);
        </script>
    </body>
</html>
