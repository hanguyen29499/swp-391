<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body>
        <%
            List<Account> allAccounts = (List<Account>) request.getAttribute("allAccounts");
        %>
        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <!-- row -->
                <div class="container-fluid">
                    <form action="show-user-controller" method="POST">
                        <select class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="filtered">
                            <option value="0"<c:if test="${selectBox==0}">selected</c:if>>All</option>
                            <option value="1"<c:if test="${selectBox==1}">selected</c:if>>Admin</option>
                            <option value="2"<c:if test="${selectBox==2}">selected</c:if>>Doctor</option>
                            <option value="3"<c:if test="${selectBox==3}">selected</c:if>>Nurse</option>
                            <option value="4"<c:if test="${selectBox==4}">selected</c:if>>Patient</option>
                            </select>
                            <button type="submit" class="btn btn-success">Filter</button>
                        </form>
                        <form action="SearchUserController" method="POST"> 
                            <div class="form-head d-flex mb-3 mb-md-4 align-items-start">
                                <!--<div class="mr-auto d-none d-lg-block">
                                    <a href="page-patient-details.html" class="btn btn-primary btn-rounded">+ Add New</a>
                                </div>-->

                                <div class="input-group search-area ml-auto d-inline-flex mr-3">
                                    <input type="text" name="search" class="form-control" placeholder="Search here">
                                    <div class="input-group-append">
                                        <button type="submit" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
                                    </div>
                                </div>

                                <a href="javascript:void(0);" class="settings-icon"><i class="flaticon-381-settings-2 mr-0"></i></a>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="table-responsive">
                                    <table id="example5" class="table table-striped patient-list mb-4 dataTablesCard fs-14">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div class="checkbox text-right align-self-center">
                                                        <div class="custom-control custom-checkbox ">
                                                            <input type="checkbox" class="custom-control-input" id="checkAll" required="">
                                                            <label class="custom-control-label" for="checkAll"></label>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th>Account ID</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Gender</th>
                                                <th>BirthDay</th>
                                                <th>Age</th>
                                                <th>Role</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <% for (Account a : allAccounts) {%>
                                        <%if (a.getStatus() == 1) {%>
                                        <tr>
                                            <td>
                                                <div class="checkbox text-right align-self-center">
                                                    <div class="custom-control custom-checkbox ">
                                                        <input type="checkbox" class="custom-control-input" id="customCheckBox1" required="">
                                                        <label class="custom-control-label" for="customCheckBox1"></label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><%=a.getAccountId()%></td>
                                            <td><%= a.getFullname()%></td>
                                            <td><%=a.getPhonenumber()%></td>
                                            <td><%=a.getEmail()%></td>
                                            <td><%=(a.getGender() == 1 ? "Male" : "Female")%></td>
                                            <td><%=a.getDob()%></td>
                                            <td><%=a.getAge()%></td>
                                            <td><%=a.getRoleName()%></td>
                                            <td>
                                                <div class="dropdown ml-auto text-right">
                                                    <div class="btn-link" data-toggle="dropdown">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M12 11C11.4477 11 11 11.4477 11 12C11 12.5523 11.4477 13 12 13C12.5523 13 13 12.5523 13 12C13 11.4477 12.5523 11 12 11Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                        <path d="M12 18C11.4477 18 11 18.4477 11 19C11 19.5523 11.4477 20 12 20C12.5523 20 13 19.5523 13 19C13 18.4477 12.5523 18 12 18Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                        <path d="M12 4C11.4477 4 11 4.44772 11 5C11 5.55228 11.4477 6 12 6C12.5523 6 13 5.55228 13 5C13 4.44772 12.5523 4 12 4Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                        </svg>
                                                    </div>
                                                    <%if (a.getRoleId() != 1) {%>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="ChangeStatusController?account_id=<%=a.getAccountId()%>&status=0" onclick="return confirm('Are you sure you want to Deactive?');">Deactive</a>
                                                    </div>
                                                    <%}%>
                                                </div>
                                            </td>
                                        </tr>
                                        <%}%>
                                        <%}%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--**********************************
                Content body end
            ***********************************-->

            <!--**********************************
                Footer start
            ***********************************-->
            <%@include file="common/footer.jsp" %>
            <!--**********************************
                Footer end
            ***********************************-->


        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <%@include file="common/included-vendors.jsp" %>

        <!--**********************************
            Scripts
        ***********************************-->

        <!-- Datatable -->
        <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

        <script>
                                                            (function ($) {

                                                                var table = $('#example5').DataTable({
                                                                    searching: false,
                                                                    paging: true,
                                                                    select: false,
                                                                    info: false,
                                                                    lengthChange: false

                                                                });
                                                                $('#example tbody').on('click', 'tr', function () {
                                                                    var data = table.row(this).data();
                                                                });

                                                            })(jQuery);
        </script>

    </body>
</html>
