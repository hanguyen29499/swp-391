<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" class="h-100">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body class="h-100">
        <div class="authincation h-100">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-md-6">
                        <div class="authincation-content">
                            <div class="row no-gutters">
                                <div class="col-xl-12">
                                    <div class="auth-form">
                                        <h4 class="text-center mb-2">Reset Password</h4>
                                        
                                        <form action="set-password?id=${id}" method="POST">
                                            <div class="form-group">
                                                
                                                <label><strong>New Password</strong></label>
                                                <input type="password" name="password" class="form-control mb-3" placeholder="Enter new password..." required="1" id="passwordRegister" onchange="checkPassword()">
                                                <p id="errorPassword" class="box-title m-b-20" hidden="true" style="color: red">Password must contain letters or numbers and be more than 8 characters</p> 

                                                <label><strong>Retype Password</strong></label>
                                                <input type="password" name="confirmPassword" class="form-control" placeholder="Retype password..." required="1" id="confirmPassword" onchange="checkConfirmPassword()">
                                                <p id="errorConfirmPassword" class="box-title m-b-20" hidden="true" style="color: red">Confirm password must match password</p> 
                                            </div>
                                            <div class="text-center">
                                                <button id="submitBtn" type="submit" class="btn btn-primary btn-block">SUBMIT</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--**********************************
            Footer start
        ***********************************-->
        <%@include file="common/footer.jsp" %>
        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->

        <script>
            function checkPassword() {
                var regexPassword = "^[a-zA-Z0-9]{8,50}$";
                var passwordRegister = document.getElementById("passwordRegister").value;
                if (!passwordRegister.match(regexPassword)) {
                    document.getElementById("errorPassword").hidden = false;
                    document.getElementById("submitBtn").disabled = true;
                } else {
                    document.getElementById("errorPassword").hidden = true;
                    document.getElementById("submitBtn").disabled = false;
                }
            }
            function checkConfirmPassword() {
                var passwordRegister = document.getElementById("passwordRegister").value;
                var confirmPassword = document.getElementById("confirmPassword").value;
                if (confirmPassword !== passwordRegister) {
                    document.getElementById("errorConfirmPassword").hidden = false;
                    document.getElementById("submitBtn").disabled = true;
                } else {
                    document.getElementById("errorConfirmPassword").hidden = true;
                    document.getElementById("submitBtn").disabled = false;
                }
            }
        </script>  
        <%@include file="common/included-vendors.jsp" %>

    </body>

</html>