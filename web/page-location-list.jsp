<%@page import="entity.District"%>
<%@page import="entity.City"%>
<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <%@include file="common/header.jsp" %>
    </head>

    <body>
        <%
            List<City> cities = (List<City>) request.getAttribute("cities");
        %>
        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Body header start
            ***********************************-->
            <%@include file="common/body-header.jsp" %>
            <!--**********************************
                Body header end
            ***********************************-->

            <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <!-- City table -->
                <div class="container-fluid">
                    <div class="form-head d-flex mb-3 mb-md-4 align-items-start">
                        <div class="mr-auto d-none d-lg-block">
                            <a href="city-add-controller" class="btn btn-primary btn-rounded">+ Add New City</a>
                        </div>

                        <form action="city-search-by-name-controller" method="POST">
                            <div class="input-group search-area ml-auto d-inline-flex mr-3">
                                <input type="text" class="form-control" placeholder="Search here"name="search_key">
                                <div class="input-group-append">
                                    <button type="button" class="input-group-text"><i class="flaticon-381-search-2"></i></button>
                                </div>
                            </div>
                        </form>
                        <a href="javascript:void(0);" class="settings-icon"><i class="flaticon-381-settings-2 mr-0"></i></a>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="table-responsive">
                                <table id="cityTable" class="table table-striped patient-list mb-4 dataTablesCard fs-14">
                                    <thead>
                                        <tr>
                                            <!--<th>
                                                <div class="checkbox text-right align-self-center">
                                                    <div class="custom-control custom-checkbox ">
                                                        <input type="checkbox" class="custom-control-input" id="checkAll" required="">
                                                        <label class="custom-control-label" for="checkAll"></label>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>ID</th>-->
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Number district</th>
                                            <th>Population</th>
                                            <th>Number patient registered</th>
                                            <th>Number F0 patient</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <% for (int i = 0; i < cities.size(); i++) {%>
                                        <tr>
                                            <!--<td>
                                                <div class="checkbox text-right align-self-center">
                                                    <div class="custom-control custom-checkbox ">
                                                        <input type="checkbox" class="custom-control-input" id="customCheckBox1" required="">
                                                        <label class="custom-control-label" for="customCheckBox1"></label>
                                                    </div>
                                                </div>
                                            </td>-->
                                            <th><%=i + 1%></th>
                                            <td><%=cities.get(i).getName()%></td>
                                            <td><%=cities.get(i).getDistricts().size() > 1 ? cities.get(i).getDistricts().size() + " districts" : cities.get(i).getDistricts().size() + " district"%></td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td> 
                                                <div class="dropdown ml-auto text-right">
                                                    <div class="btn-link" data-toggle="dropdown" >
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M12 11C11.4477 11 11 11.4477 11 12C11 12.5523 11.4477 13 12 13C12.5523 13 13 12.5523 13 12C13 11.4477 12.5523 11 12 11Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                        <path d="M12 18C11.4477 18 11 18.4477 11 19C11 19.5523 11.4477 20 12 20C12.5523 20 13 19.5523 13 19C13 18.4477 12.5523 18 12 18Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                        <path d="M12 4C11.4477 4 11 4.44772 11 5C11 5.55228 11.4477 6 12 6C12.5523 6 13 5.55228 13 5C13 4.44772 12.5523 4 12 4Z" stroke="#3E4954" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                        </svg>
                                                    </div>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="city-detail-controller?city_id=<%=cities.get(i).getId()%>">View Detail</a>
                                                        <!--<a class="dropdown-item" href="city-edit-controller?city_id=<%=cities.get(i).getId()%>">Edit</a>-->
                                                        <a class="dropdown-item" href="city-delete-controller?city_id=<%=cities.get(i).getId()%>" onclick="return confirm('Are you sure you want to Remove?');">Delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <%}%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--**********************************
                Content body end
            ***********************************-->

            <!--**********************************
                Footer start
            ***********************************-->
            <%@include file="common/footer.jsp" %>
            <!--**********************************
                Footer end
            ***********************************-->


        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->

        <!-- Required vendors -->
        <%@include file="common/included-vendors.jsp" %>

        <!--**********************************
            Scripts
        ***********************************-->

        <!-- Data table -->
        <script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>

        <script>
                                                            (function ($) {
                                                                var table = $('#cityTable').DataTable({
                                                                    searching: false,
                                                                    paging: true,
                                                                    select: false,
                                                                    info: false,
                                                                    lengthChange: false
                                                                });
                                                                $('#cityTable tbody').on('click', 'tr', function () {
                                                                    table.row(this).data();
                                                                });

                                                            })(jQuery);
        </script>

    </body>
</html>