/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import common.CommonUtil;
import entity.Account;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAOAccount;

/**
 *
 * @author phant
 */
@WebServlet(name = "ChangePasswordController", urlPatterns = {"/change-password-controller"})
public class ChangePasswordController extends HttpServlet {

    private final DAOAccount daoAccount;

    public ChangePasswordController() {
        daoAccount = new DAOAccount();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");

        Account account = (Account) request.getSession().getAttribute("account");
        String password = request.getParameter("password");
        String oldPasswordInput = request.getParameter("oldPass");
        String oldPasswordCurrent = daoAccount.getAccountPassword(account.getUserName());
        String str = null;

        if (new CommonUtil().encrypt(oldPasswordInput).equals(oldPasswordCurrent)) {
            daoAccount.changePassword(account.getAccountId(), password);
            Account acc = daoAccount.findByLoginId(account.getUserName(), password);
            request.getSession().setAttribute("account", acc);
            
            CommonUtil.dispatch(request, response, "page-user-profile.jsp");
        } else {
            str = "The old password input does not match!";
            
        }
        if(str!=null){
            request.setAttribute("error", str);
            CommonUtil.dispatch(request, response, "page-change-password.jsp");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ChangePasswordController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ChangePasswordController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
