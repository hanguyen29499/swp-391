package controller;

import common.CommonUtil;
import entity.Doctor;
import entity.Patient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAODoctor;
import model.DAOPatient;

/**
 *
 * @author ADMINS
 */
@WebServlet(name = "SearchDoctorController", urlPatterns = {"/search-doctor-controller"})
public class SearchDoctorController extends HttpServlet {

    private final DAODoctor daoDoctor;
    private final DAOPatient daoPatient;
    
    public SearchDoctorController() {
        daoDoctor = new DAODoctor();
        daoPatient = new DAOPatient();
    }
    
    public static List<Doctor> findDoctorByName(String name, String branch, List<Doctor> list){
        List<Doctor> findDoctors = new ArrayList<>();
        for(int i = 0; i < list.size(); i++){
            if(name.equals("")){
                if(list.get(i).getBranch().toLowerCase().contains(branch)){
                    findDoctors.add(list.get(i));
                }
            }else if(branch.equals("")){
                if (list.get(i).getAccount().getFullname().toLowerCase().contains(name)) {
                    findDoctors.add(list.get(i));
                }
            }else if(list.get(i).getBranch().toLowerCase().contains(branch) && list.get(i).getAccount().getFullname().toLowerCase().contains(name)){
                findDoctors.add(list.get(i));
            }
        }
        return findDoctors;
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Doctor> doctors = daoDoctor.getDoctors();
        
        String name = request.getParameter("findName");
        String branch = request.getParameter("branch");

        if(name.equals("") && branch.equals("")){
            CommonUtil.dispatch(request, response, "show-doctors-controller");
        }
        
        List<Doctor> findDoctor = findDoctorByName(name.toLowerCase(), branch.toLowerCase(), doctors);
        request.getSession().setAttribute("allDoctors", findDoctor);
        
        List<Patient> patient = daoPatient.getPatient();
        request.setAttribute("patient", patient);
        CommonUtil.dispatch(request, response, "page-doctor-list.jsp");
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}