/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import common.CommonUtil;
import entity.Account;
import java.io.IOException;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAOAccount;
import model.DAOPatient;

/**
 *
 * @author Asus
 */
@WebServlet(name = "RegisterPatientController", urlPatterns = {"/register"})
public class RegisterController extends HttpServlet {

    private final DAOAccount daoAccount;
    private final DAOPatient daoPatient;

    public RegisterController() {
        daoAccount = new DAOAccount();
        daoPatient = new DAOPatient();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        CommonUtil.dispatch(request, response, "page-register.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        String message;
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String fullName = request.getParameter("fullname");
        String phoneNumber = request.getParameter("phoneNumber");
        String dob = request.getParameter("birthDay");
        Date birthDay = Date.valueOf(dob);
        // Get current time
        long millis = System.currentTimeMillis();
        Date now = new java.sql.Date(millis);
        int age = CommonUtil.getDiffYears(birthDay, now);
        int gender = Integer.parseInt(request.getParameter("gender"));
        String address = request.getParameter("address");

        boolean pass = true;
        // check exits
        if (daoAccount.findByUserName(username) != null) {
            request.setAttribute("messageUserName", "Username existed!");
            pass = false;
        }

        if (daoAccount.findByEmail(email) != null) {
            request.setAttribute("messageEmail", "Email existed!");
            pass = false;
        }

        if (daoAccount.findByPhoneNumber(phoneNumber) != null) {
            request.setAttribute("messagePhone", "Phone number existed!");
            pass = false;
        }

        if (pass) {
            try {
                Account newAccount = new Account(username, CommonUtil.encrypt(password), fullName, phoneNumber, gender, dob, age, 4, email, address);
                daoAccount.register(newAccount);
                Account accountInfo = daoAccount.findByUserName(username);
                daoPatient.create(accountInfo.getAccountId());
                message = "Register succsess, please login !";
                request.setAttribute("message", message);
                CommonUtil.dispatch(request, response, "page-login.jsp");
            } catch (Exception ex) {
                Logger.getLogger(RegisterController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            CommonUtil.dispatch(request, response, "page-register.jsp");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
