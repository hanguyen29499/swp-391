/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dto.ViewPatient;
import entity.Account;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.DAODoctor;

/**
 *
 * @author Asus
 */
@WebServlet(name = "ViewPatientsController", urlPatterns = {"/view-patients"})
public class ViewPatientsController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    private void getPatients(HttpServletRequest request, HttpServletResponse response, Integer filtered, String namePatient){
        try {
            DAODoctor daoDotor = new DAODoctor();
            HttpSession ses = request.getSession();
            Account account = (Account) ses.getAttribute("account");
            if (Objects.isNull(account)) {
                request.getRequestDispatcher("login-controller").forward(request, response);
            }
            String pageString = request.getParameter("page");
            Integer currentPage = 1;
            if (Objects.nonNull(pageString)) {
                currentPage = Integer.parseInt(pageString);
            }
            Integer doctorId = daoDotor.getDoctorIdByAccountId(account.getAccountId());
            List<ViewPatient> listPatients = daoDotor.getTenPatients(currentPage, doctorId, filtered, namePatient);
            request.setAttribute("listPatients", listPatients);
            //Paging
            Integer total = daoDotor.getTotalPatientsByDoctorId(doctorId, filtered, namePatient);
            int totalPage = total / 10;
            if (total % 10 != 0) {
                totalPage++;
            }
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("totalPage", totalPage);
            request.setAttribute("selectBox", filtered);
            request.getRequestDispatcher("doctor-view-patient.jsp").forward(request, response);
        } catch (ServletException | IOException ex) {
            Logger.getLogger(ViewPatientsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        getPatients(request, response, 0, "");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        Integer filtered = Integer.parseInt(request.getParameter("filtered"));
        String namePatient = request.getParameter("namePatient");
        if(Objects.isNull(namePatient)){
            namePatient = "";
        }
        getPatients(request, response, filtered, namePatient);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
