/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import common.CommonUtil;
import entity.Account;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAOAccount;

/**
 *
 * @author NguyenCT
 */
@WebServlet(name = "EditProfileController", urlPatterns = {"/edit-profile-controller"})
public class EditProfileController extends HttpServlet {
    
    private final DAOAccount daoAccount;
    
    public EditProfileController() {
        daoAccount = new DAOAccount();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");

//        boolean pass = true;
//        // check exits
//        if (daoAccount.findByEmail(email) != null) {
//            request.setAttribute("messageEmail", "Email existed!");
//            pass = false;
//        }
//
//        if (daoAccount.findByPhoneNumber(phoneNumber) != null) {
//            request.setAttribute("messagePhone", "Phone number existed!");
//            pass = false;
//        }
//        if (pass) {
        String phoneNumber = request.getParameter("phone");
        String email = request.getParameter("email");
        String fullName = request.getParameter("fullname");
        int gender = Integer.parseInt(request.getParameter("gender"));
        String dob = request.getParameter("dob");
        Date birthDay = Date.valueOf(dob);
        // Get current time
        long millis = System.currentTimeMillis();
        Date now = new java.sql.Date(millis);
        int age = CommonUtil.getDiffYears(birthDay, now);
        String address = request.getParameter("address");
        Account account = (Account) request.getSession().getAttribute("account");
        account.setFullname(fullName);
        account.setPhonenumber(phoneNumber);
        account.setGender(gender);
        account.setDob(dob);
        account.setAge(age);
        account.setEmail(email);
        account.setAddress(address);
        
        daoAccount.update(account);
        request.getSession().setAttribute("account", daoAccount.findById(account.getAccountId()));
        CommonUtil.dispatch(request, response, "page-user-profile.jsp");
//        } else {
//            CommonUtil.dispatch(request, response, "page-user-profile-edit.jsp");
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(EditProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(EditProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
