/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import common.CommonUtil;
import entity.FeedbackUser;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAOFeedback;

/**
 *
 * @author ADMINS
 */
@WebServlet(name = "DeleteFeedbackController", urlPatterns = {"/delete-feedback-controller"})
public class DeleteFeedbackController extends HttpServlet {

    private final DAOFeedback daoFeedback;
    
    public DeleteFeedbackController(){
        daoFeedback = new DAOFeedback();
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int feedbackID = Integer.parseInt(request.getParameter("id_feedback"));
        int delete = Integer.parseInt(request.getParameter("delete"));
        
        FeedbackUser feedbackUser = new FeedbackUser();
        feedbackUser.setDelete(delete);
        feedbackUser.setPublish(0);
        feedbackUser.setFeedbackId(feedbackID);
        daoFeedback.updateStatus(feedbackUser);
        
        if(delete == 0){
            String confirm = request.getParameter("confirm");
            if(confirm.equals("accept")){
                daoFeedback.removeById(feedbackID);
            }
        }
        
        CommonUtil.dispatch(request, response, "show-feedback-controller");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
