/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.sun.org.apache.xalan.internal.lib.ExsltDatetime;
import common.CommonUtil;
import entity.Account;
import entity.TravelSchedule;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAO;
import model.DAOAdmin;
import model.DAOPatient;
import model.DAOTravelSchedule;

/**
 *
 * @author pc
 */
@WebServlet(name = "TravelScheduleController", urlPatterns = {"/TravelScheduleController"})
public class TravelScheduleController extends HttpServlet {

    private final DAO dao;
    private final DAOTravelSchedule daoTravelSche;

    public TravelScheduleController() {
        dao = new DAO();
        daoTravelSche = new DAOTravelSchedule();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.text.ParseException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            if (action == null) {
                CommonUtil.dispatch(request, response, "page-insert-travel-schedule.jsp");
            }
            if (action.equals("listAll")) {
                Account account = (Account) request.getSession().getAttribute("account");
                ResultSet rs1 = dao.getTravelScheduleInformation(account.getAccountId());
                request.setAttribute("rs1", rs1);
                CommonUtil.dispatch(request, response, "page-view-travel-schedule.jsp");
            }
            if (action.equals("insert")) {
                String submit = request.getParameter("submit");
                if (submit == null) {
                    CommonUtil.dispatch(request, response, "page-insert-travel-schedule.jsp");
                } else {
                    Account account = (Account) request.getSession().getAttribute("account");
                    String time = request.getParameter("timego");
                    Date timego = Date.valueOf(time);

                    String departure = request.getParameter("departure");
                    String destination = request.getParameter("destination");

                    TravelSchedule newTravelSchedule = new TravelSchedule();
                    newTravelSchedule.setDeparture(departure);
                    newTravelSchedule.setDestination(destination);
                    newTravelSchedule.setTime(timego);

                    dao.addTravelSchedule(Integer.parseInt(account.getAccountId()+""), newTravelSchedule);

                    response.sendRedirect("TravelScheduleController");
                }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
