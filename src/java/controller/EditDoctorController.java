package controller;

import common.CommonUtil;
import entity.Account;
import entity.Doctor;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAODoctor;

@WebServlet(name = "EditDoctorController", urlPatterns = {"/edit-doctor-controller"})
public class EditDoctorController extends HttpServlet {

    private final DAODoctor daoDoctor;

    public EditDoctorController() {
        daoDoctor = new DAODoctor();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String doctor_id = request.getParameter("doctor_id");
        Doctor doctor = daoDoctor.getByID(Integer.parseInt(doctor_id));
        request.getSession().setAttribute("doctor", doctor);

        response.sendRedirect("page-form-edit-infor-doctor.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String fullname = request.getParameter("fullname");
        String phonenumber = request.getParameter("phonenumber");
        String branch = request.getParameter("branch");
        int gender = Integer.parseInt(request.getParameter("gender"));
 
        Doctor doctor = (Doctor) request.getSession().getAttribute("doctor");
        doctor.setBranch(branch);

        Account doctorAccount = doctor.getAccount();
        doctorAccount.setFullname(fullname);
        doctorAccount.setPhonenumber(phonenumber);
        doctorAccount.setGender(gender);

        request.getSession().setAttribute("doctor", doctor);

        daoDoctor.update(doctor, doctorAccount);

        CommonUtil.dispatch(request, response, "show-doctors-controller");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
