/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import common.CommonUtil;
import entity.FeedbackUser;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAOFeedback;

/**
 *
 * @author ADMINS
 */
@WebServlet(name = "EditFeedbackController", urlPatterns = {"/edit-feedback-controller"})
public class EditFeedbackController extends HttpServlet {
    
    private final DAOFeedback daoFeedback;
    
    public EditFeedbackController(){
        daoFeedback = new DAOFeedback();
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int feedbackID = Integer.parseInt(request.getParameter("id_feedback"));
        FeedbackUser feedbackUser = daoFeedback.getByID(feedbackID);
        request.getSession().setAttribute("feedback", feedbackUser);
        CommonUtil.dispatch(request, response, "page-from-edit-feedback-of-patient.jsp");
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int feedbackID = Integer.parseInt(request.getParameter("id_feedback"));
        String contentFeedback = request.getParameter("contentFeedback");
        int numberStar = Integer.parseInt(request.getParameter("voteStar"));
        
        FeedbackUser feedbackUser = new FeedbackUser();
        feedbackUser.setContent(contentFeedback);
        feedbackUser.setVote(numberStar);
        feedbackUser.setFeedbackId(feedbackID);
        
        daoFeedback.update(feedbackUser);
        CommonUtil.dispatch(request, response, "show-feedback-controller");
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
