
package controller;

import common.CommonUtil;
import entity.Account;
import entity.Doctor;
import entity.FeedbackUser;
import entity.Patient;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAOAccount;
import model.DAODoctor;
import model.DAOFeedback;
import model.DAOPatient;


@WebServlet(name = "AddFeedbackOfPatientController", urlPatterns = {"/feedback-of-patient-controller"})
public class AddFeedbackOfPatientController extends HttpServlet {

    private final DAOPatient daoPatient;
    private final DAODoctor daoDoctor;
    private final DAOFeedback daoFeedback;

    public AddFeedbackOfPatientController() {
        daoPatient = new DAOPatient();
        daoDoctor = new DAODoctor();
        daoFeedback = new DAOFeedback();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        
        List<Patient> listpatient = daoPatient.getPatient();
        
        int idDoctor = 0;
        for(Patient patient: listpatient){
            if(patient.getAccountId() == account.getAccountId()){
                idDoctor = patient.getDoctorId();
            }
        }
        
        Doctor doctor = daoDoctor.getByID(idDoctor);
        request.getSession().setAttribute("getDoctor", doctor);
        CommonUtil.dispatch(request, response, "page-feedback-of-patient.jsp");
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = (Account) request.getSession().getAttribute("account");
        
        int idPatient = 0;
        List<Patient> listpatient = daoPatient.getPatient();
        for(Patient patient: listpatient){
            if(patient.getAccountId() == account.getAccountId()){
                idPatient = patient.getPatientId();
            }
        }
       
        String contentFeedback = request.getParameter("contentFeedback");
        int numberStar = Integer.parseInt(request.getParameter("voteStar"));
        
        FeedbackUser feedbackUser = new FeedbackUser();
        feedbackUser.setContent(contentFeedback);
        feedbackUser.setVote(numberStar);
        feedbackUser.setPatientId(idPatient);
        
        daoFeedback.create(feedbackUser);
        CommonUtil.dispatch(request, response, "show-feedback-controller");
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
