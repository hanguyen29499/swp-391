/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import common.CommonUtil;
import entity.City;
import entity.District;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAOLocation;

/**
 *
 * @author PC
 */
@WebServlet(name = "CityDeleteController", urlPatterns = {"/city-delete-controller"})
public class CityDeleteController extends HttpServlet {

    private final DAOLocation daoLocation;

    public CityDeleteController() {
        daoLocation = new DAOLocation();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        daoLocation.deleteCity(Integer.parseInt(request.getParameter("city_id")));

        
        List<City> cities = daoLocation.getCities();
        cities.sort((c1, c2) -> c1.getName().compareTo(c2.getName()));
        request.setAttribute("cities", cities);

        // Get list district
        List<District> districts = daoLocation.getDistricts();
        cities.forEach((city) -> {
            Set<District> checked = new HashSet<>();
            districts.stream().filter((district) -> (district.getCityId() == city.getId())).forEachOrdered(city.getDistricts()::add);
        });
        request.setAttribute("districts", districts);
        
        CommonUtil.dispatch(request, response, "page-location-list.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
