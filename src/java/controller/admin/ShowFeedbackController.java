/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import common.CommonUtil;
import common.Constant;
import entity.Account;
import entity.Doctor;
import entity.FeedbackUser;
import entity.Patient;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAODoctor;
import model.DAOFeedback;
import model.DAOPatient;

/**
 *
 * @author ADMINS
 */
@WebServlet(name = "ShowFeedbackController", urlPatterns = {"/show-feedback-controller"})
public class ShowFeedbackController extends HttpServlet {
    private final DAODoctor daoDoctor;
    private final DAOPatient daoPatient;
    private final DAOFeedback daoFeedback;
    
    
    
    public ShowFeedbackController(){
        daoDoctor = new DAODoctor();
        daoPatient = new DAOPatient();
        daoFeedback = new DAOFeedback();
    }
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        Account account = (Account) request.getSession().getAttribute("account");
        
        List<FeedbackUser> listFeedback = daoFeedback.getFeedback();
        List<Patient> listpatient = daoPatient.getPatient(); 
        List<Doctor> listDoctors = daoDoctor.getDoctors();
        
        if (account.getRoleId() == Constant.ROLE_PATIENT) {
            int idDoctor = 0;
            int idPatient = 0;
            for (Patient patient : listpatient) {
                if (patient.getAccountId() == account.getAccountId()) {
                    idDoctor = patient.getDoctorId();
                    idPatient = patient.getPatientId();
                }
            }
            request.getSession().setAttribute("idPatient", idPatient);

            Doctor doctor = daoDoctor.getByID(idDoctor);
            request.getSession().setAttribute("getDoctor", doctor);

            Doctor doctor1 = daoDoctor.getByID(idDoctor);
            request.getSession().setAttribute("getDoctor1", doctor1);

            
            request.getSession().setAttribute("allFeedback", listFeedback);
            

            
            CommonUtil.dispatch(request, response, "page-feedback.jsp");
        }else{
            
            request.getSession().setAttribute("allFeedback", listFeedback);
            request.getSession().setAttribute("allPatient", listpatient);
            request.getSession().setAttribute("allDoctors", listDoctors);
            CommonUtil.dispatch(request, response, "page-reviews.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
