package controller.admin;

import common.CommonUtil;
import entity.Doctor;
import entity.Patient;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAODoctor;
import model.DAOPatient;


@WebServlet(name = "ShowDoctorDetailsController", urlPatterns = {"/show-doctor-details-controller"})
public class ShowDoctorDetailsController extends HttpServlet {

    private final DAODoctor daoDoctor;
    private final DAOPatient daoPatient;
    
    public ShowDoctorDetailsController() {
        daoDoctor = new DAODoctor();
        daoPatient = new DAOPatient();
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        List<Doctor> doctors = daoDoctor.getDoctors();
        request.getSession().setAttribute("allDoctors", doctors);
        
        int doctorID = Integer.parseInt(request.getParameter("doctor_id"));
        Doctor doctor = daoDoctor.getByID(doctorID);
        request.getSession().setAttribute("doctor", doctor);
        
        List<Patient> patient = daoPatient.getPatient();
        request.setAttribute("patient", patient);
        CommonUtil.dispatch(request, response, "page-doctor-details.jsp");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}