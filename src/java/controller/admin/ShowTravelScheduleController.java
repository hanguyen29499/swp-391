/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import common.CommonUtil;
import entity.Account;
import entity.Patient;
import entity.TravelSchedule;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAOTravelSchedule;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ShowTravelScheduleController", urlPatterns = {"/show-travel-schedule-controller"})
public class ShowTravelScheduleController extends HttpServlet {

    private final DAOTravelSchedule daoTravelSchedule;

    public ShowTravelScheduleController() {
        daoTravelSchedule = new DAOTravelSchedule();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");
            if (action == null) {
                action = "listAll";
            }
            if (action.equals("listAll")) {
                try {
                    ResultSet rs = daoTravelSchedule.getListTravelSchedule();
                    List<Patient> patient = new ArrayList<>();
                    while (rs.next()) {
                        patient.add(new Patient(rs.getInt("patient_id"), rs.getInt("account_id"), rs.getInt("doctor_id"),
                                rs.getInt("nurse_id"), rs.getString("doctor_name"), rs.getString("nurse_name"),
                                new Account(rs.getInt("account_id"), rs.getString("full_name"), rs.getString("phone_number"), rs.getString("email")),
                                new TravelSchedule(rs.getInt("patient_id"), rs.getString("departure"), rs.getString("destination"), rs.getDate("time_go"))));
                    }
                    request.setAttribute("patient", patient);
                    CommonUtil.dispatch(request, response, "page-travel-schedule.jsp");
                } catch (SQLException ex) {
                    Logger.getLogger(ShowTravelScheduleController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (action.equals("filter")) {
                int valueFilter = Integer.parseInt(request.getParameter("filter"));
                if (valueFilter != 4) {
                    try {
                        ResultSet rs = daoTravelSchedule.getListTravelScheduleFilter(valueFilter);
                        List<Patient> patient = new ArrayList<>();
                        while (rs.next()) {
                            patient.add(new Patient(rs.getInt("patient_id"), rs.getInt("account_id"), rs.getInt("doctor_id"),
                                    rs.getInt("nurse_id"), rs.getString("doctor_name"), rs.getString("nurse_name"),
                                    new Account(rs.getInt("account_id"), rs.getString("full_name"), rs.getString("phone_number"), rs.getString("email")),
                                    new TravelSchedule(rs.getInt("patient_id"), rs.getString("departure"), rs.getString("destination"), rs.getDate("time_go"))));
                        }
                        request.setAttribute("patient", patient);
                        CommonUtil.dispatch(request, response, "page-travel-schedule.jsp");
                    } catch (SQLException ex) {
                        Logger.getLogger(ShowTravelScheduleController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    DAOTravelSchedule dao = new DAOTravelSchedule();
                    List<ResultSet> listRs = new ArrayList<>();
                    ResultSet rs = dao.getListF0TravelSche();
                    try {
                        while (rs.next()) {
                            listRs.add(dao.getListF1F2(rs.getString("destination"), rs.getDate("time_go")));
                        }
                        request.setAttribute("listRs", listRs);
                        CommonUtil.dispatch(request, response, "page-list-f1-f2.jsp");
                    } catch (SQLException ex) {
                        Logger.getLogger(DAOTravelSchedule.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
