/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import common.CommonUtil;
import common.Constant;
import entity.Account;
import entity.Doctor;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAOAccount;
import model.DAODoctor;

/**
 *
 * @author ADMINS
 */
@WebServlet(name = "AddDoctorController", urlPatterns = {"/add-doctor-controller"})
public class AddDoctorController extends HttpServlet {

    private final DAOAccount daoAccount;
    private final DAODoctor daoDoctor;

    public AddDoctorController() {
        daoAccount = new DAOAccount();
        daoDoctor = new DAODoctor();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Account> doctorsAccounts = daoAccount.findByRoleId(Constant.ROLE_DOCTOR);
        request.getSession().setAttribute("doctorsAccounts", doctorsAccounts);
        CommonUtil.dispatch(request, response, "page-form-add-doctor.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String branch = request.getParameter("branch");
        String role = request.getParameter("role");
        int account_id = Integer.parseInt(request.getParameter("account_id"));

        Doctor newDoctor = new Doctor();
        newDoctor.setBranch(branch);
        newDoctor.setRole(role);
        newDoctor.setAccountId(account_id);

        daoDoctor.create(newDoctor);
        response.sendRedirect("show-doctors-controller");

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
