/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import common.CommonUtil;
import entity.FeedbackUser;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAOFeedback;


@WebServlet(name = "PublishFeedbackController", urlPatterns = {"/publish-feedback-controller"})
public class PublishFeedbackController extends HttpServlet {

    private final DAOFeedback daoFeedback;
    
    public PublishFeedbackController(){
        daoFeedback = new DAOFeedback();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int feedbackID = Integer.parseInt(request.getParameter("id_feedback"));
        int publish = Integer.parseInt(request.getParameter("publish"));
        
        FeedbackUser feedbackUser = new FeedbackUser();
        feedbackUser.setPublish(publish);
        feedbackUser.setDelete(0);
        feedbackUser.setFeedbackId(feedbackID);
        daoFeedback.updateStatus(feedbackUser);
        
        List<FeedbackUser> listFeedback = daoFeedback.getFeedback();
        request.getSession().setAttribute("allFeedback", listFeedback);
        CommonUtil.dispatch(request, response, "page-reviews.jsp");
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
