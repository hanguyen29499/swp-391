/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import common.CommonUtil;
import entity.Account;
import entity.DiseaseDetail;
import entity.FeedbackUser;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAO;

/**
 *
 * @author pc
 */
@WebServlet(name = "EditDailyFormController", urlPatterns = {"/edit-daily-form-controller"})
public class EditDailyFormController extends HttpServlet {

    private final DAO dao;

    public EditDailyFormController() {
        dao = new DAO();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");
            if (action == null) {
                CommonUtil.dispatch(request, response, "/page-view-daily-form.jsp");
            }
            if (action.equals("update")) {
                String submit = request.getParameter("submit");
                if (submit == null) {   //// pre-update
                    Account account = (Account) request.getSession().getAttribute("account");
                    String diseaseDetailID = request.getParameter("diseasedetailid");
                    ResultSet rs2 = dao.getUpdateDiseaseDetail(account.getAccountId(), diseaseDetailID);
                    request.setAttribute("rs2", rs2);
                    CommonUtil.dispatch(request, response, "/page-edit-daily-form.jsp");
                } else {
                    Account account = (Account) request.getSession().getAttribute("account");
                    String declareTime = request.getParameter("declarationTime");
                    Date declarationTime = Date.valueOf(declareTime);
                    int diseasedetailid = Integer.parseInt(request.getParameter("diseasedetailid"));
                    int bodyTemperature = Integer.parseInt(request.getParameter("bodyTemperature"));
                    String headache = request.getParameter("headache");
                    String fever = request.getParameter("fever");
                    String shortness = request.getParameter("shortness");
                    String taste = request.getParameter("taste");
                    String diarrheaa = request.getParameter("diarrhea");
                    String skinRash = request.getParameter("skinRash");
                    String confusionn = request.getParameter("confusion");
                    String coughh = request.getParameter("cough");
                    String Tired = request.getParameter("tired");
                    String SoreThroat = request.getParameter("soreThroat");
                    String Vomiting = request.getParameter("vomiting");
                    String Anorexia = request.getParameter("anorexia");
                    String SeriousSituation = request.getParameter("seriousSituation");

                    int head = headache != null ? 1 : 0;
                    int fev = fever != null ? 1 : 0;
                    int shortbreath = shortness != null ? 1 : 0;
                    int tas = taste != null ? 1 : 0;
                    int diarrhea = diarrheaa != null ? 1 : 0;
                    int skinrash = skinRash != null ? 1 : 0;
                    int confusion = confusionn != null ? 1 : 0;
                    int cough = coughh != null ? 1 : 0;
                    int tired = Tired != null ? 1 : 0;
                    int sorethroat = SoreThroat != null ? 1 : 0;
                    int vomiting = Vomiting != null ? 1 : 0;
                    int anorexia = Anorexia != null ? 1 : 0;
                    int seriousSituation = SeriousSituation != null ? 1 : 0;

                    int injecttion = Integer.parseInt(request.getParameter("injection"));
                    String patientStatus = request.getParameter("patientStatus");

                    DiseaseDetail diseaseDetail = new DiseaseDetail();
                    diseaseDetail.setBodyTemperature(bodyTemperature);
                    diseaseDetail.setHeadache(head);
                    diseaseDetail.setFever(fev);
                    diseaseDetail.setShortnessBreath(shortbreath);
                    diseaseDetail.setLossOfTaste(tas);
                    diseaseDetail.setDiarrhea(diarrhea);
                    diseaseDetail.setSkinRash(skinrash);
                    diseaseDetail.setConfusion(confusion);
                    diseaseDetail.setCough(cough);
                    diseaseDetail.setTired(tired);
                    diseaseDetail.setSoreThroat(sorethroat);
                    diseaseDetail.setVomiting(vomiting);
                    diseaseDetail.setAnorexia(anorexia);
                    diseaseDetail.setSeriousSituation(seriousSituation);
                    diseaseDetail.setVaccineStatus(injecttion);
                    diseaseDetail.setPatientStatus(patientStatus);
                    diseaseDetail.setDeclarationTime(declarationTime);
                    diseaseDetail.setDiseaseDetailId(diseasedetailid);
                    dao.updateDiseaseDetail(diseaseDetail);
                    response.sendRedirect("DailyFormController");
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
