/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import common.CommonUtil;
import entity.Account;
import entity.InteractiveList;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DAO;

/**
 *
 * @author pc
 */
@WebServlet(name = "InteractiveController", urlPatterns = {"/interactive-controller"})
public class InteractiveController extends HttpServlet {

    private final DAO dao;

    public InteractiveController() {
        dao = new DAO();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            if (action == null) {
                CommonUtil.dispatch(request, response, "page-insert-interactive.jsp");
            }
            if (action.equals("listAll")) {
                String name = null;
                String filter = null;
                Account account = (Account) request.getSession().getAttribute("account");
                List<InteractiveList> listInteractive = dao.getInteractiveList(account.getAccountId());
                String filtered = request.getParameter("filtered");
                String nameSearch = request.getParameter("nameSearch");
                ResultSet rs = null;
                if (filtered == null && nameSearch == null) {
                    rs = dao.getInteractiveData(account.getAccountId());
                }
                if (filtered != null) {
                    if (filtered.equals("all")) {
                        filter = "";
                    }
                    if (filtered.equals("safe")) {
                        filter = "Safe";
                    } else if (filtered.equals("suspend")) {
                        filter = "Suspected of having covid 19 !";
                    }
                    rs = dao.filterStatus(account.getAccountId(), filter);
                } else if (nameSearch != null) {
                    for (int i = 0; i < listInteractive.size(); i++) {
                        if (listInteractive.get(i).getInteractiveName().contains(nameSearch)) {
                            name = listInteractive.get(i).getInteractiveName();
                        }
                    }
                    rs = dao.findByName(account.getAccountId(), name);
                }
                request.setAttribute("rs", rs);
                CommonUtil.dispatch(request, response, "/page-view-interactive-list.jsp");
            }
            if (action.equals("insert")) {
                String submit = request.getParameter("submit");
                if (submit == null) {
                    CommonUtil.dispatch(request, response, "page-insert-interactive.jsp");
                } else {
                    Account account = (Account) request.getSession().getAttribute("account");
                    String interactiveName = request.getParameter("interactiveName");
                    String relationship = request.getParameter("relationship");
                    int bodyTemperature = Integer.parseInt(request.getParameter("bodyTemperature"));
                    String headache = request.getParameter("headache");
                    String fever = request.getParameter("fever");
                    String shortness = request.getParameter("shortness");
                    String taste = request.getParameter("taste");
                    String diarrheaa = request.getParameter("diarrhea");
                    String skinRash = request.getParameter("skinRash");
                    String confusionn = request.getParameter("confusion");
                    String coughh = request.getParameter("cough");
                    String Tired = request.getParameter("tired");
                    String SoreThroat = request.getParameter("soreThroat");
                    String Vomiting = request.getParameter("vomiting");
                    String Anorexia = request.getParameter("anorexia");
                    String SeriousSituation = request.getParameter("seriousSituation");

                    int head = headache != null ? 1 : 0;
                    int fev = fever != null ? 1 : 0;
                    int shortbreath = shortness != null ? 1 : 0;
                    int tas = taste != null ? 1 : 0;
                    int diarrhea = diarrheaa != null ? 1 : 0;
                    int skinrash = skinRash != null ? 1 : 0;
                    int confusion = confusionn != null ? 1 : 0;
                    int cough = coughh != null ? 1 : 0;
                    int tired = Tired != null ? 1 : 0;
                    int sorethroat = SoreThroat != null ? 1 : 0;
                    int vomiting = Vomiting != null ? 1 : 0;
                    int anorexia = Anorexia != null ? 1 : 0;
                    int seriousSituation = SeriousSituation != null ? 1 : 0;
                    String interativeStatus = null;

                    if (bodyTemperature >= 38 || head == 1 || fev == 1 || shortbreath == 1 || tas == 1 || diarrhea == 1 || skinrash == 1 || confusion == 1 || cough == 1 || tired == 1 || sorethroat == 1 || vomiting == 1 || anorexia == 1 || seriousSituation == 1) {
                        interativeStatus = "Suspected of having covid 19 !";
                    } else if (head == 0 && fev == 0 && shortbreath == 0 && tas == 0 && diarrhea == 0 && skinrash == 0 && confusion == 0 && cough == 0 && tired == 0 && sorethroat == 0 && vomiting == 0 && anorexia == 0 && seriousSituation == 0) {
                        interativeStatus = "Safe";
                    }
                    InteractiveList interactive = new InteractiveList();
                    interactive.setInteractiveName(interactiveName);
                    interactive.setRelationship(relationship);
                    interactive.setInteractiveStatus(interativeStatus);
                    dao.addInterative(interactive, account.getAccountId());
                    response.sendRedirect("interactive-controller");
                }
            }
            if (action.equals("update")) {
                String submit = request.getParameter("submit");
                if (submit == null) {
                    Account account = (Account) request.getSession().getAttribute("account");
                    int interactiveId = Integer.parseInt(request.getParameter("interactiveId"));
                    ResultSet rs2 = dao.getUpdateInteractive(account.getAccountId(), interactiveId);
                    request.setAttribute("rs2", rs2);
                    CommonUtil.dispatch(request, response, "/page-edit-interactive.jsp");
                } else {
                    Account account = (Account) request.getSession().getAttribute("account");
                    int interactiveId = Integer.parseInt(request.getParameter("interactiveId"));
                    String interactiveName = request.getParameter("interactiveName");
                    String relationship = request.getParameter("relationship");
                    int bodyTemperature = Integer.parseInt(request.getParameter("bodyTemperature"));
                    String headache = request.getParameter("headache");
                    String fever = request.getParameter("fever");
                    String shortness = request.getParameter("shortness");
                    String taste = request.getParameter("taste");
                    String diarrheaa = request.getParameter("diarrhea");
                    String skinRash = request.getParameter("skinRash");
                    String confusionn = request.getParameter("confusion");
                    String coughh = request.getParameter("cough");
                    String Tired = request.getParameter("tired");
                    String SoreThroat = request.getParameter("soreThroat");
                    String Vomiting = request.getParameter("vomiting");
                    String Anorexia = request.getParameter("anorexia");
                    String SeriousSituation = request.getParameter("seriousSituation");

                    int head = headache != null ? 1 : 0;
                    int fev = fever != null ? 1 : 0;
                    int shortbreath = shortness != null ? 1 : 0;
                    int tas = taste != null ? 1 : 0;
                    int diarrhea = diarrheaa != null ? 1 : 0;
                    int skinrash = skinRash != null ? 1 : 0;
                    int confusion = confusionn != null ? 1 : 0;
                    int cough = coughh != null ? 1 : 0;
                    int tired = Tired != null ? 1 : 0;
                    int sorethroat = SoreThroat != null ? 1 : 0;
                    int vomiting = Vomiting != null ? 1 : 0;
                    int anorexia = Anorexia != null ? 1 : 0;
                    int seriousSituation = SeriousSituation != null ? 1 : 0;
                    String interativeStatus = null;

                    if (bodyTemperature >= 38 || head == 1 || fev == 1 || shortbreath == 1 || tas == 1 || diarrhea == 1 || skinrash == 1 || confusion == 1 || cough == 1 || tired == 1 || sorethroat == 1 || vomiting == 1 || anorexia == 1 || seriousSituation == 1) {
                        interativeStatus = "Suspected of having covid 19 !";
                    } else if (head == 0 && fev == 0 && shortbreath == 0 && tas == 0 && diarrhea == 0 && skinrash == 0 && confusion == 0 && cough == 0 && tired == 0 && sorethroat == 0 && vomiting == 0 && anorexia == 0 && seriousSituation == 0) {
                        interativeStatus = "Safe";
                    }
                    InteractiveList interactive = new InteractiveList();
                    interactive.setInteractiveName(interactiveName);
                    interactive.setRelationship(relationship);
                    interactive.setInteractiveStatus(interativeStatus);
                    interactive.setInteractiveId(interactiveId);
                    dao.updateInteractive(interactive);
                    response.sendRedirect("interactive-controller");
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
