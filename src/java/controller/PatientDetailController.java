/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dto.PatientDetail;
import entity.Account;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.DAODoctor;

/**
 *
 * @author Asus
 */
@WebServlet(name = "PatientDetailController", urlPatterns = {"/patient-detail"})
public class PatientDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    private void getDetailPatient(HttpServletRequest request, HttpServletResponse response, Integer patientId) throws ServletException, IOException{
        DAODoctor daoDotor = new DAODoctor();
        HttpSession ses = request.getSession();
        Account account = (Account) ses.getAttribute("account");
        if (Objects.isNull(account)) {
            request.getRequestDispatcher("login-controller").forward(request, response);
        }
        Integer doctorId = daoDotor.getDoctorIdByAccountId(account.getAccountId());
        List<PatientDetail> patientDetails = daoDotor.getPatientDetailByDoctorId(doctorId, patientId);
        request.setAttribute("patientDetails", patientDetails);
        request.getRequestDispatcher("page-patient-details.jsp").forward(request, response);
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response); 
        try {   
            Integer patientId = Integer.parseInt(request.getParameter("patientId"));
            getDetailPatient(request, response, patientId);
        } catch (ServletException | IOException e) {
            Logger.getLogger(ViewPatientsController.class.getName()).log(Level.SEVERE, null, e);
        }     
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String diseaseDetailId = request.getParameter("diseaseDetailId");
        String commentDoctor = request.getParameter("doctorComment");
        try {
            DAODoctor daoDotor = new DAODoctor();
            if(!Objects.isNull(diseaseDetailId)){
                 daoDotor.updateStatusPatient(Integer.parseInt(diseaseDetailId));
            }  
            Integer patientId = Integer.parseInt(request.getParameter("patientId"));
            if(!Objects.isNull(commentDoctor)){
                daoDotor.updateCommentForPatient(patientId, commentDoctor);
            }
            getDetailPatient(request, response, patientId);
        } catch (NumberFormatException | ServletException | IOException e) {
            Logger.getLogger(ViewPatientsController.class.getName()).log(Level.SEVERE, null, e);
        }  
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
