/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author phant
 */
public class DailyReport {
    private int dailyReportId;
    private int patientId;
    private int diseaseDetailId;

    public DailyReport() {
    }

    public DailyReport(int dailyReportId, int patientId, int diseaseDetailId) {
        this.dailyReportId = dailyReportId;
        this.patientId = patientId;
        this.diseaseDetailId = diseaseDetailId;
    }

    public int getDailyReportId() {
        return dailyReportId;
    }

    public void setDailyReportId(int dailyReportId) {
        this.dailyReportId = dailyReportId;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public int getDiseaseDetailId() {
        return diseaseDetailId;
    }

    public void setDiseaseDetailId(int diseaseDetailId) {
        this.diseaseDetailId = diseaseDetailId;
    }
    
}
