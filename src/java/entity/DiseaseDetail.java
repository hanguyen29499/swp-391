/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.sql.Date;

/**
 *
 * @author phant
 */
public class DiseaseDetail {

    private int diseaseDetailId;
    private int bodyTemperature;
    private int headache;
    private int fever;
    private int shortnessBreath;
    private int lossOfTaste;
    private int diarrhea;
    private int skinRash;
    private int confusion;
    private int cough;
    private int tired;
    private int soreThroat;
    private int vomiting;
    private int anorexia;
    private int seriousSituation;
    private int vaccineStatus;
    private String patientStatus;
    private Date declarationTime;

    public DiseaseDetail() {
    }

    public DiseaseDetail(int diseaseDetailId, int bodyTemperature, int headache, int fever, int shortnessBreath, int lossOfTaste, int diarrhea, int skinRash, int confusion, int cough, int tired, int soreThroat, int vomiting, int anorexia, int seriousSituation, int vaccineStatus, String patientStatus, Date declarationTime) {
        this.diseaseDetailId = diseaseDetailId;
        this.bodyTemperature = bodyTemperature;
        this.headache = headache;
        this.fever = fever;
        this.shortnessBreath = shortnessBreath;
        this.lossOfTaste = lossOfTaste;
        this.diarrhea = diarrhea;
        this.skinRash = skinRash;
        this.confusion = confusion;
        this.cough = cough;
        this.tired = tired;
        this.soreThroat = soreThroat;
        this.vomiting = vomiting;
        this.anorexia = anorexia;
        this.seriousSituation = seriousSituation;
        this.vaccineStatus = vaccineStatus;
        this.patientStatus = patientStatus;
        this.declarationTime = declarationTime;
    }

   

    public int getDiseaseDetailId() {
        return diseaseDetailId;
    }

    public void setDiseaseDetailId(int diseaseDetailId) {
        this.diseaseDetailId = diseaseDetailId;
    }

    public int getBodyTemperature() {
        return bodyTemperature;
    }

    public void setBodyTemperature(int bodyTemperature) {
        this.bodyTemperature = bodyTemperature;
    }

    public int getHeadache() {
        return headache;
    }

    public void setHeadache(int headache) {
        this.headache = headache;
    }

    public int getFever() {
        return fever;
    }

    public void setFever(int fever) {
        this.fever = fever;
    }

    public int getShortnessBreath() {
        return shortnessBreath;
    }

    public void setShortnessBreath(int shortnessBreath) {
        this.shortnessBreath = shortnessBreath;
    }

    public int getLossOfTaste() {
        return lossOfTaste;
    }

    public void setLossOfTaste(int lossOfTaste) {
        this.lossOfTaste = lossOfTaste;
    }

    public int getDiarrhea() {
        return diarrhea;
    }

    public void setDiarrhea(int diarrhea) {
        this.diarrhea = diarrhea;
    }

    public int getSkinRash() {
        return skinRash;
    }

    public void setSkinRash(int skinRash) {
        this.skinRash = skinRash;
    }

    public int getConfusion() {
        return confusion;
    }

    public void setConfusion(int confusion) {
        this.confusion = confusion;
    }

    public int getCough() {
        return cough;
    }

    public void setCough(int cough) {
        this.cough = cough;
    }

    public int getTired() {
        return tired;
    }

    public void setTired(int tired) {
        this.tired = tired;
    }

    public int getSoreThroat() {
        return soreThroat;
    }

    public void setSoreThroat(int soreThroat) {
        this.soreThroat = soreThroat;
    }

    public int getVomiting() {
        return vomiting;
    }

    public void setVomiting(int vomiting) {
        this.vomiting = vomiting;
    }

    public int getAnorexia() {
        return anorexia;
    }

    public void setAnorexia(int anorexia) {
        this.anorexia = anorexia;
    }

    public int getSeriousSituation() {
        return seriousSituation;
    }

    public void setSeriousSituation(int seriousSituation) {
        this.seriousSituation = seriousSituation;
    }

    public int getVaccineStatus() {
        return vaccineStatus;
    }

    public void setVaccineStatus(int vaccineStatus) {
        this.vaccineStatus = vaccineStatus;
    }

    public String getPatientStatus() {
        return patientStatus;
    }

    public void setPatientStatus(String patientStatus) {
        this.patientStatus = patientStatus;
    }

    public Date getDeclarationTime() {
        return declarationTime;
    }

    public void setDeclarationTime(Date declarationTime) {
        this.declarationTime = declarationTime;
    }

}
