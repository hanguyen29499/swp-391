/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author phant
 */
public class InteractiveList {

    private int interactiveId;
    private String interactiveName;
    private String relationship;
    private String interactiveStatus;
    private int patientId;

    public InteractiveList() {
    }

    public InteractiveList(int interactiveId, String interactiveName, String relationship, String interactiveStatus, int patientId) {
        this.interactiveId = interactiveId;
        this.interactiveName = interactiveName;
        this.relationship = relationship;
        this.interactiveStatus = interactiveStatus;
        this.patientId = patientId;
    }

    public int getInteractiveId() {
        return interactiveId;
    }

    public void setInteractiveId(int interactiveId) {
        this.interactiveId = interactiveId;
    }

    public String getInteractiveName() {
        return interactiveName;
    }

    public void setInteractiveName(String interactiveName) {
        this.interactiveName = interactiveName;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getInteractiveStatus() {
        return interactiveStatus;
    }

    public void setInteractiveStatus(String interactiveStatus) {
        this.interactiveStatus = interactiveStatus;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

}
