/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

public class Patient {

    
    private Integer patientId;
    private Integer accountId;
    private Integer doctorId;
    private Integer nurseId;
    private Integer locationId;
    private String doctorName;
    private String nurseName;
    private Account account;
    private TravelSchedule travelSchedule;

    public Patient() {
    }

    public Patient(int patientId, int accountId, int doctorId, int nurseId) {
        this.patientId = patientId;
        this.accountId = accountId;
        this.doctorId = doctorId;
        this.nurseId = nurseId;
    }
    
    public Patient(int patientId, int accountId, int doctorId, int nurseId, String doctorName, String nurseName, Account account, TravelSchedule travelSchedule) {
        this.patientId = patientId;
        this.accountId = accountId;
        this.doctorId = doctorId;
        this.nurseId = nurseId;
        this.doctorName = doctorName;
        this.nurseName = nurseName;
        this.account = account;
        this.travelSchedule = travelSchedule;
    }

    public Patient(int patientId, int accountId, int doctorId, int nurseId, String doctorName, String nurseName, Account account) {
        this.patientId = patientId;
        this.accountId = accountId;
        this.doctorId = doctorId;
        this.nurseId = nurseId;
        this.doctorName = doctorName;
        this.nurseName = nurseName;
        this.account = account;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public int getNurseId() {
        return nurseId;
    }

    public void setNurseId(int nurseId) {
        this.nurseId = nurseId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getNurseName() {
        return nurseName;
    }

    public void setNurseName(String nurseName) {
        this.nurseName = nurseName;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public TravelSchedule getTravelSchedule() {
        return travelSchedule;
    }

    public void setTravelSchedule(TravelSchedule travelSchedule) {
        this.travelSchedule = travelSchedule;
    }

    
}
