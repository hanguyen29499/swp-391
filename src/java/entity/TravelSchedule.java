/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Date;

/**
 *
 * @author phant
 */
public class TravelSchedule {

    private int travelId;
    private int patientId;
    private String departure;
    private String destination;
    private Date time;

    public TravelSchedule() {
    }

    public TravelSchedule(int travelId, int patientId, String departure, String destination, Date time) {
        this.travelId = travelId;
        this.patientId = patientId;
        this.departure = departure;
        this.destination = destination;
        this.time = time;
    }

    public TravelSchedule(int patientId, String departure, String destination, Date time) {
        this.patientId = patientId;
        this.departure = departure;
        this.destination = destination;
        this.time = time;
    }

    public int getTravelId() {
        return travelId;
    }

    public int getPatientId() {
        return patientId;
    }

    public String getDeparture() {
        return departure;
    }

    public String getDestination() {
        return destination;
    }

    public Date getTime() {
        return time;
    }

    public void setTravelId(int travelId) {
        this.travelId = travelId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setTime(Date time) {
        this.time = time;
    }

}
