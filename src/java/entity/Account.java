/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import common.Constant;

/**
 *
 * @author NguyenCT
 */
public class Account {

    private int accountId;
    private String userName;
    private String password;
    private String fullname;
    private String phonenumber;
    private int gender;
    private String dob;
    private int age;
    private int status;
    private int roleId;
    private String email;
    private String address;

    public Account() {
    }

    public Account(int accountId, String userName, String password, String fullname, String phonenumber, int gender, String dob, int age, int status, int roleId, String email, String address) {
        this.accountId = accountId;
        this.userName = userName;
        this.password = password;
        this.fullname = fullname;
        this.phonenumber = phonenumber;
        this.gender = gender;
        this.dob = dob;
        this.age = age;
        this.status = status;
        this.roleId = roleId;
        this.email = email;
        this.address = address;
    }

    public Account(String userName, String password, String fullname, String phonenumber, int gender, String dob, int age, int roleId, String email) {
        this.userName = userName;
        this.password = password;
        this.fullname = fullname;
        this.phonenumber = phonenumber;
        this.gender = gender;
        this.dob = dob;
        this.age = age;
        this.status = 1;
        this.roleId = roleId;
        this.email = email;
    }

    public Account(String userName, String password, String fullname, String phonenumber, int gender, String dob, int age, int roleId, String email, String address) {
        this.userName = userName;
        this.password = password;
        this.fullname = fullname;
        this.phonenumber = phonenumber;
        this.gender = gender;
        this.dob = dob;
        this.age = age;
        this.status = 1;
        this.roleId = roleId;
        this.email = email;
        this.address = address;
    }

    public Account(int accountId, String fullname, String phonenumber, int gender, String dob, int age, String email) {
        this.accountId = accountId;
        this.fullname = fullname;
        this.phonenumber = phonenumber;
        this.gender = gender;
        this.dob = dob;
        this.age = age;
        this.email = email;
    }

    public Account(int accountId, String fullname, String phonenumber, String email) {
        this.accountId = accountId;
        this.fullname = fullname;
        this.phonenumber = phonenumber;
        this.email = email;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRoleName() {
        switch (roleId) {
            case Constant.ROLE_ADMIN:
                return "Admin";
            case Constant.ROLE_DOCTOR:
                return "Doctor";
            case Constant.ROLE_NURSE:
                return "Nurse";
            case Constant.ROLE_PATIENT:
                return "Patient";
            default:
                return "";
        }
    }

}
