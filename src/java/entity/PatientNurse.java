/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author phant
 */
public class PatientNurse {

    private int id;
    private int patientId;
    private int nurseId;

    public PatientNurse() {
    }

    public PatientNurse(int id, int patientId, int nurseId) {
        this.id = id;
        this.patientId = patientId;
        this.nurseId = nurseId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public int getNurseId() {
        return nurseId;
    }

    public void setNurseId(int nurseId) {
        this.nurseId = nurseId;
    }

}
