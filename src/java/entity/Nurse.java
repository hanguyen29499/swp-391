/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author phant
 */
public class Nurse {
    
    private int nurseId;
    private String branch;
    private int accountId;

    public Nurse() {
    }

    public Nurse(int nurseId, String branch, int accountId) {
        this.nurseId = nurseId;
        this.branch = branch;
        this.accountId = accountId;
    }

    public int getNurseId() {
        return nurseId;
    }

    public void setNurseId(int nurseId) {
        this.nurseId = nurseId;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }
}
