/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.sql.Date;

/**
 *
 * @author Asus
 */
public class PatientDetail {
    
    private Integer patientId;
    private String fullname;
    private String email;
    private String phonenumber;
    private Integer age;
    private Integer gender;
    private String address;
    private Integer bodyTemperature;
    private Integer headache;
    private Integer fever;
    private Integer shortnessBreath;
    private Integer lossOfTaste;
    private Integer diarrhea;
    private Integer skinRash;
    private Integer confusion;
    private Integer cough;
    private Integer tired;
    private Integer soreThroat;
    private Integer vomiting;
    private Integer anorexia;
    private Integer seriousSituation;
    private Integer vaccineStatus;
    private String patientStatus;
    private Integer diseaseDetailId;
    private Date declarationTime;
    private String doctorComment;

    public PatientDetail() {
    }

    public Integer getDiseaseDetailId() {
        return diseaseDetailId;
    }

    public void setDiseaseDetailId(Integer diseaseDetailId) {
        this.diseaseDetailId = diseaseDetailId;
    }

   

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getBodyTemperature() {
        return bodyTemperature;
    }

    public void setBodyTemperature(Integer bodyTemperature) {
        this.bodyTemperature = bodyTemperature;
    }

    public Integer getHeadache() {
        return headache;
    }

    public void setHeadache(Integer headache) {
        this.headache = headache;
    }

    public Integer getFever() {
        return fever;
    }

    public void setFever(Integer fever) {
        this.fever = fever;
    }

    public Integer getShortnessBreath() {
        return shortnessBreath;
    }

    public void setShortnessBreath(Integer shortnessBreath) {
        this.shortnessBreath = shortnessBreath;
    }

    public Integer getLossOfTaste() {
        return lossOfTaste;
    }

    public void setLossOfTaste(Integer lossOfTaste) {
        this.lossOfTaste = lossOfTaste;
    }

    public Integer getDiarrhea() {
        return diarrhea;
    }

    public void setDiarrhea(Integer diarrhea) {
        this.diarrhea = diarrhea;
    }

    public Integer getSkinRash() {
        return skinRash;
    }

    public void setSkinRash(Integer skinRash) {
        this.skinRash = skinRash;
    }

    public Integer getConfusion() {
        return confusion;
    }

    public void setConfusion(Integer confusion) {
        this.confusion = confusion;
    }

    public Integer getCough() {
        return cough;
    }

    public void setCough(Integer cough) {
        this.cough = cough;
    }

    public Integer getTired() {
        return tired;
    }

    public void setTired(Integer tired) {
        this.tired = tired;
    }

    public Integer getSoreThroat() {
        return soreThroat;
    }

    public void setSoreThroat(Integer soreThroat) {
        this.soreThroat = soreThroat;
    }

    public Integer getVomiting() {
        return vomiting;
    }

    public void setVomiting(Integer vomiting) {
        this.vomiting = vomiting;
    }

    public Integer getAnorexia() {
        return anorexia;
    }

    public void setAnorexia(Integer anorexia) {
        this.anorexia = anorexia;
    }

    public Integer getSeriousSituation() {
        return seriousSituation;
    }

    public void setSeriousSituation(Integer seriousSituation) {
        this.seriousSituation = seriousSituation;
    }

    public Integer getVaccineStatus() {
        return vaccineStatus;
    }

    public void setVaccineStatus(Integer vaccineStatus) {
        this.vaccineStatus = vaccineStatus;
    }

    public String getPatientStatus() {
        return patientStatus;
    }

    public void setPatientStatus(String patientStatus) {
        this.patientStatus = patientStatus;
    }

    public Date getDeclarationTime() {
        return declarationTime;
    }

    public void setDeclarationTime(Date declarationTime) {
        this.declarationTime = declarationTime;
    }

    public String getDoctorComment() {
        return doctorComment;
    }

    public void setDoctorComment(String doctorComment) {
        this.doctorComment = doctorComment;
    }

    @Override
    public String toString() {
        return "PatientDetail{" + "patientId=" + patientId + ", fullname=" + fullname + ", email=" + email + ", phonenumber=" + phonenumber + ", age=" + age + ", gender=" + gender + ", address=" + address + ", bodyTemperature=" + bodyTemperature + ", headache=" + headache + ", fever=" + fever + ", shortnessBreath=" + shortnessBreath + ", lossOfTaste=" + lossOfTaste + ", diarrhea=" + diarrhea + ", skinRash=" + skinRash + ", confusion=" + confusion + ", cough=" + cough + ", tired=" + tired + ", soreThroat=" + soreThroat + ", vomiting=" + vomiting + ", anorexia=" + anorexia + ", seriousSituation=" + seriousSituation + ", vaccineStatus=" + vaccineStatus + ", patientStatus=" + patientStatus + ", diseaseDetailId=" + diseaseDetailId + ", declarationTime=" + declarationTime + ", doctorComment=" + doctorComment + '}';
    }

    
   

    
}
