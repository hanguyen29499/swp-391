/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.sql.Date;

/**
 *
 * @author Asus
 */
public class ViewPatient {

    private Integer patientId;
    private String fullname;
    private String email;
    private String phonenumber;
    private Integer bodyTemperature;
    private Integer headache;
    private Integer fever;
    private Integer shortnessBreath;
    private Integer seriousSituation;
    private Integer vaccineStatus;
    private String patientStatus;
    private Date declarationTime;
    

    public ViewPatient() {
    }

    public ViewPatient(Integer patientId, String fullname, String email, String phonenumber, Integer bodyTemperature, Integer headache, Integer fever, Integer shortnessBreath, Integer seriousSituation, Integer vaccineStatus, String patientStatus, Date declarationTime) {
        this.patientId = patientId;
        this.fullname = fullname;
        this.email = email;
        this.phonenumber = phonenumber;
        this.bodyTemperature = bodyTemperature;
        this.headache = headache;
        this.fever = fever;
        this.shortnessBreath = shortnessBreath;
        this.seriousSituation = seriousSituation;
        this.vaccineStatus = vaccineStatus;
        this.patientStatus = patientStatus;
        this.declarationTime = declarationTime;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Integer getBodyTemperature() {
        return bodyTemperature;
    }

    public void setBodyTemperature(Integer bodyTemperature) {
        this.bodyTemperature = bodyTemperature;
    }

    public Integer getHeadache() {
        return headache;
    }

    public void setHeadache(Integer headache) {
        this.headache = headache;
    }

    public Integer getFever() {
        return fever;
    }

    public void setFever(Integer fever) {
        this.fever = fever;
    }

    public Integer getShortnessBreath() {
        return shortnessBreath;
    }

    public void setShortnessBreath(Integer shortnessBreath) {
        this.shortnessBreath = shortnessBreath;
    }

    public Integer getSeriousSituation() {
        return seriousSituation;
    }

    public void setSeriousSituation(Integer seriousSituation) {
        this.seriousSituation = seriousSituation;
    }

    public Integer getVaccineStatus() {
        return vaccineStatus;
    }

    public void setVaccineStatus(Integer vaccineStatus) {
        this.vaccineStatus = vaccineStatus;
    }

    public String getPatientStatus() {
        return patientStatus;
    }

    public void setPatientStatus(String patientStatus) {
        this.patientStatus = patientStatus;
    }

    public Date getDeclarationTime() {
        return declarationTime;
    }

    public void setDeclarationTime(Date declarationTime) {
        this.declarationTime = declarationTime;
    }

    @Override
    public String toString() {
        return "ViewPatient{" + "patientId=" + patientId + ", fullname=" + fullname + ", email=" + email + ", phonenumber=" + phonenumber + ", bodyTemperature=" + bodyTemperature + ", headache=" + headache + ", fever=" + fever + ", shortnessBreath=" + shortnessBreath + ", seriousSituation=" + seriousSituation + ", vaccineStatus=" + vaccineStatus + ", patientStatus=" + patientStatus + ", declarationTime=" + declarationTime + '}';
    }

    
    
}
