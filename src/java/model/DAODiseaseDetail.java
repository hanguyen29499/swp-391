/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.DiseaseDetail;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DAO;

/**
 *
 * @author pc
 */
public class DAODiseaseDetail extends DAO {

    public DAODiseaseDetail() {
    }

    public int addDiseaseDetail(DiseaseDetail diseaseDetail) {
        int n = 0;
        String sql = "INSERT INTO disease_detail ( body_temperature, headache, fever, shortness_breath,loss_of_taste) VALUES (?,?,?,?,?)";
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setInt(1, diseaseDetail.getBodyTemperature());
            pre.setInt(2, diseaseDetail.getHeadache());
            pre.setInt(3, diseaseDetail.getFever());
            pre.setInt(4, diseaseDetail.getShortnessBreath());
            pre.setInt(5, diseaseDetail.getLossOfTaste());
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAODiseaseDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

}
