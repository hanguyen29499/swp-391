/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Account;
import entity.Doctor;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PC
 */
public class DAONurse extends DAO {
    
        public int selectNurseId(int account_id){
            int n=0;
            String sql="select nurse_id from nurse join user_account on nurse.account_id=user_account.account_id where nurse.account_id=?";
            try {
                PreparedStatement ps= getConnection().prepareStatement(sql);
                ps.setInt(1, account_id);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {                    
                    return rs.getInt(1);
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAONurse.class.getName()).log(Level.SEVERE, null, ex);
            }
            return n;
        }
    
        public void changeNurse(int patient_id, int nurse_id , String nurse_name) {
        String sql = "update patient set nurse_id=?,nurse_name=? where patient_id=?";
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setInt(1, nurse_id);
            ps.setString(2, nurse_name);
            ps.setInt(3, patient_id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAONurse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
