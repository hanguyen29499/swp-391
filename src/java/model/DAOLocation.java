/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.City;
import entity.District;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOLocation extends DAO {

    public List<City> getCities() {
        List<City> cities = new ArrayList<>();
        try {
            String sql = "SELECT * FROM city";
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                City city = new City();
                city.setId(rs.getInt(1));
                city.setName(rs.getString(2));

                cities.add(city);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAODoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cities;
    }

    public City findCityById(int cityId) {
        List<City> cities = new ArrayList<>();
        try {
            String sql = "SELECT * FROM city WHERE city_id = ?";
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setInt(1, cityId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                City city = new City();
                city.setId(rs.getInt(1));
                city.setName(rs.getString(2));

                cities.add(city);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAODoctor.class.getName()).log(Level.SEVERE, null, ex);
        }

        return !cities.isEmpty() ? cities.iterator().next() : null;
    }

    public List<City> findCityByName(String cityName) {
        List<City> cities = new ArrayList<>();
        try {
            String sql = "SELECT * FROM city WHERE city_name LIKE ?";
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setString(1, "%" + cityName + "%");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                City city = new City();
                city.setId(rs.getInt(1));
                city.setName(rs.getString(2));

                cities.add(city);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAODoctor.class.getName()).log(Level.SEVERE, null, ex);
        }

        return cities;
    }

    public List<District> getDistricts() {
        List<District> districts = new ArrayList<>();
        try {
            String sql = "SELECT * FROM district INNER JOIN city "
                    + "ON city.city_id = district.city_id";
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                District district = new District();
                district.setDistrictId(rs.getInt("district_id"));
                district.setDistrictName(rs.getString("district_name"));
                district.setCityId(rs.getInt("city_id"));
                district.setCityName(rs.getString("city_name"));

                districts.add(district);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAODoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return districts;
    }

    public List<District> findDistrictsByCityId(int cityId) {

        List<District> districts = new ArrayList<>();
        try {
            String sql = "SELECT * FROM district INNER JOIN city "
                    + "ON city.city_id = district.city_id "
                    + "WHERE city.city_id = ?";
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setInt(1, cityId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                District district = new District();
                district.setDistrictId(rs.getInt("district_id"));
                district.setDistrictName(rs.getString("district_name"));
                district.setCityId(rs.getInt("city_id"));
                district.setCityName(rs.getString("city_name"));

                districts.add(district);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAODoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return districts;
    }

    public void deleteCity(int cityId) {
        String sqlDeleteDistrict = "DELETE FROM district\n"
                + "WHERE city_id = " + cityId + "";
        try {
            getStatement().execute(sqlDeleteDistrict);
        } catch (SQLException ex) {
            Logger.getLogger(DAOAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }

        String sqlDeleteCity = "DELETE FROM city\n"
                + "WHERE city_id = " + cityId + "";
        try {
            getStatement().execute(sqlDeleteCity);
        } catch (SQLException ex) {
            Logger.getLogger(DAOAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addCity(String name) {
        String sqlAddCity = "INSERT INTO city(city_name) "
                + "VALUES (?);";

        try {
            PreparedStatement ps = getConnection().prepareStatement(sqlAddCity);
            ps.setString(1, name);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addDistrict(int cityId, String name) {
        String sqlAddCity = "INSERT INTO district(district_name, city_id) "
                + "VALUES (?, ?);";

        try {
            PreparedStatement ps = getConnection().prepareStatement(sqlAddCity);
            ps.setString(1, name);
            ps.setInt(2, cityId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteDistrict(int districtId) {
        String sqlDeleteDistrict = "DELETE FROM district\n"
                + "WHERE district_id = " + districtId + "";

        try {
            getStatement().execute(sqlDeleteDistrict);
        } catch (SQLException ex) {
            Logger.getLogger(DAOAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
