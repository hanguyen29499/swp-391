/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.FeedbackUser;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DAO;

/**
 *
 * @author pc
 */
public class DAOFeedback extends DAO {

    public DAOFeedback() {

    }

    public int addFeedback(FeedbackUser feedback) {
        int n = 0;
        String sql = "INSERT INTO dbo.feedback_user ( [content], patient_id )\n"
                + "          VALUES (?,?)";
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            String s="select top 1 patient.patient_id\n"
                    + "from patient\n"
                    + "ORDER BY patient.patient_id DESC";
            PreparedStatement pstm = getConnection().prepareStatement(s);
            ResultSet rs = pstm.executeQuery();
            pre.setString(1, feedback.getContent());
            while (rs.next()) {
                pre.setInt(2, rs.getInt(1));
            }
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOFeedback.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public void create(FeedbackUser feedbackUser){
        try {
            String sql = "insert into feedback_user(content, vote, patient_id)\n"
                    + "values(?,?,?)";
            PreparedStatement stm = getConnection().prepareStatement(sql);
            stm.setString(1, feedbackUser.getContent());
            stm.setInt(2, feedbackUser.getVote());
            stm.setInt(3, feedbackUser.getPatientId());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOFeedback.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void update(FeedbackUser feedbackUser){
        try {
            String sql = "update feedback_user set "
                    + "content = '"+feedbackUser.getContent()+"', "
                    + "vote = "+feedbackUser.getVote()+"\n"
                    + "where feedback_id = "+feedbackUser.getFeedbackId()+"";
            getStatement().execute(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAOFeedback.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<FeedbackUser> getFeedback(){
        List<FeedbackUser> list = new ArrayList<>();
        try {
            String sql = "select *  from feedback_user";
            PreparedStatement stm = getConnection().prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                FeedbackUser feedbackUser = new FeedbackUser();
                feedbackUser.setFeedbackId(rs.getInt("feedback_id"));
                feedbackUser.setContent(rs.getString("content"));
                feedbackUser.setVote(rs.getInt("vote"));
                feedbackUser.setPatientId(rs.getInt("patient_id"));
                feedbackUser.setPublish(rs.getInt("published"));
                feedbackUser.setDelete(rs.getInt("removed"));
                list.add(feedbackUser);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOFeedback.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public FeedbackUser getByID(int feedbackID){
        try {
            String sql =  "select *  from feedback_user \n"
                    + "where feedback_id = ?";
            PreparedStatement stm = getConnection().prepareStatement(sql);
            stm.setInt(1, feedbackID);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                FeedbackUser feedbackUser = new FeedbackUser();
                feedbackUser.setFeedbackId(rs.getInt("feedback_id"));
                feedbackUser.setContent(rs.getString("content"));
                feedbackUser.setVote(rs.getInt("vote"));
                feedbackUser.setPatientId(rs.getInt("patient_id"));
                return feedbackUser;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOFeedback.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void removeById(int feedbackID){
        try {
            String sql = "delete  from feedback_user \n"
                    + "where feedback_id = ?";
            PreparedStatement stm = getConnection().prepareStatement(sql);
            stm.setInt(1, feedbackID);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOFeedback.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void updateStatus(FeedbackUser feedbackUser){
        
        String sql = "update feedback_user set published = " + feedbackUser.getPublish() + ", "
                    + "removed = " + feedbackUser.getDelete() + "\n"
                    + "where feedback_id = " + feedbackUser.getFeedbackId() + "";
        try {
            getStatement().execute(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAOFeedback.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
