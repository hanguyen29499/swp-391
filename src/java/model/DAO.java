/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import common.Constant;
import dto.PatientDetail;
import entity.Account;
import entity.DiseaseDetail;
import entity.InteractiveList;
import entity.TravelSchedule;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author NguyenCT
 */
public class DAO extends DBConnect {

    public boolean login(String username, String password) {
        try {
            boolean isValid = false;
            String sql = "select * from user_account where username=? and password=?";
            PreparedStatement ps = getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, username);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                isValid = true;
            }
            return isValid;
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public String encrypt(String strToEncrypt) throws Exception {
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        byte[] key = Constant.KEY.getBytes(StandardCharsets.UTF_8);
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16);
        SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));
    }

    public Account checkUserExits(String username, Integer roleId) {
        try {
            String query = "SELECT * FROM user_account "
                    + "WHERE username = ? "
                    + "AND role_id = ? ";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setString(1, username);
            ps.setInt(2, roleId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Account userAccount = new Account();
                userAccount.setAccountId(rs.getInt("account_id"));
                userAccount.setUserName(rs.getString("username"));
                userAccount.setFullname(rs.getString("full_name"));
                return userAccount;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int addInformationForm(Account acc) {
        int n = 0;
        String query = "INSERT INTO user_account(username, [password], full_name, phone_number, gender, birth_day, age, role_id, email) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
        try {
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setString(1, acc.getUserName());
            ps.setString(2, acc.getPassword());
            ps.setString(3, acc.getFullname());
            ps.setString(4, acc.getPhonenumber());
            ps.setInt(5, acc.getGender());
            ps.setString(6, acc.getDob());
            ps.setInt(7, acc.getAge());
            ps.setInt(8, acc.getRoleId());
            ps.setString(9, acc.getEmail());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public void updateDiseaseDetail(DiseaseDetail diseaseDetail) {
        String sql = "UPDATE disease_detail SET body_temperature = ? , headache = ? , fever = ? , shortness_breath = ? , loss_of_taste = ? , diarrhea = ? , skin_rash = ?, confusion=?, cough=?, tired=?, sore_throat = ? , vomiting = ? , anorexia = ? , serious_situation = ? , vaccine_status=? , patient_status = ? , declaration_time = ?\n"
                + "WHERE disease_detail.disease_detail_id = ?;";
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setInt(1, diseaseDetail.getBodyTemperature());
            pre.setInt(2, diseaseDetail.getHeadache());
            pre.setInt(3, diseaseDetail.getFever());
            pre.setInt(4, diseaseDetail.getShortnessBreath());
            pre.setInt(5, diseaseDetail.getLossOfTaste());
            pre.setInt(6, diseaseDetail.getDiarrhea());
            pre.setInt(7, diseaseDetail.getSkinRash());
            pre.setInt(8, diseaseDetail.getConfusion());
            pre.setInt(9, diseaseDetail.getCough());
            pre.setInt(10, diseaseDetail.getTired());
            pre.setInt(11, diseaseDetail.getSoreThroat());
            pre.setInt(12, diseaseDetail.getVomiting());
            pre.setInt(13, diseaseDetail.getAnorexia());
            pre.setInt(14, diseaseDetail.getSeriousSituation());
            pre.setInt(15, diseaseDetail.getVaccineStatus());
            pre.setString(16, diseaseDetail.getPatientStatus());
            pre.setDate(17, diseaseDetail.getDeclarationTime());
            pre.setInt(18, diseaseDetail.getDiseaseDetailId());
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAODiseaseDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int addDiseaseDetail(DiseaseDetail diseaseDetail) {
        int n = 0;
        String sql = "INSERT INTO disease_detail (body_temperature, headache,fever,shortness_breath,loss_of_taste,diarrhea, skin_rash,confusion,cough, tired,sore_throat,vomiting,anorexia,serious_situation,vaccine_status,patient_status,declaration_time)\n"
                + "   VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setInt(1, diseaseDetail.getBodyTemperature());
            pre.setInt(2, diseaseDetail.getHeadache());
            pre.setInt(3, diseaseDetail.getFever());
            pre.setInt(4, diseaseDetail.getShortnessBreath());
            pre.setInt(5, diseaseDetail.getLossOfTaste());
            pre.setInt(6, diseaseDetail.getDiarrhea());
            pre.setInt(7, diseaseDetail.getSkinRash());
            pre.setInt(8, diseaseDetail.getConfusion());
            pre.setInt(9, diseaseDetail.getCough());
            pre.setInt(10, diseaseDetail.getTired());
            pre.setInt(11, diseaseDetail.getSoreThroat());
            pre.setInt(12, diseaseDetail.getVomiting());
            pre.setInt(13, diseaseDetail.getAnorexia());
            pre.setInt(14, diseaseDetail.getSeriousSituation());
            pre.setInt(15, diseaseDetail.getVaccineStatus());
            pre.setString(16, diseaseDetail.getPatientStatus());
            pre.setDate(17, diseaseDetail.getDeclarationTime());
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAODiseaseDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

//    public int updateFeedback(String content, int feedbackid) {
//        int n = 0;
//        String sql = "UPDATE feedback_user SET content = ?\n"
//                + "     WHERE feedback_id =?;\n"
//                + "\n"
//                + "";
//        try {
//            PreparedStatement pre = getConnection().prepareStatement(sql);
//            pre.setString(1, content);
//            pre.setInt(2, feedbackid);
//            n = pre.executeUpdate();
//        } catch (SQLException ex) {
//            Logger.getLogger(DAOFeedback.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return n;
//    }
    public int checkPatientIdExist(int accountid) {
        int n = 0;
        String sqlCheck = "select account_id \n"
                + "from patient\n"
                + "where account_id = ?;";
        try {
            PreparedStatement pstm = getConnection().prepareStatement(sqlCheck);
            pstm.setInt(1, accountid);
            ResultSet rs1 = pstm.executeQuery();
            if (rs1.next()) {
                if (accountid == rs1.getInt(1)) {
                    n = 1;
                } else {
                    n = 0;
                }
            }
            pstm.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public Integer totalPatient() {
        try {
            String query = "SELECT COUNT(patient_id) AS total_patient "
                    + "FROM [swp_covid_manager].[dbo].[patient]";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total_patient");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Integer totalDoctor() {
        try {
            String query = "SELECT COUNT(doctor_id) AS total_doctor "
                    + "FROM [swp_covid_manager].[dbo].[doctor]";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total_doctor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Integer totalNurse() {
        try {
            String query = "SELECT COUNT(nurse_id) AS total_nurse "
                    + "FROM [swp_covid_manager].[dbo].[nurse]";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total_nurse");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Integer totalF0() {
        try {
            String query = "SELECT COUNT(p.patient_id) AS total_f0 "
                    + "FROM patient p "
                    + "JOIN daily_report dr "
                    + "ON p.patient_id = dr.patient_id "
                    + "JOIN disease_detail dd "
                    + "ON dd.disease_detail_id = dr.disease_detail_id "
                    + "JOIN user_account ua "
                    + "ON ua.account_id = p.account_id "
                    + "JOIN "
                    + "(SELECT dl.patient_id,MAX(d.declaration_time) AS max_date "
                    + "FROM disease_detail d join "
                    + "daily_report dl ON dl.disease_detail_id = d.disease_detail_id "
                    + "GROUP BY dl.patient_id)a \n"
                    + "ON a.patient_id = p.patient_id AND a.max_date = dd.declaration_time "
                    + "AND dd.patient_status = 'F0' ";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total_f0");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Integer totalPatientsRecovered() {
        try {
            String query = "SELECT COUNT(p.patient_id) AS total_patients_recovered "
                    + "FROM patient p "
                    + "JOIN daily_report dr "
                    + "ON p.patient_id = dr.patient_id "
                    + "JOIN disease_detail dd "
                    + "ON dd.disease_detail_id = dr.disease_detail_id "
                    + "JOIN user_account ua "
                    + "ON ua.account_id = p.account_id "
                    + "JOIN "
                    + "(SELECT dl.patient_id,MAX(d.declaration_time) AS max_date "
                    + "FROM disease_detail d join "
                    + "daily_report dl ON dl.disease_detail_id = d.disease_detail_id "
                    + "GROUP BY dl.patient_id)a \n"
                    + "ON a.patient_id = p.patient_id AND a.max_date = dd.declaration_time "
                    + "AND dd.patient_status = 'OK' ";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total_patients_recovered");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public PatientDetail getPatientDetailByAccountId(Integer accountId) {
        try {
            String query = "SELECT p.patient_id, ua.full_name, \n"
                    + "ua.email,ua.phone_number, ua.age, ua.gender, ua.[address], dd.[disease_detail_id]\n"
                    + ",[body_temperature]\n"
                    + " ,[headache]\n"
                    + ",[fever]\n"
                    + ",[shortness_breath]\n"
                    + ",[loss_of_taste]\n"
                    + ",[diarrhea]\n"
                    + ",[skin_rash]\n"
                    + ",[confusion]\n"
                    + ",[cough]\n"
                    + ",[tired]\n"
                    + ",[sore_throat]\n"
                    + ",[vomiting]\n"
                    + ",[anorexia]\n"
                    + ",[serious_situation]\n"
                    + ",[vaccine_status]\n"
                    + ",[patient_status]\n"
                    + ",p.doctor_comment "
                    + ", max_date \n"
                    + "FROM patient p \n"
                    + "JOIN daily_report dr \n"
                    + "ON p.patient_id = dr.patient_id \n"
                    + "JOIN disease_detail dd \n"
                    + "ON dd.disease_detail_id = dr.disease_detail_id \n"
                    + "JOIN user_account ua \n"
                    + "ON ua.account_id = p.account_id \n"
                    + "JOIN \n"
                    + "(SELECT dl.patient_id,MAX(d.declaration_time) AS max_date \n"
                    + "FROM disease_detail d join \n"
                    + "daily_report dl ON dl.disease_detail_id = d.disease_detail_id \n"
                    + "GROUP BY dl.patient_id)a \n"
                    + "ON a.patient_id = p.patient_id AND a.max_date = dd.declaration_time \n"
                    + "AND ua.account_id = ?";

            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                PatientDetail patient = new PatientDetail();
                patient.setPatientId(rs.getInt("patient_id"));
                patient.setFullname(rs.getString("full_name"));
                patient.setPhonenumber(rs.getString("phone_number"));
                patient.setEmail(rs.getString("email"));
                patient.setAge(rs.getInt("age"));
                patient.setGender(rs.getInt("gender"));
                patient.setAddress(rs.getString("address"));
                patient.setBodyTemperature(rs.getInt("body_temperature"));
                patient.setFever(rs.getInt("fever"));
                patient.setHeadache(rs.getInt("headache"));
                patient.setShortnessBreath(rs.getInt("shortness_breath"));
                patient.setLossOfTaste(rs.getInt("loss_of_taste"));
                patient.setDiarrhea(rs.getInt("diarrhea"));
                patient.setSkinRash(rs.getInt("skin_rash"));
                patient.setConfusion(rs.getInt("confusion"));
                patient.setCough(rs.getInt("cough"));
                patient.setTired(rs.getInt("tired"));
                patient.setSoreThroat(rs.getInt("sore_throat"));
                patient.setVomiting(rs.getInt("vomiting"));
                patient.setAnorexia(rs.getInt("anorexia"));
                patient.setSeriousSituation(rs.getInt("serious_situation"));
                patient.setVaccineStatus(rs.getInt("vaccine_status"));
                patient.setPatientStatus(rs.getString("patient_status"));
                patient.setDeclarationTime(rs.getDate("max_date"));
                patient.setDoctorComment(rs.getString("doctor_comment"));
                patient.setDiseaseDetailId(rs.getInt("disease_detail_id"));
                return patient;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void main(String[] args) {
        DAO d = new DAO();
        System.out.println(d.getPatientDetailByAccountId(3).getDoctorComment());
    }

    public int addDailyFormExistAccount(int accountid) {
        int n = 0;
        try {
            String sql = "INSERT INTO dbo.daily_report (patient_id, disease_detail_id)\n"
                    + "        VALUES(?,?)";
            PreparedStatement pre = getConnection().prepareStatement(sql);
            /// Get Value of Disease Detail               
            String DiseaseDetailID = "select top 1 disease_detail.disease_detail_id\n"
                    + "from dbo.disease_detail\n"
                    + "ORDER BY disease_detail.disease_detail_id DESC";
            PreparedStatement psm = getConnection().prepareStatement(DiseaseDetailID);
            ResultSet diseaseDetailID = psm.executeQuery();
            if (checkPatientIdExist(accountid) == 1) {
                /// Get Value  patient ID when you already have accountID in system
                String getPatientID = "select patient.patient_id\n"
                        + "from user_account INNER JOIN patient ON user_account.account_id = patient.account_id\n"
                        + "WHERE user_account.account_id = ?";
                PreparedStatement pstm = getConnection().prepareStatement(getPatientID);
                pstm.setInt(1, accountid);
                ResultSet patientID = pstm.executeQuery();
                while (patientID.next() && diseaseDetailID.next()) {
                    pre.setInt(1, patientID.getInt(1));
                    pre.setInt(2, diseaseDetailID.getInt(1));
                }
                n = pre.executeUpdate();
            }
            if (checkPatientIdExist(accountid) == 0) {
                String getNewPatientID = "INSERT INTO patient (account_id)\n"
                        + "      VALUES(?);";
                PreparedStatement psmm = getConnection().prepareStatement(getNewPatientID);
                psmm.setInt(1, accountid);
                psmm.executeUpdate();
                //// get value of patient ID after have already insert
                String selectPatient = "select patient.patient_id\n"
                        + "from user_account INNER JOIN patient ON user_account.account_id = patient.account_id\n"
                        + "WHERE user_account.account_id = ?";
                PreparedStatement pree = getConnection().prepareStatement(selectPatient);
                pree.setInt(1, accountid);
                ResultSet patientSelect = pree.executeQuery();
                while (patientSelect.next() && diseaseDetailID.next()) {
                    pre.setInt(1, patientSelect.getInt(1));
                    pre.setInt(2, diseaseDetailID.getInt(1));
                }
                n = pre.executeUpdate();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAODailyReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public ResultSet getUserDiseaseDetail(int accountid) {
        String sql = "select declaration_time , disease_detail.disease_detail_id , body_temperature , headache , fever , shortness_breath , loss_of_taste , diarrhea, skin_rash,confusion, cough, tired, sore_throat, vomiting, anorexia, serious_situation, vaccine_status, patient_status\n"
                + "from user_account inner join patient on user_account.account_id = patient.account_id\n"
                + "				  inner join daily_report on patient.patient_id = daily_report.patient_id\n"
                + "				  inner join disease_detail on daily_report.disease_detail_id = disease_detail.disease_detail_id\n"
                + "        where user_account.account_id = ?";
        ResultSet rs = null;
        PreparedStatement pre = null;
        try {
            pre = getConnection().prepareStatement(sql);
            pre.setInt(1, accountid);
            rs = pre.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public ResultSet getUpdateDiseaseDetail(int accountId, String diseaseDetailID) {
        String sql = "select declaration_time,disease_detail.disease_detail_id, disease_detail.body_temperature, disease_detail.headache, disease_detail.fever, disease_detail.shortness_breath, disease_detail.loss_of_taste,diarrhea, skin_rash, confusion,cough , tired,sore_throat,vomiting , anorexia , serious_situation, vaccine_status ,patient_status\n"
                + "     from user_account inner join patient on user_account.account_id = patient.account_id\n"
                + "                       inner join daily_report on patient.patient_id = daily_report.patient_id\n"
                + "                        inner join disease_detail on daily_report.disease_detail_id = disease_detail.disease_detail_id\n"
                + "               WHERE user_account.account_id= ? and disease_detail.disease_detail_id = ? ";
        ResultSet rs = null;
        PreparedStatement pre = null;
        try {
            pre = getConnection().prepareStatement(sql);
            pre.setInt(1, accountId);
            pre.setString(2, diseaseDetailID);
            rs = pre.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public int addTravelSchedule(int accountid, TravelSchedule travel) {
        int n = 0;
        String sql = "INSERT INTO dbo.travel_schedule ( patient_id , departure, destination, time_go)\n"
                + "       VALUES (?,?,?,?)";
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            String getPatientID = "select patient.patient_id\n"
                    + "from user_account INNER JOIN patient ON user_account.account_id = patient.account_id\n"
                    + "WHERE user_account.account_id = ?";
            PreparedStatement pstm = getConnection().prepareStatement(getPatientID);
            pstm.setInt(1, accountid);
            ResultSet patientID = pstm.executeQuery();

            while (patientID.next()) {
                pre.setInt(1, patientID.getInt(1));
            }
            pre.setString(2, travel.getDeparture());
            pre.setString(3, travel.getDestination());
            pre.setDate(4, (java.sql.Date) travel.getTime());
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOTravelSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public ResultSet getTravelScheduleInformation(int accountid) {
        String sql = "select travel_schedule.departure, travel_schedule.destination, travel_schedule.time_go\n"
                + "from user_account INNER JOIN patient on user_account.account_id = patient.account_id\n"
                + "                   INNER JOIN travel_schedule on patient.patient_id = travel_schedule.patient_id"
                + "              WHERE user_account.account_id = ?";
        ResultSet rs = null;
        PreparedStatement pre = null;
        try {
            pre = getConnection().prepareStatement(sql);
            pre.setInt(1, accountid);
            rs = pre.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public List<DiseaseDetail> getPatientDetail(int accountId) {
        try {
            String query = "SELECT TOP 2 disease_detail.disease_detail_id, body_temperature, headache , fever, shortness_breath,loss_of_taste, diarrhea, skin_rash, confusion, cough, tired, sore_throat, vomiting, anorexia, serious_situation, vaccine_status, patient_status, declaration_time\n"
                    + "from user_account inner join patient on user_account.account_id = patient.account_id\n"
                    + "                  inner join daily_report on patient.patient_id = daily_report.patient_id\n"
                    + "				  inner join disease_detail on daily_report.disease_detail_id = disease_detail.disease_detail_id\n"
                    + "		WHERE user_account.account_id = ? \n"
                    + "		ORDER BY [declaration_time] DESC";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();
            List<DiseaseDetail> patientDetails = new ArrayList<>();
            while (rs.next()) {
                DiseaseDetail patient = new DiseaseDetail();
                patient.setDiseaseDetailId(rs.getInt(1));
                patient.setBodyTemperature(rs.getInt(2));
                patient.setFever(rs.getInt(4));
                patient.setHeadache(rs.getInt(3));
                patient.setShortnessBreath(rs.getInt(5));
                patient.setLossOfTaste(rs.getInt(6));
                patient.setDiarrhea(rs.getInt(7));
                patient.setSkinRash(rs.getInt(8));
                patient.setConfusion(rs.getInt(9));
                patient.setCough(rs.getInt(10));
                patient.setTired(rs.getInt(11));
                patient.setSoreThroat(rs.getInt(12));
                patient.setVomiting(rs.getInt(13));
                patient.setAnorexia(rs.getInt(14));
                patient.setSeriousSituation(rs.getInt(15));
                patient.setVaccineStatus(rs.getInt(16));
                patient.setPatientStatus(rs.getString(17));
                patient.setDeclarationTime(rs.getDate(18));
                patientDetails.add(patient);
            }
            return patientDetails;
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int addInterative(InteractiveList interactive, int accountID) {
        int n = 0;
        String sql = "INSERT INTO interactive_list (interactiveName,relationship,interactiveStatus , patient_id)\n"
                + "         VALUES( ?,?,?,?)";
        ResultSet rs = null;
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setString(1, interactive.getInteractiveName());
            pre.setString(2, interactive.getRelationship());
            pre.setString(3, interactive.getInteractiveStatus());
            String query = " select patient.patient_id\n"
                    + "	 from user_account inner join patient on user_account.account_id = patient.account_id\n"
                    + "	 where user_account.account_id = ?";
            PreparedStatement pstm = getConnection().prepareStatement(query);
            pstm.setInt(1, accountID);
            rs = pstm.executeQuery();
            while (rs.next()) {
                pre.setInt(4, rs.getInt(1));
            }
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public ResultSet getInteractiveData(int accountId) {
        String sql = "select interactive_id, interactiveName , relationship , interactiveStatus\n"
                + "from user_account inner join patient on user_account.account_id = patient.account_id\n"
                + "                   inner join interactive_list on patient.patient_id = interactive_list.patient_id\n"
                + "			where user_account.account_id = ?";
        ResultSet rs = null;
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setInt(1, accountId);
            rs = pre.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public ResultSet filterStatus(int accountId, String filter) {
        String sql = "select interactive_id, interactiveName , relationship , interactiveStatus\n"
                + "from user_account inner join patient on user_account.account_id = patient.account_id\n"
                + "                   inner join interactive_list on patient.patient_id = interactive_list.patient_id\n"
                + "			where user_account.account_id = ? \n";
        ResultSet rs = null;
        try {
            if (filter == null) {
                sql += " ";
            } else {
                sql += "   AND interactiveStatus like ?";
            }
            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setInt(1, accountId);
            pre.setString(2, '%' + filter + '%');
            rs = pre.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;

    }

    public ResultSet findByName(int accountId, String nameSearch) {
        ResultSet rs2 = null;
        ResultSet rs = null;
        try {
            String sql = "select interactive_id,interactiveName , relationship , interactiveStatus\n"
                    + "from user_account inner join patient on user_account.account_id = patient.account_id\n"
                    + "                   inner join interactive_list on patient.patient_id = interactive_list.patient_id\n"
                    + "			where user_account.account_id = ? \n"
                    + "			AND interactiveName like ?";
            PreparedStatement pstm = getConnection().prepareStatement(sql);
            pstm.setInt(1, accountId);
            pstm.setString(2, '%' + nameSearch + '%');
            rs2 = pstm.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs2;
    }

    public List<InteractiveList> getInteractiveList(int accountId) {
        List<InteractiveList> interactiveList = new ArrayList<>();
        String sql = "select interactiveName ,relationship , interactiveStatus ,interactive_list.patient_id\n"
                + "from user_account inner join patient on user_account.account_id = patient.account_id\n"
                + "                  inner join interactive_list on patient.patient_id = interactive_list.patient_id\n"
                + "               where user_account.account_id = ?";
        ResultSet rs = null;
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setInt(1, accountId);
            rs = pre.executeQuery();
            while (rs.next()) {
                InteractiveList interactive = new InteractiveList();
                interactive.setInteractiveName(rs.getString(1));
                interactive.setRelationship(rs.getString(2));
                interactive.setInteractiveStatus(rs.getString(3));
                interactive.setPatientId(rs.getInt(4));
                interactiveList.add(interactive);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return interactiveList;
    }

    public void updateInteractive(InteractiveList interactive) {
        String sql = "UPDATE interactive_list SET interactiveName = ?, relationship = ? , interactiveStatus = ? \n"
                + "       where interactive_id = ?";
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setString(1, interactive.getInteractiveName());
            pre.setString(2, interactive.getRelationship());
            pre.setString(3, interactive.getInteractiveStatus());
            pre.setInt(4, interactive.getInteractiveId());
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ResultSet getUpdateInteractive(int accountId, int interactiveId) {
        String sql = "select interactive_id, interactiveName , relationship , interactiveStatus\n"
                + "from user_account inner join patient on user_account.account_id = patient.account_id\n"
                + "                   inner join interactive_list on patient.patient_id = interactive_list.patient_id\n"
                + "			where user_account.account_id = ? and interactive_id = ?";
        ResultSet rs = null;
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setInt(1, accountId);
            pre.setInt(2, interactiveId);
            rs = pre.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
}
