/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import common.CommonUtil;
import entity.TravelSchedule;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pc
 */
public class DAOTravelSchedule extends DAO {

    public DAOTravelSchedule() {

    }

    public int update(TravelSchedule travel) {
        int n = 0;
        String sql = "UPDATE dbo.travel_schedule SET patient_id = ? , departure_id = ? , destination_id = ? , time_go = '?' \n"
                + "WHERE travel_schedule.travel_id = ?";
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setInt(1, travel.getPatientId());
            pre.setString(2, travel.getDeparture());
            pre.setString(3, travel.getDestination());
            pre.setDate(4, (Date) travel.getTime());
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOTravelSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public int removeTravelSchedule(int travelID) {
        int n = 0;
        String sql = "DELETE FROM travel_schedule\n"
                + "WHERE travel_schedule.travel_id = ?";
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setInt(1, travelID);
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOTravelSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public ResultSet getListTravelSchedule() {
        String sql = "select travel_schedule.patient_id, full_name, email, phone_number, departure,\n"
                + "destination, time_go, doctor_name, nurse_name, doctor_id, nurse_id, patient.account_id\n"
                + "from travel_schedule left join patient on travel_schedule.patient_id = patient.patient_id\n"
                + "left join user_account on patient.account_id = user_account.account_id\n"
                + "join daily_report on daily_report.patient_id = patient.patient_id";
        ResultSet rs = null;
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql);
            rs = ps.executeQuery();
        } catch (Exception ex) {
            Logger.getLogger(DAOTravelSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public ResultSet getListTravelScheduleFilter(int value) {
        String sql = "select travel_schedule.patient_id, full_name, email, phone_number, departure,\n"
                + "destination, time_go, doctor_name, nurse_name, doctor_id, nurse_id, patient.account_id\n"
                + "from travel_schedule left join patient on travel_schedule.patient_id = patient.patient_id\n"
                + "left join user_account on patient.account_id = user_account.account_id\n"
                + "join daily_report on daily_report.patient_id = patient.patient_id";
        if (value == 1) {
            sql += "order by time_go,destination";
        } else if (value == 2) {
            sql += "order by doctor_name";
        } else if (value == 3) {
            sql += "order by nurse_name";
        }

        ResultSet rs = null;
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql);
            rs = ps.executeQuery();
        } catch (Exception ex) {
            Logger.getLogger(DAOTravelSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public ResultSet getPatientTravelSchedule(int id) {
        String sql = "select full_name, phone_number, gender, birth_day,age, status, role_id, email, \n"
                + "departure, destination, time_go\n"
                + "from travel_schedule join patient on travel_schedule.patient_id = patient.patient_id\n"
                + "join user_account on user_account.account_id = patient.account_id\n"
                + "where patient.patient_id = ?";
        ResultSet rs = null;
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
        } catch (Exception ex) {
            Logger.getLogger(DAOTravelSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public ResultSet getListF0TravelSche() {
        String sql = "select distinct destination, time_go\n"
                + "from travel_schedule join patient on patient.patient_id = travel_schedule.patient_id\n"
                + "join user_account on patient.account_id = user_account.account_id \n"
                + "left join daily_report on daily_report.patient_id = patient.patient_id\n"
                + "left join disease_detail on disease_detail.disease_detail_id = daily_report.disease_detail_id\n"
                + "where patient_status = 'F0'";
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            ps = getConnection().prepareStatement(sql);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DAOTravelSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public ResultSet getListF1F2(String destination, Date time_go) {
        String sql = "with t as(\n"
                + "select distinct full_name, email, phone_number, destination, time_go, patient_status\n"
                + "from travel_schedule join patient on patient.patient_id = travel_schedule.patient_id\n"
                + "join user_account on patient.account_id = user_account.account_id \n"
                + "left join daily_report on daily_report.patient_id = patient.patient_id\n"
                + "left join disease_detail on disease_detail.disease_detail_id = daily_report.disease_detail_id\n"
                + "except\n"
                + "select distinct full_name, email, phone_number, destination, time_go, patient_status\n"
                + "from travel_schedule join patient on patient.patient_id = travel_schedule.patient_id\n"
                + "join user_account on patient.account_id = user_account.account_id \n"
                + "left join daily_report on daily_report.patient_id = patient.patient_id\n"
                + "left join disease_detail on disease_detail.disease_detail_id = daily_report.disease_detail_id\n"
                + "where patient_status ='F0'\n"
                + ") select distinct full_name, email, phone_number, destination, time_go from t\n"
                + "where destination = ? and time_go = ? ";
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            ps = getConnection().prepareStatement(sql);
            ps.setString(1, destination);
            ps.setDate(2, time_go);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DAOTravelSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

}
