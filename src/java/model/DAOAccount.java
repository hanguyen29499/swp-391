/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import common.CommonUtil;
import entity.Account;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PC
 */
public class DAOAccount extends DAO {

    public void register(Account newAccount) {
        String query = "INSERT INTO user_account(username, [password], full_name, phone_number, gender, birth_day, age, [status], role_id, email, address) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        try {
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setString(1, newAccount.getUserName());
            ps.setString(2, newAccount.getPassword());
            ps.setString(3, newAccount.getFullname());
            ps.setString(4, newAccount.getPhonenumber());
            ps.setInt(5, newAccount.getGender());
            ps.setDate(6, (java.sql.Date) Date.valueOf(newAccount.getDob()));
            ps.setInt(7, newAccount.getAge());
            ps.setInt(8, newAccount.getStatus());
            ps.setInt(9, newAccount.getRoleId());
            ps.setString(10, newAccount.getEmail());
            ps.setString(11, newAccount.getAddress());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Account findByUserName(String username) {
        try {
            String query = "SELECT * FROM user_account "
                    + "WHERE username = ? ";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Account userAccount = new Account();
                userAccount.setAccountId(rs.getInt("account_id"));
                return userAccount;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Account findByPhoneNumber(String phoneNumber) {
        try {
            String query = "SELECT * FROM user_account "
                    + "WHERE phone_number = ? ";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setString(1, phoneNumber);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Account userAccount = new Account();
                userAccount.setAccountId(rs.getInt("account_id"));
                return userAccount;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Account findByEmail(String email) {
        try {
            String query = "SELECT * FROM user_account "
                    + "WHERE email = ? ";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Account userAccount = new Account();
                userAccount.setAccountId(rs.getInt("account_id"));
                return userAccount;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void update(Account acc) {
        try {
            String sql = "Update user_account set [full_name]=?,[gender]=?,[birth_day]=?,[age]=?,[address]=? where account_id=?";
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setString(1, acc.getFullname());
            ps.setInt(2, acc.getGender());
            ps.setString(3, acc.getDob());
            ps.setInt(4, acc.getAge());
            ps.setString(5, acc.getAddress());
            ps.setInt(6, acc.getAccountId());

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Account findByLoginId(String username, String passwordInput) {
        try {
            String password = encrypt(passwordInput);
            String query = "SELECT * FROM user_account "
                    + "WHERE username = ? "
                    + "AND password = ? ";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Account userAccount = new Account();
                userAccount.setAccountId(rs.getInt("account_id"));
                userAccount.setUserName(rs.getString("username"));
                userAccount.setFullname(rs.getString("full_name"));
                userAccount.setPhonenumber(rs.getString("phone_number"));
                userAccount.setGender(rs.getInt("gender"));
                userAccount.setDob(rs.getString("birth_day"));
                userAccount.setAge(rs.getInt("age"));
                userAccount.setEmail(rs.getString("email"));
                userAccount.setRoleId(rs.getInt("role_id"));
                userAccount.setAddress(rs.getString("address"));
                userAccount.setStatus(rs.getInt("status"));
                return userAccount;
            }
            return null;
        } catch (Exception ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Account findById(int accountId) {
        try {
            String query = "SELECT * FROM user_account "
                    + "WHERE account_id = ? ";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Account userAccount = new Account();
                userAccount.setAccountId(rs.getInt("account_id"));
                userAccount.setUserName(rs.getString("username"));
                userAccount.setFullname(rs.getString("full_name"));
                userAccount.setPhonenumber(rs.getString("phone_number"));
                userAccount.setGender(rs.getInt("gender"));
                userAccount.setDob(rs.getString("birth_day"));
                userAccount.setAge(rs.getInt("age"));
                userAccount.setEmail(rs.getString("email"));
                userAccount.setRoleId(rs.getInt("role_id"));
                userAccount.setAddress(rs.getString("address"));
                userAccount.setStatus(rs.getInt("status"));

                return userAccount;
            }

            return null;
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void deleteById(int accountId) {
        String sql = "DELETE FROM user_account\n"
                + "WHERE account_id = " + accountId + "";
        try {
            getStatement().execute(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAODoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changePassword(int id, String password) {
        try {
            String passEncrypted = CommonUtil.encrypt(password);
            String sql = "UPDATE [dbo].[user_account]\n"
                    + "SET [password] = ?\n"
                    + "WHERE account_id=?";
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setString(1, passEncrypted);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Account> getAll(String filtered) {
        List<Account> allAccounts = new ArrayList<>();
        try {
            String sql = "SELECT * FROM user_account";
            switch (filtered) {
                case "1":
                    sql += " where role_id = 1 ";
                    break;
                case "2":
                    sql += " where role_id = 2 ";
                    break;
                case "3":
                    sql += " where  role_id= 3 ";
                    break;
                case "4":
                    sql += " where role_id = 4 ";
                    break;
                default:
                    sql += "";
                    break;
            }
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                allAccounts.add(new Account(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getString(11),
                        rs.getString(12)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return allAccounts;
    }

    public List<Account> findByRoleId(int roleID) {
        List<Account> accountsByRole = new ArrayList<>();
        try {
            String sql = "SELECT * FROM user_account\n"
                    + "WHERE role_id = " + roleID + "";

            PreparedStatement ps = getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                accountsByRole.add(new Account(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getString(11),
                        rs.getString(12)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return accountsByRole;
    }

    public void deleteByRoleId(int roleID) {
        String sql = "DELETE FROM user_account\n"
                + "WHERE role_id = " + roleID + "";
        try {
            getStatement().execute(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAOAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getAccountPassword(String username) {
        String sql = "SELECT password FROM user_account WHERE username=?";
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void changeStatus(int id, int status) {
        String sql = "update user_account set status=? where account_id=?";
        try {
            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setInt(1, status);
            pre.setInt(2, id);
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOAccount.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Account> findByName(String name) {
        List<Account> accountbyname = new ArrayList<>();
        try {
            String sql = "SELECT * FROM user_account where full_name like'%" + name + "%'";

            PreparedStatement ps = getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                accountbyname.add(new Account(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getString(11),
                        rs.getString(12)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return accountbyname;
    }
}
