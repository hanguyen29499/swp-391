/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Account;
import entity.Patient;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOPatient extends DAO {

    public List<Patient> getPatients() {
        List<Patient> patients = new ArrayList<>();
        String sql = "SELECT * FROM patient";
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                patients.add(new Patient(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getInt(4)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOPatient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return patients;
    }

    public List<Patient> getPatient() {
        List<Patient> patients = new ArrayList<>();
        try {
            String sql = "select * from patient join user_account on patient.account_id= user_account.account_id";

            PreparedStatement ps = getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account account = new Account();
                account.setGender(rs.getInt("gender"));
                account.setFullname(rs.getString("full_name"));
                account.setPhonenumber(rs.getString("phone_number"));
                account.setAccountId(rs.getInt("account_id"));

                Patient patient = new Patient();

                patient.setPatientId(rs.getInt("patient_id"));
                patient.setAccountId(rs.getInt("account_id"));
                patient.setDoctorId(rs.getInt("doctor_id"));
                patient.setNurseId(rs.getInt("nurse_id"));
                patient.setDoctorName(rs.getString("doctor_name"));
                patient.setNurseName(rs.getString("nurse_name"));
                patient.setAccount(account);

                patients.add(patient);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return patients;
    }

    public void create(Integer accountId) {
        try {
            String query = "INSERT INTO [dbo].[patient] "
                    + "           ([account_id]) "
                    + "     VALUES(?);";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setInt(1, accountId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOPatient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Patient> getPatientById(int patient_id) {
        List<Patient> patients = new ArrayList<>();
        try {
            String sql = "select * from patient join user_account on patient.account_id= user_account.account_id where patient_id=?";

            PreparedStatement ps = getConnection().prepareStatement(sql);
            ps.setInt(1, patient_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account account = new Account();
                account.setGender(rs.getInt("gender"));
                account.setFullname(rs.getString("full_name"));
                account.setPhonenumber(rs.getString("phone_number"));
                account.setAccountId(rs.getInt("account_id"));

                Patient patient = new Patient();

                patient.setPatientId(rs.getInt("patient_id"));
                patient.setAccountId(rs.getInt("account_id"));
                patient.setDoctorId(rs.getInt("doctor_id"));
                patient.setNurseId(rs.getInt("nurse_id"));
                patient.setDoctorName(rs.getString("doctor_name"));
                patient.setNurseName(rs.getString("nurse_name"));
                patient.setAccount(account);

                patients.add(patient);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return patients;
    }

    public List<Patient> searchPatientByName(String patient_name) {
        List<Patient> patients = new ArrayList<>();
        try {
            String sql = "select * from patient join user_account on patient.account_id= user_account.account_id where full_name like '%" + patient_name + "%'";

            PreparedStatement ps = getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account account = new Account();
                account.setGender(rs.getInt("gender"));
                account.setFullname(rs.getString("full_name"));
                account.setPhonenumber(rs.getString("phone_number"));
                account.setAccountId(rs.getInt("account_id"));

                Patient patient = new Patient();

                patient.setPatientId(rs.getInt("patient_id"));
                patient.setAccountId(rs.getInt("account_id"));
                patient.setDoctorId(rs.getInt("doctor_id"));
                patient.setNurseId(rs.getInt("nurse_id"));
                patient.setDoctorName(rs.getString("doctor_name"));
                patient.setNurseName(rs.getString("nurse_name"));
                patient.setAccount(account);

                patients.add(patient);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return patients;
    }
}
