package model;

import dto.PatientDetail;
import dto.ViewPatient;
import entity.Account;
import entity.Doctor;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PC
 */
public class DAODoctor extends DBConnect {

    public DAODoctor() {
        daoAccount = new DAOAccount();
    }

    public List<ViewPatient> getTenPatients(Integer page, Integer doctorId, Integer filtered, String namePatient) {
        try {
            String query = "SELECT p.patient_id, ua.full_name, \n"
                    + "ua.email,ua.phone_number, "
                    + "dd.[body_temperature] "
                    + "      ,dd.[headache] "
                    + "      ,dd.[fever] "
                    + "      ,dd.[shortness_breath] "
                    + "	  ,dd.[vaccine_status] "
                    + "	  ,dd.[patient_status] "
                    + "	  ,dd.[serious_situation] "
                    + ", max_date "
                    + "FROM patient p "
                    + "JOIN daily_report dr "
                    + "ON p.patient_id = dr.patient_id "
                    + "JOIN disease_detail dd "
                    + "ON dd.disease_detail_id = dr.disease_detail_id "
                    + "JOIN user_account ua "
                    + "ON ua.account_id = p.account_id "
                    + "JOIN "
                    + "(SELECT dl.patient_id,MAX(d.declaration_time) AS max_date "
                    + "FROM disease_detail d join "
                    + "daily_report dl ON dl.disease_detail_id = d.disease_detail_id "
                    + "GROUP BY dl.patient_id)a "
                    + "ON a.patient_id = p.patient_id AND a.max_date = dd.declaration_time "
                    + "WHERE p.doctor_id = ? "
                    + "AND dd.patient_status = 'F0' ";
            if (filtered == 1) {
                query += "AND dd.body_temperature >= 38 ";
            } else if (filtered == 2) {
                query += "AND dd.headache = 1 ";
            } else if (filtered == 3) {
                query += "AND dd.shortness_breath = 1 ";
            }else if (filtered == 4) {
                query += "AND dd.serious_situation = 1 ";
            }
            query += " AND ua.full_name like ? "
                    + "ORDER BY p.patient_id "
                    + "OFFSET ? ROWS FETCH NEXT 10 ROWS ONLY";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setInt(1, doctorId);
            ps.setString(2, '%'+namePatient+'%');
            ps.setInt(3, (page - 1) * 10);
            ResultSet rs = ps.executeQuery();
            List<ViewPatient> listPatients = new ArrayList<>();
            while (rs.next()) {
                ViewPatient patient = new ViewPatient();
                patient.setPatientId(rs.getInt("patient_id"));
                patient.setFullname(rs.getString("full_name"));
                patient.setPhonenumber(rs.getString("phone_number"));
                patient.setEmail(rs.getString("email"));
                patient.setBodyTemperature(rs.getInt("body_temperature"));
                patient.setFever(rs.getInt("fever"));
                patient.setHeadache(rs.getInt("headache"));
                patient.setShortnessBreath(rs.getInt("shortness_breath"));
                patient.setSeriousSituation(rs.getInt("serious_situation"));
                patient.setVaccineStatus(rs.getInt("vaccine_status"));
                patient.setPatientStatus(rs.getString("patient_status"));
                patient.setDeclarationTime(rs.getDate("max_date"));
                listPatients.add(patient);
            }
            return listPatients;
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Integer getDoctorIdByAccountId(Integer accountId) {
        try {
            String query = "SELECT d.doctor_id FROM user_account ua "
                    + "JOIN doctor d "
                    + "ON d.account_id = ua.account_id "
                    + "WHERE ua.account_id = ?";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("doctor_id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Integer getTotalPatientsByDoctorId(Integer doctorId, Integer filtered, String namePatient) {
        try {
            String query = "SELECT COUNT(p.patient_id) AS [total_patients]\n"
                    + "FROM patient p\n"
                    + "JOIN daily_report dr\n"
                    + "ON p.patient_id = dr.patient_id\n"
                    + "JOIN disease_detail dd\n"
                    + "ON dd.disease_detail_id = dr.disease_detail_id\n"
                    + "JOIN user_account ua\n"
                    + "ON ua.account_id = p.account_id\n"
                    + "JOIN\n"
                    + "(SELECT dl.patient_id,MAX(d.declaration_time) AS max_date\n"
                    + "FROM disease_detail d join \n"
                    + "daily_report dl ON dl.disease_detail_id = d.disease_detail_id \n"
                    + "GROUP BY dl.patient_id)a\n"
                    + "ON a.patient_id = p.patient_id AND a.max_date = dd.declaration_time\n"
                    + "WHERE p.doctor_id = ?\n"
                    + "AND dd.patient_status = 'F0'";
            if (filtered == 1) {
                query += "AND dd.body_temperature >= 38 ";
            } else if (filtered == 2) {
                query += "AND dd.headache = 1 ";
            } else if (filtered == 3) {
                query += "AND dd.shortness_breath = 1 ";
            }
            query += "AND ua.full_name like ?";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setInt(1, doctorId);
            ps.setString(2, '%'+namePatient+'%');
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total_patients");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private final DAOAccount daoAccount;

    public List<Doctor> getDoctors() {
        List<Doctor> doctors = new ArrayList<>();
        try {

            String sql = "SELECT * FROM doctor INNER JOIN user_account\n"
                    + "ON doctor.account_id = user_account.account_id";
            PreparedStatement ps = getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Account account = new Account();
                account.setGender(rs.getInt("gender"));
                account.setFullname(rs.getString("full_name"));
                account.setPhonenumber(rs.getString("phone_number"));
                account.setAccountId(rs.getInt("account_id"));

                Doctor doctor = new Doctor();
                doctor.setDoctorId(rs.getInt("doctor_id"));
                doctor.setRole(rs.getString("role"));
                doctor.setBranch(rs.getString("branch"));
                doctor.setAccount(account);

                doctors.add(doctor);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAODoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return doctors;
    }

    public void create(Doctor doctor) {
        try {
            String sql = "insert into doctor (branch, role, account_id)\n"
                    + "values (?, ?, ?)";

            PreparedStatement pre = getConnection().prepareStatement(sql);
            pre.setString(1, doctor.getBranch());
            pre.setString(2, doctor.getRole());
            pre.setInt(3, doctor.getAccountId());
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAODoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void update(Doctor doctor, Account account) {
        String sql = ""
                // Update infor for table doctor 
                + "update doctor\n"
                + "set branch = '" + doctor.getBranch() + "'\n"
                + "where doctor_id = " + doctor.getDoctorId() + " \n"
                + "\n"
                //Update infor for table user_account
                + "update user_account \n"
                + "set full_name = '" + account.getFullname() + "', "
                + "phone_number = '" + account.getPhonenumber() + "', "
                + "gender = " + account.getGender() + "\n"
                + "where account_id = " + account.getAccountId() + "";

        try {
            getStatement().execute(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAODoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete(Doctor doctor) {
        String sql = "DELETE FROM doctor\n"
                + "WHERE doctor_id = " + doctor.getDoctorId() + "";
        daoAccount.deleteById(doctor.getAccountId());
        try {
            getStatement().execute(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAODoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Doctor getByID(int doctorID) {
        try {
            String sql = "select * from doctor inner join user_account\n"
                    + "on doctor.account_id = user_account.account_id\n"
                    + "where doctor_id = ?";

            PreparedStatement stm = getConnection().prepareStatement(sql);
            stm.setInt(1, doctorID);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Account account = new Account();
                account.setGender(rs.getInt("gender"));
                account.setFullname(rs.getString("full_name"));
                account.setPhonenumber(rs.getString("phone_number"));
                account.setAccountId(rs.getInt("account_id"));
                account.setEmail(rs.getString("email"));
                account.setDob(rs.getString("birth_day"));

                Doctor doctor = new Doctor();
                doctor.setDoctorId(rs.getInt("doctor_id"));
                doctor.setBranch(rs.getString("branch"));
                doctor.setRole(rs.getString("role"));
                doctor.setAccount(account);
                return doctor;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(DAODoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<PatientDetail> getPatientDetailByDoctorId(Integer doctorId, Integer patientId) {
        try {
            String query = "SELECT TOP 2 p.patient_id, ua.full_name, \n"
                    + "ua.email,ua.phone_number, ua.age, ua.gender, ua.[address], dd.[disease_detail_id]\n"
                    + "            ,[body_temperature]\n"
                    + "      ,[headache]\n"
                    + "      ,[fever]\n"
                    + "      ,[shortness_breath]\n"
                    + "      ,[loss_of_taste]\n"
                    + "      ,[diarrhea]\n"
                    + "      ,[skin_rash]\n"
                    + "      ,[confusion]\n"
                    + "      ,[cough]\n"
                    + "      ,[tired]\n"
                    + "      ,[sore_throat]\n"
                    + "      ,[vomiting]\n"
                    + "      ,[anorexia]\n"
                    + "      ,[serious_situation]\n"
                    + "      ,[vaccine_status]\n"
                    + "      ,[patient_status]\n"
                    + "	  ,[declaration_time]\n"
                    + "FROM patient p\n"
                    + "JOIN daily_report dr\n"
                    + "ON p.patient_id = dr.patient_id\n"
                    + "JOIN disease_detail dd\n"
                    + "ON dd.disease_detail_id = dr.disease_detail_id\n"
                    + "JOIN user_account ua\n"
                    + "ON ua.account_id = p.account_id\n"
                    + "WHERE p.doctor_id = ?\n"
                    + "AND p.patient_id = ?\n"
                    + "ORDER BY [declaration_time] DESC";

            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setInt(1, doctorId);
            ps.setInt(2, patientId);
            ResultSet rs = ps.executeQuery();
            List<PatientDetail> patientDetails = new ArrayList<>();
            while (rs.next()) {
                PatientDetail patient = new PatientDetail();
                patient.setPatientId(rs.getInt("patient_id"));
                patient.setFullname(rs.getString("full_name"));
                patient.setPhonenumber(rs.getString("phone_number"));
                patient.setEmail(rs.getString("email"));
                patient.setAge(rs.getInt("age"));
                patient.setGender(rs.getInt("gender"));
                patient.setAddress(rs.getString("address"));
                patient.setBodyTemperature(rs.getInt("body_temperature"));
                patient.setFever(rs.getInt("fever"));
                patient.setHeadache(rs.getInt("headache"));
                patient.setShortnessBreath(rs.getInt("shortness_breath"));
                patient.setLossOfTaste(rs.getInt("loss_of_taste"));
                patient.setDiarrhea(rs.getInt("diarrhea"));
                patient.setSkinRash(rs.getInt("skin_rash"));
                patient.setConfusion(rs.getInt("confusion"));
                patient.setCough(rs.getInt("cough"));
                patient.setTired(rs.getInt("tired"));
                patient.setSoreThroat(rs.getInt("sore_throat"));
                patient.setVomiting(rs.getInt("vomiting"));
                patient.setAnorexia(rs.getInt("anorexia"));
                patient.setSeriousSituation(rs.getInt("serious_situation"));
                patient.setVaccineStatus(rs.getInt("vaccine_status"));
                patient.setPatientStatus(rs.getString("patient_status"));
                patient.setDeclarationTime(rs.getDate("declaration_time"));
                patient.setDiseaseDetailId(rs.getInt("disease_detail_id"));
                patientDetails.add(patient);
            }
            return patientDetails;
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void updateStatusPatient(Integer diseaseDetailId) {
        try {
            String query = "UPDATE [dbo].[disease_detail] "
                    + "   SET [patient_status] = 'OK' "
                    + " WHERE disease_detail_id = ?";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setInt(1, diseaseDetailId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    public void updateCommentForPatient(Integer patientId, String comment) {
        try {
            String query = "UPDATE [dbo].[patient] "
                    + "   SET [doctor_comment] = ? "
                    + " WHERE [patient_id] = ?";
            PreparedStatement ps = getConnection().prepareStatement(query);
            ps.setString(1, comment);
            ps.setInt(2, patientId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    


}
