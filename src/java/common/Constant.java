/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

/**
 *
 * @author PC
 */
public final class Constant {

    private Constant() {
    }

    public static final String KEY = "SWP";

    // Gender
    public static final int MALE = 1;
    public static final int FEMALE = 0;

    // Status
    public static final int ACTIVE = 1;
    public static final int IN_ACTIVE = 0;

    // Role
    public static final int ROLE_ADMIN = 1;
    public static final int ROLE_DOCTOR = 2;
    public static final int ROLE_NURSE = 3;
    public static final int ROLE_PATIENT = 4;

    // Vote Start
    public static final int ONE_STAR = 1;
    public static final int TWO_STAR = 2;
    public static final int THREE_STAR = 3;
    public static final int FOUR_STAR = 4;
    public static final int FIVE_STAR = 5;

}
